/*---------------------------------------------------------------------------
Jump script by Stormridge
Leverages unscheduled UI thread for switch moves to avoid glitching screen.
---------------------------------------------------------------------------*/
private ["_height","_vel","_dir","_speed", "_haltPropagation"];

if (br_jumpAllowPlayerJump) then 
{
    br_jumpAllowPlayerJump=false;
    
    if ((inputAction "Turbo" > 0) || (inputAction "MoveFastForward" > 0) || ((speed player) * 0.277778 > 12)) then 
    {
        if  (player == vehicle player && 
                isTouchingGround player && 
                (stance player isEqualTo "STAND" || stance player isEqualTo "CROUCH")) then  
        {
            br_playerJumpAnimationStateInitial = animationState player;
            br_playerJumpVelocityJumpStart = velocity player;
            _zVelocityInitial = ((br_playerJumpVelocityJumpStart select 2) max br_jumpZForceStart) min 5;
            br_playerJumpVelocityJumpStart set [2, _zVelocityInitial];
            _speedCheck = sqrt ((br_playerJumpVelocityJumpStart select 0)^2 + (br_playerJumpVelocityJumpStart select 1)^2 + (br_playerJumpVelocityJumpStart select 2)^2);
            if (_speedCheck > br_jumpMaxSpeedLimit) then 
            {
                if (abs(br_playerJumpVelocityJumpStart select 0) > br_jumpMaxSpeedLimit) then {br_playerJumpVelocityJumpStart set [0,0];};
                if (abs(br_playerJumpVelocityJumpStart select 1) > br_jumpMaxSpeedLimit) then {br_playerJumpVelocityJumpStart set [1,0];};
            };
            br_frameCount = 0;
            br_jumpStarted = false;
            br_jumpStartTime = diag_tickTime;
            br_playerJumpFrameManager = addMissionEventHandler ["EachFrame",
                {
                    br_frameCount = br_frameCount + 1;
                    if (br_frameCount == 1) then
                    {
                        if (currentWeapon player isEqualTo "") then 
                        {
                            player setVelocity ((br_playerJumpVelocityJumpStart vectorMultiply .5) vectorAdd [0,0,2]);
                        }
                        else
                        {
                            player setVelocity br_playerJumpVelocityJumpStart;
                        };
                        addMissionEventHandler ["Draw3D", 
                                {
                                    player switchMove "AovrPercMrunSrasWrflDf";
                                    removeMissionEventHandler ["Draw3D", _thisEventHandler];
                                }];
                        BR_Animation = [player,"AovrPercMrunSrasWrflDf"];
                        publicVariable "BR_Animation";                                
                    }
                    else
                    {
                        if (isTouchingGround vehicle player && diag_tickTime - br_jumpStartTime > .4) then
                        {
                            removeMissionEventHandler ["EachFrame", _thisEventHandler];
                            addMissionEventHandler ["Draw3D", 
                                {
                                    player switchMove br_playerJumpAnimationStateInitial;
                                    removeMissionEventHandler ["Draw3D", _thisEventHandler];
                                }];
                            [] spawn {
                                uiSleep 1;
                                br_jumpAllowPlayerJump=true;
                            };
                        };
                    };
                }
            ];
        }
        else
        {
            br_jumpAllowPlayerJump=true; 
        };
    }
    else
    {
        br_jumpAllowPlayerJump=true;
    };
};

