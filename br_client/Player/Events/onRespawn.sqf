/*---------------------------------------------------------------------------
STRM: Called when player respawns
---------------------------------------------------------------------------*/
params ["_newPlayerObject", "_oldBody"];

diag_log format["[RESPAWN] NewUnit=%1, Corpse=%2]", _newPlayerObject, _oldBody];

// ##############################################
// RULE: Spectator?
// ##############################################
if (call BR_Client_Spectator_CanPlayerSpectate) then
{
	diag_log "[RESPAWN] Player has spectator rights.  Starting spectator mode.";
	(getPosATL _oldBody) call BR_Client_Spectator_StartSpectatorMode;
}
else
{
	// ##############################################
	// RULE: Team Spectator?
	// ##############################################
	if (br_inTeam && br_public_teamingEnabled && br_game_started) then 
	{
		diag_log "[RESPAWN] Player is in a team.  Starting team spectator mode.";
		call BR_Client_Player_TeamSpectator_StartTeamSpectator;
	}
	else
	{
		// BAD - Should not happen.
		diag_log "[RESPAWN] ERROR.  Should not be reached on live servers.  Player was not kicked on death and has respawned!";
		endMission "END6";
		player setVariable ["PUBR_isover",true,true];
	};
};
true 