/*---------------------------------------------------------------------------
STMR: Called when player gets in a vehicle
---------------------------------------------------------------------------*/
params ["_unit","_positionCargo","_vehicleObject", "_turretArray"];
if (_vehicleObject in br_heliVehicleObjects) then {
	call BR_Client_GUI_Help_PlayerEntersHelicopter;
	if (_vehicleObject isKindOf "Air") then {
		[] spawn {
			while {!((vehicle player) isEqualTo player)} do {
				_fuel = fuel vehicle player;
				if (_fuel < .01) exitWith {["WARNING:  YOU ARE OUT OF FUEL!  LAND NOW!", 7] call BR_Client_GUI_Notifications_NotifyPlayer;};
				sleep 3;
			};
		};
	};
};
false