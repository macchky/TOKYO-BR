/*---------------------------------------------------------------------------
STRM: Player is dead.  Wat2do?
---------------------------------------------------------------------------*/
[] spawn {

	private _playerUID = getplayeruid player;

	diag_log format ["[PLAYER KILLED] Player ID: %1",_playerUID];

	player setVariable ["br_playerDed", 0, true];

	if (call BR_Client_Spectator_CanPlayerSpectate) then
	{
		diag_log "[PLAYER KILLED] Player has spectator rights.  Waiting for respawn body.";
		systemChat ">> Spectator Mode will start in 5 seconds...";
	}
	else
	{
		if (br_inTeam && br_public_teamingEnabled && br_game_started) then 
		{
			diag_log "[PLAYER KILLED] Player is in a team.  Avoiding kick.  Waiting for respawned body.";
			systemChat ">> If teammates are alive, Spectator Mode will start in 5 seconds...";
		}
		else
		{
			diag_log "[PLAYER KILLED] Player is dead, has no team, and has no spectator rights.";
			"THANKS FOR PLAYING BATTLE ROYALE!" call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;
			uiSleep 5;
			player setVariable ["PUBR_isover",true,true];
		};
	};
};
true 