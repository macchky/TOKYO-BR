/*---------------------------------------------------------------------------
STRM: Cheesy hit handler.  Need to move to spectator's EH
---------------------------------------------------------------------------*/
if (br_public_isSpectatorInGame) then {
	br_PlayerLastHitTime = diag_tickTime;	
	if !(br_PlayerIsBeingHit) then {	
		br_PlayerIsBeingHit=true;
		player setVariable ["BRHit",true,true];
		[] spawn {
			while {true} do {
				sleep 2;
				if (diag_tickTime - br_PlayerLastHitTime > 7) exitWith { player setVariable ["BRHit",false,true]; br_PlayerIsBeingHit=false; };
			};
		};
	};
};