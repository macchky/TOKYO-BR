/*---------------------------------------------------------------------------
STRM: Cheesy fired handler.  Need to move to spectator's EH
---------------------------------------------------------------------------*/
if (br_public_isSpectatorInGame) then {
	br_PlayerLastFiredTime = diag_tickTime;
	if !(br_PlayerIsFiring) then {
		br_PlayerIsFiring=true;
		player setVariable ["BRFired",true,true];
		[] spawn {
			while {true} do {
				sleep 2;
				if (diag_tickTime - br_PlayerLastFiredTime > 7) exitWith { player setVariable ["BRFired",false,true]; br_PlayerIsFiring=false; };
			};
		};
	};
};