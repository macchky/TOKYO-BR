/*---------------------------------------------------------------------------
Stormridge's team spectate.
---------------------------------------------------------------------------*/
diag_log "[TEAM SPECTATOR] Initializing...";
diag_log "[TEAM SPECTATOR] Starting Team Member Monitor";

br_teamSpectatableUnitsInit=false;

[] spawn BR_Client_Player_TeamSpectator_TeamMemberMonitor;

waitUntil {br_teamSpectatableUnitsInit};

diag_log "[TEAM SPECTATOR] Starting view team member";

call BR_Client_Player_TeamSpectator_ViewFirstTeamMember;

systemChat ">> TEAM SPECTATE Started.  Use arrow keys to cycle through your teammates' view.";

true 