/*---------------------------------------------------------------------------
STRM: Monitors player's team mates to see who's still alive...
---------------------------------------------------------------------------*/
call BR_Client_Player_TeamSpectator_UpdateSpectatableUnits;
br_teamSpectatableUnitsInit=true;

diag_log format["[TEAM SPECTATOR: MONITOR] Initial spectatable units: %1", br_teamSpectatableUnits];
diag_log format["[TEAM SPECTATOR: MONITOR] Entering monitor loop..."];

private _firstLoop=true;

while {true} do
{
	call BR_Client_Player_TeamSpectator_UpdateSpectatableUnits;

	// RULE:  Must have units to spectate.
	if (count br_teamSpectatableUnits < 1) exitWith
	{
		diag_log "[TEAM SPECTATOR: MONITOR] Player has no team members.  Disconnecting...";
		if (_firstLoop) then
		{
			player setVariable ["PUBR_isover",true,true];  // Immediate DC
		}
		else 
		{
			">>> You have no teammates left!  Disconnecting..." call BR_Client_Player_Util_DisconnectPlayerWithMessage;
		};
	};

	// RULE: Can't spectate dead units, but must be initialized first.
	//		Do not kick for viewing dead / non-listed player due to race conditions
	if !(isNull br_teamCurrentViewMemberObject) then 
	{
		if ( !(alive br_teamCurrentViewMemberObject) || 
			!(isPlayer br_teamCurrentViewMemberObject)  ||
			side br_teamCurrentViewMemberObject == CIVILIAN) then
		{
			diag_log "[TEAM SPECTATOR: MONITOR] The player we are spectating is dead, not a player, or wrong side.  Switching views...";
			[1,true] call BR_Client_Player_TeamSpectator_ViewNextTeamMember;
			systemChat ">>> Switching view.";
		};
	}
	else
	{
		diag_log "[TEAM SPECTATOR: MONITOR] Current view member object is null.  Monitor will assume system not yet initialized.";
	};

	_firstLoop=false;

	sleep 1;
};