/*---------------------------------------------------------------------------
STRM: Grab first team member and view them.
---------------------------------------------------------------------------*/
diag_log "[TEAM SPECTATOR: VIEW FIRST] Requested to start viewing first team member.";

if ((count br_teamSpectatableUnits) < 1) exitWith
{
	diag_log "[TEAM SPECTATOR: VIEW NEXT] ERROR!  There are no spectatable units!"; 
	false
};

br_teamCurrentViewMemberObject = br_teamSpectatableUnits select 0;

br_teamCurrentViewMemberObject call BR_Client_Player_TeamSpectator_ViewPlayerMonitorVehChange;

systemChat format[">> Spectator:  Viewing %1", name br_teamCurrentViewMemberObject];

true