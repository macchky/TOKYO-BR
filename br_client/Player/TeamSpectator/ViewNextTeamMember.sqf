/*---------------------------------------------------------------------------
STRM: Switch view
	
	This code relies on the other threads maintaining a clean list
		of br_teamSpectatableUnits.

		0 = backward in list
		1 = forward in list

		force = ignore timeouts

---------------------------------------------------------------------------*/
params ["_directionInt", "_force"];

if (!_force && ((diag_tickTime - br_teamViewLastSwitchedTime) < 3)) exitWith {};

br_teamViewLastSwitchedTime = diag_tickTime;

diag_log format["[TEAM SPECTATOR: VIEW NEXT] Requested to change views with args: %1", _this];

call BR_Client_Player_TeamSpectator_UpdateSpectatableUnits;

if ((count br_teamSpectatableUnits) < 1) exitWith
{
	diag_log "[TEAM SPECTATOR: VIEW NEXT] ERROR!  There are no spectatable units!"; 
	false
};

if (isNull br_teamCurrentViewMemberObject || isNil "br_teamCurrentViewMemberObject") exitWith
{
	diag_log "[TEAM SPECTATOR: VIEW NEXT] ERROR!  Player is not spectating!  Current view object is null!";
	false	
};

private _currentViewMemberIndex = br_teamSpectatableUnits find br_teamCurrentViewMemberObject;

diag_log format["[TEAM SPECTATOR: VIEW NEXT] BEFORE: viewing %1", name br_teamCurrentViewMemberObject];

// Adjust 'left' or 'right' to view the next player.
private _nextViewMemberIndex = _currentViewMemberIndex;

if (_directionInt isEqualTo 0) then 
{
	_nextViewMemberIndex = _nextViewMemberIndex - 1;
}
else
{
	_nextViewMemberIndex = _nextViewMemberIndex + 1;
};

// Bounds check, wrap around.  If the 'find' command returned -1, then +/- 1 we're still OK.
if (_nextViewMemberIndex < 0) then {_nextViewMemberIndex = ((count br_teamSpectatableUnits) - 1);};
if (_nextViewMemberIndex >= (count br_teamSpectatableUnits)) then {_nextViewMemberIndex = 0;};

// Update current view object to new player, and switch view
br_teamCurrentViewMemberObject = br_teamSpectatableUnits select _nextViewMemberIndex;

br_teamCurrentViewMemberObject call BR_Client_Player_TeamSpectator_ViewPlayerMonitorVehChange;

systemChat format[">> Spectator:  Viewing %1", name br_teamCurrentViewMemberObject];

diag_log format["[TEAM SPECTATOR: VIEW NEXT] AFTER: viewing %1", name br_teamCurrentViewMemberObject];

true 