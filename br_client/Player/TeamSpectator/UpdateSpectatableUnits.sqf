/*---------------------------------------------------------------------------
STRM: Creates list of spectatable units.
---------------------------------------------------------------------------*/
private _tempSpectatableUnits = [];
{
	private _playerObject = objectFromNetId _x;
	if (player != _playerObject &&
		alive _playerObject && 
		isPlayer _playerObject && 
		side _playerObject == WEST) then 
	{
		_tempSpectatableUnits pushBack _playerObject;
	};
} foreach br_teamMembers;
br_teamSpectatableUnits = + _tempSpectatableUnits;