/*---------------------------------------------------------------------------
STRM: Starts up team spectator mode
---------------------------------------------------------------------------*/
player setVariable ["IsTeamSpectator", true];

if (side player != civilian) then {[player] joinSilent (createGroup CIVILIAN);diag_log "[SPECTATOR] Switched Side";};

diag_log "[TEAM SPECTATOR] Moving body";
private _specfinallocation = [getMarkerPos "spectator",random 50 max 5,random 360,0] call SHK_pos;
player setPos _specfinallocation;
player allowDamage false;

diag_log "[SPECTATOR] Asking server to disable sim.";
["Client_Spectator_Initialize", [netId player, "teamspectator"]] call BR_Client_Network_SendMessage;

[] spawn BR_Client_Player_TeamSpectator_TeamSpectatorSystem;