/*---------------------------------------------------------------------------
STRM: Views a player and monitors for camera changes
---------------------------------------------------------------------------*/
private ["_playerToView"];

_playerToView = _this;

diag_log format["[TEAM SPECTATOR: VIEW NEXT] Setting up camera on %1", name _playerToView];

// Terminate old monitor
if (!isNil "br_teamScriptTeamCameraMonitor") then 
{
	diag_log "[TEAM SPECTATOR: VIEW NEXT] Terminating old cam monitor.";
	terminate br_teamScriptTeamCameraMonitor;
	br_teamScriptTeamCameraMonitor=nil;
};

// Start thread to monitor player vehicle
br_teamScriptTeamCameraMonitor = [_playerToView] spawn 
{
	private ["_playerToView", "_playerVehicle", "_vehTypePast", "_playerVehiclePast", "_playerVehicleNow", "_vehTypeNow"];

	_playerToView = _this select 0;
	_playerVehiclePast = vehicle _playerToView;
	_vehTypePast = typeOf _playerVehiclePast;

	diag_log "[TEAM SPECTATOR: VIEW MONITOR] Monitoring player.";

	while {true} do
	{
		_playerVehiclePast switchCamera "EXTERNAL";

		waitUntil {
			_playerVehicleNow = vehicle _playerToView;
			_vehTypeNow = typeOf _playerVehicleNow;
			!(_vehTypeNow isEqualTo _vehTypePast)
		};

		_playerVehiclePast = _playerVehicleNow;
		_vehTypePast = _vehTypeNow;
	};
};

true