/*---------------------------------------------------------------------------
by Stormridge
Repair a car...  or try to!
---------------------------------------------------------------------------*/
private ["_vehicleToRepair","_nearbyVehicles","_vehicleDamageInfo", "_foundSupportedHitBoxType","_hitboxNameArray","_damageValueArray", "_nearbyAir"];

if (diag_tickTime - br_LastToolkitRepairAttempt > 2) then
{
	if ((vehicle player == player)  &&									// Can't do it inside car...
		 ("ItemBRToolKit" in (magazines player + assignedItems player))	// Has to have the item still
		 && alive player) 												// and alive
		then 
	{
		try {
			// Check for other actions in-progress
	        if (BR_IsPlayerUsingToolkit) throw "Finish repairing this car before repairing again!";
	        if (BR_IsPlayerBoosting) throw "You can't repair a car while using boosters!";
			if (((animationState player) find "medic") >= 0) throw "You can't repair a car while using First Aid!";

	        // Find a vehicle
	        _nearbyVehicles = position player nearObjects ["Car",5];
	        _nearbyAir = position player nearObjects ["Air",7];
	        _nearbyVehicles append _nearbyAir;

	        if (count _nearbyVehicles < 1) throw "You need to be near a vehicle to repair it!";

			_vehicleToRepair = _nearbyVehicles select 0;

			if (damage _vehicleToRepair == 1) throw "This wreck can't be repaired!";

			_vehicleDamageInfo = getAllHitPointsDamage _vehicleToRepair;

			// diag_log format["STRM DEBUG: Found vehicle %1", _vehicleToRepair];
			// diag_log format["STRM DEBUG: 	Vehicle Damage: %1", _vehicleDamageInfo];
			
			if (count _vehicleDamageInfo == 0) throw "This vehicle cannot be repaired.";

			_hitboxNameArray = _vehicleDamageInfo select 0;
			_damageValueArray = _vehicleDamageInfo select 2;

			// Check it out..
			_foundSupportedHitBoxType=false;
			for "_i" from 0 to (count _hitboxNameArray) do {
				if ((_hitboxNameArray select _i) in BR_VEHICLE_REPAIR_ALLOWED_PARTS &&
					(_damageValueArray select _i) > 0) exitWith {_foundSupportedHitBoxType=true};
			};
			if (!_foundSupportedHitBoxType) throw "This vehicle doesn't have tire or engine damage!";

			// OK, repair it.
	        [_vehicleToRepair,_hitboxNameArray] spawn BR_Client_Player_Interaction_Toolkit_RepairVehicle;

	    } catch {
	    	systemChat _exception;
		};
    };
	br_LastToolkitRepairAttempt = diag_tickTime;
};