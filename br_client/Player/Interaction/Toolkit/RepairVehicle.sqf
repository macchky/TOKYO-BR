/*---------------------------------------------------------------------------
by Stormridge - Repairs a vehicle
---------------------------------------------------------------------------*/
private ["_vehicleToRepair","_mags", "_hasRepaired","_hitboxNameArray"];

_vehicleToRepair=_this select 0;
_hitboxNameArray=_this select 1;

if(BR_IsPlayerUsingToolkit) exitWith {false};
BR_IsPlayerUsingToolkit=true;

try 
{
	_hasRepaired = false;

    player removeMagazine "ItemBRToolKit";

    if !(7 call BR_Client_Player_Interaction_Toolkit_ToolkitAnimation) then 
    {
        player addMagazine "ItemBRToolKit";
        throw "Repair canceled.";
    };

    // Request server to find the car's owner and repair it.
	["Client_Interaction_RepairVehicleRequest", [netId _vehicleToRepair,_hitboxNameArray]] call BR_Client_Network_SendMessage;

	// Done
    BR_IsPlayerUsingToolkit = false;
} 
catch
{
		BR_IsPlayerUsingToolkit = false;
		systemChat _exception;
};