/*---------------------------------------------------------------------------
Keeps player info updated so external threads can update things like UI

Also handles inventory scanning
---------------------------------------------------------------------------*/
private ["_health", "_playerAllHitPointsDamage", "_playerHitPtsArray", "_forceStatsUpdate"];
_forceStatsUpdate=_this;
if ((diag_tickTime - BR_Client_Damage_PlayerLastStatsUpdate) > 1) then
{
	_health = damage player;
	
	BR_Client_Damage_PlayerAttributes set [0, _health];
	BR_Client_Damage_PlayerAttributes set [1, br_Boost];

	if (_forceStatsUpdate || ((diag_tickTime - BR_Client_Damage_PlayerLastStatsUpdate_HitPoints) > 1.1)) then
	{
		BR_Client_Damage_PlayerAttributes set [3, (getAllHitPointsDamage player)];
		BR_Client_Damage_PlayerAttributes set [2, 0];	// assume no issues with hitboxes
		{
			if (_x >= .1) exitWith {BR_Client_Damage_PlayerAttributes set [2, 1];};	// Flag damage at 0.1
		} foreach ((BR_Client_Damage_PlayerAttributes select 3) select 2);
		
		BR_Client_Damage_PlayerLastStatsUpdate_HitPoints = diag_tickTime;		
	};

	// Inventory Scanning  (e.g.  custom first aid stuff will go here)
	if (diag_tickTime - BR_Client_Damage_PlayerLastStatsUpdate_Inventory > 2.5) then {
		// Toolkit
		if ("ItemBRToolKit" in (magazines player + assignedItems player) && alive player) then  // && (damage player > 0)  <- for first aid kits, when we get around to it.
		{
			if (BR_Client_Damage_Hud_ActionTrack_Toolkit < 0) then 
			{
				BR_Client_Damage_Hud_ActionTrack_Toolkit = player addAction ["Repair nearby vehicle", {call BR_Client_Player_Interaction_ActionVehicleToolkitRepair}, "", 1.5, false];
			};
		} 
		else 
		{
			// Remove Action
			if (BR_Client_Damage_Hud_ActionTrack_Toolkit >= 0) then 
			{
				player removeAction BR_Client_Damage_Hud_ActionTrack_Toolkit;
				BR_Client_Damage_Hud_ActionTrack_Toolkit=-1;
			};
		};
		BR_Client_Damage_PlayerLastStatsUpdate_Inventory = diag_tickTime;
	};
	// Last line
	BR_Client_Damage_PlayerLastStatsUpdate = diag_tickTime;
};
true