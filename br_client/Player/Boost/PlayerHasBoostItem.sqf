/**
* Verify whether the player is able to consume a booster of any type
*/
private _mags = magazines player;
("ItemRedgull" in _mags || "ItemPainkiller" in _mags)