/*---------------------------------------------------------------------------
Stormridge: Basic boost management.
---------------------------------------------------------------------------*/
private ["_damage", "_hitPointsArray", "_startDamage", "_lastBoost"];
br_Boost = 100;
br_TargetHPIncreasePerBoost = .13;
_lastBoost = -1;
while{true} do {
	
	uiSleep 3; //4;
	br_Boost = br_Boost - 1;
	if (br_Boost <= 0) then {
		br_Boost = 0;
	};
	
	if (alive player && br_Boost > 0) then 
	{
		// 1. Snapshot current state
		_startDamage = damage player;
		_hitPointsArray = (getAllHitPointsDamage player) select 2;
		// 2. Update overall damage value
		if (_startDamage > 0) then 
		{
			player setDamage (_startDamage - linearConversion[0,100,br_Boost,BR_BoostHealPerTick,BR_BoostHealPerTick*2,true]);
			if (damage player < 0) then { player setDamage 0;};
		};
		// ? Make sure Player is NOT invulnerable (e.g. Firing Range start zone) - opting not to check to keep this fast.
		// 3. Heal body parts and replace damage screwed up by setDamage
		{
			_x = _x - linearConversion[0,100,br_Boost,BR_BoostHealPerTick,BR_BoostHealPerTick*2,true];
			if (_x < 0) then {_x=0;};
			player setHitIndex[_forEachIndex,_x];
		} foreach _hitPointsArray;
		// 4. Apply Advantages
		// 	  Heal hands to reduce sway.  Arms have no effect.
		if (br_Boost > 90) then 
		{
			if ((_hitPointsArray select 9) > 0) then 
			{ 
				player setHitIndex[9,0];
			};
		};
		//   Heal Legs, for movement
		if (br_Boost > 40) then
		{
			if ((_hitPointsArray select 10) >= .5) then 
			{
				player setHitIndex[10,.4];	// Can Walk at 0.4
			};
		};			
	};

	if (br_public_isSpectatorInGame && _lastBoost != br_Boost) then {
		player setVariable ["br_BoostVar", br_Boost, true];
	};
	_lastBoost = br_Boost;
};