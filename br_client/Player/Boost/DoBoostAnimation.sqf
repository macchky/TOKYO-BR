/**
 * Animate the boosting process.
 * @param endOfAnimationTime The duration of the animation.
 * @returns true if the animation has not been canceled during the given endOfAnimationTimeframe
 */
private ["_stance", "_time", "_success"];
BR_CancelAnim = false;
_stance = animationState player;
_time = time + _this;
_success = true;

player playActionNow "Medic";

_eh = (findDisplay 46) displayAddEventHandler ["KeyDown",{
	if((_this select 1) in BR_PlayerAnimationCancellationMovements) then {
		BR_CancelAnim = true;
	};
	false
}];

// Added to fix when players accidentally FirstAidKit while in middle of boosting-which causes the action menu to disappear forever.
// Stormridge: Jan23.2016 - Fixed mouse button lookup, and translation of mouse buttons to DIK codes.
_mouseEH = (findDisplay 46) displayAddEventHandler ["MouseButtonDown", {
	if((BR_MOUSE_DIK_LOOKUP select (_this select 1)) in BR_PlayerAnimationCancellationMovements) then {
		BR_CancelAnim = true;
	};
	false
}];
_mouseEH2 = (findDisplay 46) displayAddEventHandler ["MouseButtonClick", {
	if((BR_MOUSE_DIK_LOOKUP select (_this select 1)) in BR_PlayerAnimationCancellationMovements) then {
		BR_CancelAnim = true;
	};
	false
}];

waitUntil{time >= _time or BR_CancelAnim};

anim_cancel = [netId player,_stance];
publicVariable "anim_cancel";
player switchMove _stance;

(findDisplay 46) displayRemoveEventHandler ["KeyDown",_eh];
(findDisplay 46) displayRemoveEventHandler ["MouseButtonDown",_mouseEH];
(findDisplay 46) displayRemoveEventHandler ["MouseButtonClick",_mouseEH2];

if(BR_CancelAnim) then {
	_success = false;
};
_success;