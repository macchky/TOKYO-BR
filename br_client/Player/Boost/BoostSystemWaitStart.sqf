/* 
	By PLAYERUNKNOWN and BR Devs

Updated Jan9 2016 Stormridge:
	- Fixed med kit and boost interaction issues, which could cause Action Menu to disappear forever
	- Attempts to detect simultaneous use of FirstAidKits and Boost Items, disallows it.
	- Will now cancel boosting if use tries to FirstAidKit from action menu.  (also causes action menu issues)
	- Adds boost spam protection (1 second delay required between boost attempts)
	- New logic for boost application thread
		Rules:
			- Each boost item provides 40 boost
			- Boost > 90 (player had 2 boosters) will provide sway reduction & advantage  ('insta-heal' hands)
			- Boost > 40 (player had 2 boosters) will heal legs to the point the player can walk, simulating ability to run while injured
			- Medkitting to 75% and two boosters will get you to full health.
			-	Prior requirement means boosters will heal about 13% per hitbox, per booster
			- Boost ticks every 3 seconds, for 120 seconds of boost. (2 mins)
			- .13/40 = .00325 healed per tick
*/
/////////////////////// Global Boost variables
waitUntil {br_deploy_players};

/////////////////////// Initialize
waitUntil{!isNull player};
waitUntil{alive player};
sleep 3;
diag_log "[BOOST] Loading";
1 cutRsc ["br_RscDisplayPlayerStatus","PLAIN"];


[] spawn BR_Client_Player_Boost_BoostLevelManager;

[] spawn BR_Client_GUI_HPBar_MonitorHPAndBoost;