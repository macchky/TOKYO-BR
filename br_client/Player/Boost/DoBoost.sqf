BR_IsPlayerBoosting = true;
if(vehicle player != player) exitWith {BR_IsPlayerBoosting = false};
try {
	private ["_mags", "_boostItem", "_hasBoosted"];
	    
	_hasBoosted = false;
	_boostItem = "";
	_mags = magazines player;

    if ("ItemRedgull" in _mags) then
    {
		_boostItem = "ItemRedgull";
    }
	else
    {
		_boostItem = "ItemPainkiller";
    };
    if (_boostItem isEqualTo "") throw "You need boost items in order to boost!";
    
    player removeMagazine _boostItem;

    if !(6.4 call BR_Client_Player_Boost_DoBoostAnimation) then {
        player addMagazine _boostItem;
        throw "Boost canceled";
    };

    br_Boost = (br_Boost + 40) min 100;
    BR_IsPlayerBoosting = false;
} 
catch
{
	BR_IsPlayerBoosting = false;
	systemChat _exception;
};