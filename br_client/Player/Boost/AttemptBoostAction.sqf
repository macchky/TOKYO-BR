/*---------------------------------------------------------------------------
Old boost code
---------------------------------------------------------------------------*/
if (diag_tickTime - BR_LastBoostAttempt > 1) then
{
	// Non-warnable items
	if ((vehicle player == player) && (call BR_Client_Player_Boost_PlayerHasBoostItem) && (br_Boost < 100)) then 
	{
		// Warnable items, which result in player message
		try {
			if (br_isPlayerPerformingSeriesAction) throw "Complete your other action before attempting boost.";
	        if (BR_IsPlayerBoosting) throw "Finish this boost before using another booster.";
	        if (call BR_Client_Player_Util_isPlayerMedkitting) throw "You can't boost while using First Aid.";
	        [] spawn BR_Client_Player_Boost_DoBoost;
	    } catch {
	    	systemChat _exception;
		};
    };
	BR_LastBoostAttempt = diag_tickTime;
};
