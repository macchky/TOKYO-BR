/*---------------------------------------------------------------------------
by Stormridge
- Quick and Dirty, Limited repack system
---------------------------------------------------------------------------*/
private _canRepackMags=false;

STRM_MagRepack_CountPlayersMagsOfType = {
	_class = _this select 0;
	_playersMags = _this select 1;
	_result = 0;
	{
		if ((_x select 0) isEqualTo _class) then {
			_result = _result + (_x select 1);
		};
	} foreach _playersMags;
	_result
};

if (diag_tickTime - br_lastMagRepackAttempt > 1) then
{
	// Non-warnable items
	if ((vehicle player == player)) then 
	{
		// Warnable items, which result in player message
		try {
			if (br_isPlayerPerformingSeriesAction) throw "Finish current action before attempting mag repack.";
	        if (BR_IsPlayerBoosting) throw "Finish boosting before trying another action.";
	        if (call BR_Client_Player_Util_isPlayerMedkitting) throw "You can't boost while using First Aid.";
	        _canRepackMags=true;
	    } catch {
	    	systemChat _exception;
		};
    };
	br_lastMagRepackAttempt = diag_tickTime;
};
if (_canRepackMags) then {
    br_isPlayerPerformingSeriesAction=true;
    if(vehicle player != player) exitWith {br_isPlayerPerformingSeriesAction=false};
	

	try {
   	    // Have any mags?
   	    _playersMags = magazinesAmmoFull player;
   	    //diag_log format["[MAGS] Players mags are: %1",_playersMags];
   	    if (count _playersMags < 1) throw "You don't have any magazines to pack!";

   	    systemChat "Attempting to pack magazines...";
   	    
		_timeStartAnimation = diag_tickTime;
		player playActionNow "medicOther";

   	    // Filter
   	    _magClassesToPack = [];
   	    {
   	    	_class = _x select 0;
   	    	_ammoCount = _x select 1;
   	    	_state = _x select 2;
   	    	_type = _x select 3;
   	    	_loc = _x select 4;

   	    	_thisMagCapacity = getNumber (configFile >> 'CfgMagazines' >> _class >> 'count');

   	    	if (_type in [-1,1,2] &&
   	    		_thisMagCapacity >= 5 &&
   	    		_ammoCount < _thisMagCapacity) then {_magClassesToPack pushBackUnique _class};

   		} forEach _playersMags;

   		// Sponge em up
		{
			// Count up ammo for this mag type (including primary/secondary)
			_class = _x;
			_magClassCapacity = getNumber (configFile >> 'CfgMagazines' >> _class >> 'count');
			_totalAmmoOfThisClass = [_class,_playersMags] call STRM_MagRepack_CountPlayersMagsOfType;
			// Delete all mags of this type (including primary/secondary) -- remember if took from weapon?
			player removeMagazines _class;
			{
				if (_x isEqualTo _class) then {
					player removePrimaryWeaponItem _class;
				};
			} foreach primaryWeaponMagazine player;
			{
				if (_x isEqualTo _class) then {
					player removeHandgunItem _class;
				};
			} foreach handgunMagazine player;
			// Add mags of this type according to result.
			_unpackedAmmoRemaining = _totalAmmoOfThisClass;
			while {_unpackedAmmoRemaining > 0} do {
				if (_unpackedAmmoRemaining > _magClassCapacity) then {
					player addMagazine _class;
					_unpackedAmmoRemaining = _unpackedAmmoRemaining - _magClassCapacity;
				} else {
					player addMagazine [_class, _unpackedAmmoRemaining];
					_unpackedAmmoRemaining = 0; 
				};
			};
		} forEach _magClassesToPack;

		// Spawn timer that frees up the player
		_timeStartAnimation spawn {
			waitUntil {diag_tickTime - _this > 7};
		    br_isPlayerPerformingSeriesAction = false;
		    systemChat "Mag Repack Complete.";
		};
	} 
	catch
	{
		br_isPlayerPerformingSeriesAction = false;
		systemChat _exception;
	};
};