private ["_newPlayerObject","_oldPlayerObject"];

_newPlayerObject = _this;

diag_log "Received new player notification from server.  Moving in";

_oldPlayerObject = player;
player reveal [_newPlayerObject, 4]; // clamped to 0...4 range
selectPlayer _newPlayerObject;

diag_log "Moved in to new player object...";

if (_oldPlayerObject isKindOf "VirtualMan_F") then {deleteVehicle _oldPlayerObject};

if (player isKindOf "VirtualMan_F") then
{ 
	["There was a problem spawning!","Please try reconnecting!"] spawn BIS_fnc_guiMessage;
	cutText ["", "BLACK IN", 1];
	endLoadingScreen;
	1 fadeSound 1;
	disableUserInput false;
	endMission "END6";
	player setVariable ["PUBR_isover",true,true];
};

player setVariable ["BIS_noCoreConversations", true];
player setSpeaker "NoVoice";
player disableConversation true;
player enableFatigue false;
enableSentences false;
enableRadio false;
showSubtitles false;
player setCustomAimCoef 0.000;
player enableStamina false;
player allowSprint true;

player addEventHandler ["GetInMan", { _this call BR_Client_Player_Events_onGetInMan }];
player addEventHandler ["Fired",{ _this call BR_Client_Player_Events_onFired }];
player addEventHandler ["Hit",{ _this call BR_Client_Player_Events_onHit }];		
player addEventHandler ["Killed", { _this call BR_Client_Player_Events_onKilled }];
player addEventHandler ["Respawn", { _this call BR_Client_Player_Events_OnRespawn }];

setGroupIconsVisible [false,false];	// no groups

disableUserInput false;	// Toggle input for bugz
disableUserInput true;
disableUserInput false;

br_BRPlayerRequestComplete=true;	// this will trigger the FSM.

[] execVM "ClientSidePlayerInit.sqf";	// Run this file, which should be in the mission from the server.  It will set br_ClientSidePlayerInitComplete=true, when complete.

true