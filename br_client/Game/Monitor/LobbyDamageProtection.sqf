/*---------------------------------------------------------------------------
Protects player until game start
---------------------------------------------------------------------------*/
waitUntil {!isNil "br_deploy_players"};
waitUntil {br_deploy_players};
uiSleep 30;
while {true} do 
{
	if(str(side player) == "CIV") exitWith {};
	player allowDamage true;
	uiSleep 30;
};