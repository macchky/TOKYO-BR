//Code by Killzone Kid & Lystic 
// STRM: Updated to free up EHs after lobby is over.
private ["_donators","_playerID","_br_playerArray","_rank"];

br_VoN_DisplayEH = [];		// Tracks EH so we can free them up later.

[] spawn {
	
	
	VoN_ChannelId_fnc = {
		switch _this do {
			case localize "str_channel_global" : {["str_channel_global",0]};
			case localize "str_channel_side" : {["str_channel_side",1]};
			case localize "str_channel_command" : {["str_channel_command",2]};
			case localize "str_channel_group" : {["str_channel_group",3]};
			case localize "str_channel_vehicle" : {["str_channel_vehicle",4]};
			case localize "str_channel_direct" : {["str_channel_direct",5]};
			default {["",-1]};
		}
	};
	VoN_Event_fnc = {
		VoN_currentTxt = _this;
		VoN_channelId = VoN_currentTxt call VoN_ChannelId_fnc;
		PLAYER_SPEAKS_PV = [netId player] + VoN_channelId;
		publicVariable "PLAYER_SPEAKS_PV";
	};

	0 = [] spawn {
		
		
		VoN_isOn = false;
		VoN_currentTxt = "";
		VoN_channelId = -1;
		_fncDown = {
			if (!VoN_isOn) then {
				if (!isNull findDisplay 55 && !isNull findDisplay 63) then {
					VoN_isOn = true;
					ctrlText (findDisplay 63 displayCtrl 101)
					call VoN_Event_fnc;
					findDisplay 55 displayAddEventHandler ["Unload", {
						VoN_isOn = false;
						"" call VoN_Event_fnc;
					}]; 
				};
			};
			false
		};
		_fncUp = {
			if (VoN_isOn) then {
				_ctrlText = ctrlText (findDisplay 63 displayCtrl 101);
				if (VoN_currentTxt != _ctrlText) then {
					_ctrlText call VoN_Event_fnc;
				};
			};
			false
		};

		waitUntil {!isNull findDisplay 46};

		private _id = (findDisplay 46) displayAddEventHandler ["KeyDown", _fncDown];
		br_VoN_DisplayEH pushBack [_id, "KeyDown"];

		_id = (findDisplay 46) displayAddEventHandler ["KeyUp", _fncUp];
		br_VoN_DisplayEH pushBack [_id, "KeyUp"];

		_id = (findDisplay 46) displayAddEventHandler ["MouseButtonDown", _fncDown];
		br_VoN_DisplayEH pushBack [_id, "MouseButtonDown"];

		_id = (findDisplay 46) displayAddEventHandler ["MouseButtonUp", _fncUp];
		br_VoN_DisplayEH pushBack [_id, "MouseButtonUp"];

		_id = (findDisplay 46) displayAddEventHandler ["JoystickButton", _fncDown];
		br_VoN_DisplayEH pushBack [_id, "JoystickButton"];
	};
	
	//-----
	waitUntil {!isNil {player getVariable "br_rank"}};
	addMissionEventHandler["Draw3D",{
		{	
			_rank = _x getVariable "br_rank";
			_r = _rank select 0;
			_g = _rank select 1;
			_b = _rank select 2;
			_title = _rank select 3;
			_global = _rank select 4;
			_wins = _rank select 5;
			_pos = getposatl _x;
			_pid = getPlayerUID _x;
			_multi = 0.15;
			if (player != _x) then {
				_dist = player distance _x;
				_multi = 0.16 + (_dist / 100);
			};	
			_eyepos = ASLtoATL eyepos _x;
			_eyepos2 = ASLtoATL eyepos _x;
			_3 = _x modelToWorld [-0.5,0,0];
			_4 = _x modelToWorld [-0.5,0,0];
			_3 set [2,(_eyepos select 2) + 0.25];
			_4 set [2,(_eyepos2 select 2) + 0.25];
			_fontsize = 0.03;
			_eyepos set [2,(_3 select 2) + _multi];
			_eyepos2 set [2,(_4 select 2) + 0.10];
			_color = [parseNumber _r,parseNumber _g,parseNumber _b,0];
			_color2 = [1,1,1,1];
			//--- If von is on
			if(_x getVariable ["Speaking",false]) then {
				_color = [0,0,1,1];
				_color2 = [0,0,1,1];
			} else {
				//if !(_pid in br_donators) then {
					_color = [parseNumber _r,parseNumber _g,parseNumber _b,1];
					_color2 = [1,1,1,1];
				//};			
			};
			drawIcon3D["",_color2,_eyepos,0,0,0,format["%1 #%2",name _x,_global],1,_fontsize,'EtelkaNarrowMediumPro'];
			drawIcon3D["",_color,_eyepos2,0,0,0,format["%1 - %2 WINS ",_title,_wins],1,_fontsize,'EtelkaNarrowMediumPro'];
		} forEach allPlayers;
	}];
	"PLAYER_SPEAKS_PV" addPublicVariableEventHandler {
		_spkr = objectFromNetId (_this select 1 select 0);
		_chTxt = localize (_this select 1 select 1);
		_chID = _this select 1 select 2;
		if(_chID == 5) then {
			_spkr setVariable ["Speaking",true,false]; 
		};
		if(_chID < 0) then {
			_spkr setVariable ["Speaking",false,false];
		};
	};

	// STRM:  Added side check, so late joining spectators don't have to deal with crap overlays
	//			This is due to a bug in how public variables are set by the server, and then how ClientSideInit.sqf sets default variables values,
	//				which override whatever the JIP value was.
	waitUntil{br_game_started || (str(side player) == "CIV")};
	removeAllMissionEventHandlers "Draw3D";

	// STRM: Remove EHs
	{
		if ((_x select 0) != -1) then {
			(findDisplay 46) displayRemoveEventHandler [_x select 1, _x select 0];
		};
	} foreach br_VoN_DisplayEH;
	
};