/*---------------------------------------------------------------------------
old code to lock player
---------------------------------------------------------------------------*/
private ["_currentPos","_initialPos"];

_initialPos = br_startPosition;

if (worldName isEqualTo "tanoa") then
{
	while{!br_game_started} do
	{
		if (br_deploy_players) exitWith {};
	    _currentPos = position player;
	    if (_initialPos distance2D _currentPos > 85) then {	player setpos ([_initialPos,(random 30 max 10),random 360,0] call SHK_pos);};
	    uiSleep 2;
	};
} 
else
{
	while{!br_game_started} do
	{
		
		if (br_deploy_players) exitWith {};
	    
	    _currentPos = position player;
	    
	    if (_initialPos distance2D _currentPos > 55 && 
	    	!(player getVariable["IsSpectator",false]) && 
	    	!(player getVariable["IsTeamSpectator",false])) then 
	    {
	        player setpos ([_initialPos,(random 30 max 10),random 360,0] call SHK_pos);
	    };

	    uiSleep 2;
	};
};
true 