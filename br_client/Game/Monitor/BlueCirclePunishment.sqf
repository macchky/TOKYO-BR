/*---------------------------------------------------------------------------
Old code to monitor blue circles
---------------------------------------------------------------------------*/
// Script by Wa4go & lazyink. Please request permission for use!
// Rewritten by STRM

private ["_damage","_isPlayerOutside","_finishPos","_zone","_punishDamage", "_isBackIn", "_justPunished"];

br_PunishPlayer = {
    (_this select 0) call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;
    _damage = getDammage player;
    player setDammage (_damage + (_this select 1));
};

waitUntil{!isNil "br_zone_started"};

_justPunished=false;

while {true} do {

    _finishPos = getMarkerPos "playArea";
    _zone = zoneSize;
    _isPlayerOutside = player distance2D _finishPos > _zone;    // STRM: Converted to distance2D
	_punishDamage = br_punishDamage;

    // STRM: Let's not do this.  Commented out!
    // if (_justPunished) then {
    //     _time = time + br_punishDelay + 6; // STRM: Player was punished on last circle check.  Give additional 6 seconds for medkit
    // } else {
    //    _time = time + br_punishDelay;
    //};

    _time = time + br_punishDelay;

    //If the player is outside then warn him
    _justPunished=false;
    _isBackIn=false;
    if(_isPlayerOutside && (str(side player) != "CIV") && br_zone_started && !(player getVariable ["IsSpectator", false]) && !(player getVariable ["IsTeamSpectator", false])) then {
        
        "YOU ARE OUTSIDE THE PLAY ZONE!" call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;
		
        // For 20 seconds, see if they get back into the CURRENT circle (which may move while we wait...)
		while {time < _time} do {
            uiSleep 7;  // Don't check too often, so they can't bounce in and out.
            // STRM:  See if they go back in the circle.  If so, reset the punishment.  Otherwise you can come back in then get suprised by a hit 20 sec later.
            if (player distance2D _finishPos <= _zone) exitWith {_isBackIn=true};
        };

        // They got back into the original circle, at least...
        // Did they get back in, AND is the circle still in effect?  If so, message and exit this context
        if (_isBackIn && br_zone_started) exitWith {"YOU ARE BACK IN THE ZONE!" call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;};

        // So they either got back in the circle after it shifted (which doesn't count)
        //      Or the circle didn't shift and they simply never got in the circle.

        // Did circle resize during that wait?  Doesn't matter what isBackIn's value.  They either were in the old circle before it moved, or never came back inside old circle radius.
        if !(br_zone_started) then {
            // Player left circle early OR never got in the circle!  However, the circle has moved!
            ["YOU WERE NOT IN THE PLAY ZONE BEFORE IT MOVED!",_punishDamage] call br_PunishPlayer;
            _justPunished=true; // Denote that player was punished, for the next loop
        } else {
            // Circle is still being enforced AND the player is outside
            ["YOU ARE STILL OUTSIDE THE PLAY ZONE! MOVE BACK INSIDE NOW!",_punishDamage] call br_PunishPlayer;
            _justPunished=true;
        };
        uiSleep 1;
    };
	uiSleep 1;
};
