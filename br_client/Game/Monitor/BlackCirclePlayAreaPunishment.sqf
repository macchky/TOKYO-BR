/*---------------------------------------------------------------------------
Old code to monitor play area
---------------------------------------------------------------------------*/
// Script by Wa4go & lazyink. Please request permission for use!
// Rewritten by STRM

private ["_damage","_isPlayerOutside","_finishPos","_zone","_punishDamage"];

fnc_br_delete_kill = {
    "YOU ARE BACK IN THE GAME AREA!" call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;
};

waitUntil{br_kill_zone};

while {true} do {

    _finishPos = getMarkerPos "center";
    _zone = br_gameAreaRadius;
    _isPlayerOutside = player distance2D _finishPos > _zone;    // STRM: Converted to distance2D
	_punishDamage = br_killDamage;
	_time = time + br_killDelay;
	    
    //If the player is outside then warn him
    if(_isPlayerOutside && (str(side player) != "CIV") && !(player getVariable ["IsSpectator", false]) && !(player getVariable ["IsTeamSpectator", false])) then {
        "YOU ARE OUTSIDE THE GAME AREA!" call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;
		
		while {time < _time} do {
            uiSleep 1;
        };        
        //If the player is still outside then punish him
        _isPlayerOutside = player distance2D _finishPos > _zone;    // STRM: Converted to distance2D
        if(_isPlayerOutside) then {
            "YOU ARE STILL OUTSIDE THE GAME AREA! MOVE BACK INSIDE NOW!" call BR_Client_GUI_Notifications_NotifyPlayerUrgentMessage;
			_damage = getDammage player;
			player setDammage (_damage + _punishDamage);
        } else {            
            [] call fnc_br_delete_kill;
        };        
        uiSleep 1;
    };
	uiSleep 1;
};