/*---------------------------------------------------------------------------
Older code that does screen shaking N stuff
---------------------------------------------------------------------------*/
enableCamShake true;

_power = 2.1;
_durationShake = 20;
_compensation = 8;
_frequency = 40;
_offset = 0.1;

_object = player;
if(!isNil "spawnplane") then {
	_object = spawnplane;
};
_pos = getPosATL _object;

_light = "#lightpoint" createVehicleLocal _pos;
_light setposatl [_pos select 0,_pos select 1,(_pos select 2) + 10];
_light setLightDayLight true;
_light setLightBrightness 300;
_light setLightAmbient [0.05, 0.05, 0.1];
_light setlightcolor [1, 1, 2];		
uiSleep 0.1;
_light setLightBrightness 0;
uiSleep (random 0.1);	

"DynamicBlur" ppEffectEnable true;
"DynamicBlur" ppEffectAdjust [_power/2];
"DynamicBlur" ppEffectCommit _offset;
uiSleep _offset;
"DynamicBlur" ppEffectAdjust [0];
"DynamicBlur" ppEffectCommit (_durationShake - _compensation);

addCamShake [_power, _durationShake, _frequency];

_lightning = (["lightning1_F","lightning2_F"] call bis_Fnc_selectrandom) createVehicleLocal [100,100,100];
_lightning setdir (random 360);
_lightning setposatl _pos;	
_duration = (2 + random 1);				
for "_i" from 0 to _duration do {
	_time = time + 0.1;
	_light setLightBrightness (50 + random 100);
};

deletevehicle _lightning;
deletevehicle _light;