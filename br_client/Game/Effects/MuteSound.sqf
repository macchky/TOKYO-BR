/*---------------------------------------------------------------------------
Old sound code
---------------------------------------------------------------------------*/
if(soundVolume != 1) then {
	1 fadeSound 1;
	hintSilent "Removing Earplugs";
} else {
	1 fadeSound 0.3;
	hintSilent "Inserting Earplugs";
};