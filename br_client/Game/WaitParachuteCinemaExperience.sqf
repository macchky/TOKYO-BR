/*---------------------------------------------------------------------------
Older code that does parachuting on Wake/SF, and cinema effects
---------------------------------------------------------------------------*/
waitUntil{!isNil "br_date"};
setDate br_date;
player allowDamage false;
_map = worldName;
waitUntil {br_game_started};
showGPS false;
showMap false;
showHUD false;
uiSleep 8;
[0,1.5,false] call BIS_fnc_cinemaBorder;
private ["_ppGrain"];
_ppGrain = ppEffectCreate ["filmGrain", 2012];
_ppGrain ppEffectEnable true;
_ppGrain ppEffectAdjust [0.1, 1, 1, 0, 1];
_ppGrain ppEffectCommit 0;		
_hasPlane = true;
if (_map == "wake") then {
	player addBackpack "B_Parachute";	
	_spawnPos = [getMarkerPos "center",random 2000 max 750,random 360,0] call SHK_pos;
	player setPosASL [ _spawnPos select 0, _spawnPos select 1,(_spawnPos select 2) + 2300];
	_hasPlane = false;
};
if (br_isGameStreetFight) then {
	_spawnZone = br_gameAreaRadius - 400;
	player addBackpack "B_Parachute";
	_spawnPos = [getMarkerPos "center",_spawnZone,random 360,0] call SHK_pos;
	player setPosASL [ _spawnPos select 0, _spawnPos select 1,(_spawnPos select 2) + 2400];
	_hasPlane = false;
};

waitUntil {br_alert};

0 call BR_Client_Game_Effects_ShakeScreen;
uiSleep 8;
0 call BR_Client_Game_Effects_ShakeScreen;

waitUntil {br_deploy_players};
showGPS true;
showHUD true;
showMap true;
uiSleep 3;
[1,1.5,false] call BIS_fnc_cinemaBorder;
uiSleep 1.5;
ppEffectDestroy _ppGrain;

//[player] spawn fnc_boost;

while {true} do {
	if (((getPosATL player) select 2) < 200) then {
		player allowDamage true;
	};
};
