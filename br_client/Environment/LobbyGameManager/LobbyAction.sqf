/*---------------------------------------------------------------------------
STRM: Ask server to perform a lobby action on our behalf
---------------------------------------------------------------------------*/
["Client_Interaction_LobbyGameManagerRequest", [_this]] call BR_Client_Network_SendMessage;