/*---------------------------------------------------------------------------
STRM:  Creates an AI that can assist managing game settings within the lobby
---------------------------------------------------------------------------*/
private ["_group", "_unitCoords", "_unitDir", "_unit"];

//_group = createGroup west;
_unit = "B_BattleRoyalePlayer_F" createVehicleLocal [0,0,0];
_unit setVariable ["BIS_enableRandomization", false];
_unit addUniform "U_C_Scientist";
_unit addHeadgear "H_Beret_Colonel";
_unit setName "Virtual Stormridge";

_unitCoords = [0,0,0];
_unitDir = 0;

switch (worldName) do {
    case "altis": { _unitCoords = [5466.48,15008.2,0]; _unitDir = 127; };
    case "stratis": { _unitCoords = [2629.24,597.246,0.595436]; _unitDir = 357; };
    case "bozcaada": { _unitCoords = [12648.3,11588.4,0]; _unitDir = 355; };
    default { diag_log "ERROR: Map not defined in CreateLobbyGameManager.  Using start marker pos.";  _unitCoords= getMarkerPos "start"; _unitDir = 0;};
};

_unit setPosATL _unitCoords;
_unit setDir _unitDir;
_unit switchMove "amovpercmstpsnonwnondnon_salute";
{_unit disableAI _x} forEach ["AUTOTARGET", "FSM", "MOVE", "TARGET"];

_unit setVariable ["Oracle", true];

_unit allowDamage false;



// admin
if (_this == 0) then {
	_unit addAction ["Start game", {"Start game" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["Skip time", {"Skip time" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["Fog", {"Fog" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["No Fog", {"No Fog" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["Full Rain", {"Full Rain" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["No Rain", {"No Rain" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
} else {
	// streamer admin
	_unit addAction ["Start game", {"Start game" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["Skip time by 6 hours", {"Skip time" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["Fog", {"Fog" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["No Fog", {"No Fog" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["Full Rain", {"Full Rain" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];
	_unit addAction ["No Rain", {"No Rain" spawn BR_Client_Environment_LobbyGameManager_LobbyAction}, "", 3, true];	
};

_unit