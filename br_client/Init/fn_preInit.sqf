/*
https://community.bistudio.com/wiki/Initialization_Order
*/
private ["_compiledCode","_sqfFile"];

#include "..\Functions.hpp";

// If the server wants to override some local functions, do it here:
if (missionName == "battleroyale") then {
	call compile preprocessFileLineNumbers "serverClientFunctionsOverride.sqf";
};

// Then define client functions
{
	_sqfFile = _x select 1;
	_compiledCode = compileFinal (preprocessFileLineNumbers _sqfFile);
	missionNamespace setVariable [_x select 0, _compiledCode];
} forEach BR_CLIENT_PREDEFINED_FUNCTIONS;

// Used by pause screen, to display the news.
if (missionName == "battleroyale") then {
	uiNamespace setVariable ["Client_OnMPPause_NewsFeed", BR_Client_GUI_MenuNewsFeed];
};

call BR_Client_Init_PreInit;
true