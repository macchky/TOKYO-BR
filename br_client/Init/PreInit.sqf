diag_log format["[CLIENT PRE-INIT] Mission is %1", missionName];

#include "..\Globals.hpp"

if (missionName == "battleroyale") then {
	
	call compile preprocessFileLineNumbers "battleroyale_setup.sqf"; // STRM: This needs to be defined before the client FSM starts, such that we can use the correct FSM depending on game type.
	call BR_Client_External_SHK_pos_shk_pos_init;		

	if (hasInterface) then 
	{
		diag_log "[CLIENT PRE-INIT] Game type appears to be BATTLE ROYALE!";

		2 fadeSound 0;	// Fixes glitch where you hear sound before FSM can take over.
		
		[] call BR_Client_GUI_HUDInitialize;

		if (isMultiplayer) then
		{
			[] spawn {
				waitUntil{!(isNull player)};
				[] execFSM "br_client\FSM\BR_Client_JoinGame.fsm";
			};
		};
	};
}
else
{
	diag_log format["[CLIENT PRE-INIT] Game type is not BATTLE ROYALE!  (%1)     Initializing HUD.", missionName];
	[] call BR_Client_GUI_HUDInitialize;
};