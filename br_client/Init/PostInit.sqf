if (hasInterface) then 
{
	diag_log "BR_Client_Init_PostInit";
	
	if (missionName == "battleroyale") then {
		if (!isServer) then {

			diag_log "[CLIENT POST-INIT] Starting notification managers";

			call BR_Client_GUI_Notifications_NotificationManager;
			call BR_Client_GUI_Notifications_KillMessageNotificationManager;
			call BR_Client_Network_PublicVariables_InitializePVAREH;
			
			[] spawn {
				waitUntil {vehicle player == player && ((typeOf player) isEqualTo "B_BattleRoyalePlayer_F")};
				while {true} do 
				{
					false call BR_Client_Player_PlayerInfoUpdateTask;
					sleep .5;
				};
			};
		};
	};

	if (missionName == "Updates BR Firing Range") then {
		[] spawn {
			waitUntil {vehicle player == player && ((typeOf player) isEqualTo "B_BattleRoyalePlayer_F")};
			while {true} do 
			{
				false call BR_Client_Player_PlayerInfoUpdateTask;
				sleep .5;
			};
		};
	};
};
true