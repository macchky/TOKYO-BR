BR_VEHICLE_REPAIR_ALLOWED_PARTS=["HitEngine","Hit", "HitLFWheel", "HitLBWheel", "HitLMWheel", "HitLF2Wheel", "HitRFWheel", "HitRBWheel", "HitRMWheel", "HitRF2Wheel"];
BR_IsPlayerUsingToolkit=false;
BR_IsPlayerBoosting = false;
BR_DidPlayerCancelUsingToolkit=false;

br_hudKillMessageNotifications = [];
br_hudNotifications = [];
br_hudNotificationsSystemChat = [];

br_heliVehicleObjects = [];

br_BRPlayerRequestComplete=false;
br_ClientSidePlayerInitComplete=false;
br_LastToolkitRepairAttempt=0;

br_client_sessionID = "";

br_isThisPlayerSpectator=false;	// Used since it takes a while for body to respawn

br_public_playersRemaining=-1;
br_public_playersInitially=-1;
br_public_isSpectatorInGame=false;

br_playerLastFiredTime = 0;		// Last time a shot was fired
br_playerIsFiring=false;		// Poor man's mutex
br_playerLastHitTime = 0;		// Last time player was shot
br_playerIsBeingHit=false;		// Poor man's mutex

br_ejectionMessageReceived = false;

br_fontDefault = "PuristaMedium";
br_fontSizeDefault = "0.9";

br_jumpZForceStart = 3.2;
br_jumpMaxSpeedLimit = 8;
br_jumpAllowPlayerJump=true;

br_teamGUIMenuIsVisible=false;
br_teamGUIButtonInviteLastPressed = 0;
br_teamInvites = [];
br_teamMembers = [];
br_teamSpectatableUnits = [];
br_inTeam = false;
br_teamID = "0";
br_teamMapDisplayEH = nil;
br_teamCurrentViewMemberObject = objNull;
br_teamAllowTeamChanges = true;
br_teamViewLastSwitchedTime=0;
br_teamMapHelpMessageShownAlready=false;
br_teamPlayerCurrentChannel=5;
br_teamGUICtrlBGColor = [0, 0.3, 0.6, 0.8];
br_teamGUICtrlBGColorRed = [.45, 0, 0, 0.8];
br_teamGUIMenuBGBlur = objNull;
br_teamScriptTeamCameraMonitor = nil;

br_mapIconPlayer = gettext (configfile >> "cfgmarkers" >> "mil_start" >> "icon");
br_mapVisibleMapHooked = false;

br_teamInvitesMaxAllowed = 10;

br_lastMagRepackAttempt=0;

br_isPlayerPerformingSeriesAction=false;

br_skinsCurrentSkin = "";

//////  Damage UI VARIABLES ///////
BR_Client_Damage_PlayerLastStatsUpdate=0;
BR_Client_Damage_PlayerLastStatsUpdate_Inventory=0;
BR_Client_Damage_PlayerLastStatsUpdate_HitPoints=0;
BR_Client_Damage_PlayerAttributes=[
	1, 		// Health
	1,		// Boost
	0,		// Flag that hitpoints are problematic, for quick OnDraw3d inspection
	[]		//	HitPoints
];
BR_Client_Damage_HudEventHandler=0;
BR_Client_Damage_HudTicktimeRenderedAt=0;
BR_Client_Damage_DisplayHitPartNumberLookup = [0,0,0,3,3,3,3,3,8,8,10];	// maps HitPart Index numbers to image filenames.
BR_Client_Damage_DisplayHitParts_Head = [0,1,2]; 		// Maps Head to Head-related hit parts
BR_Client_Damage_DisplayHitParts_Chest = [3,4,5,6,7]; 	// Maps Chest to Chest-related hit parts
BR_Client_Damage_DisplayHitParts_Arms = [8,9]; 		// Maps Arms to Arm-related hit parts
BR_Client_Damage_DisplayHitParts_Legs = [10]; 			// Maps Legs to Leg-related hit parts

BR_Client_Damage_DisplayHitPart_ImagePathBase = "br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart"; //10_White.paa";

BR_Client_Damage_Hud_ActionTrack_Toolkit=-1;

// Legacy variables 
br_game_started = false;
br_deploy_players = false;
br_alert = false;
br_winner_check = false;
br_kill_zone = false;
br_FiredDelay = 0;
br_HitDelay = 0;	
br_gameRulesMessageText = "";
BR_CancelAnim = false;
BR_PlayerAnimationCancellationMovements = actionKeys "NextAction" + actionKeys "PreviousAction" + actionKeys "Action" + actionKeys "ActionContext" + actionKeys "TurnLeft" + actionKeys "TurnRight" + actionKeys  "SitDown" + actionKeys "MoveForward" + actionKeys "MoveBack" + actionKeys "MoveFastForward" + actionKeys "MoveSlowForward" + actionKeys "MoveLeft" + actionKeys "MoveRight" + actionKeys "EvasiveForward" + actionKeys "EvasiveLeft" + actionKeys "EvasiveRight" + actionKeys "EvasiveBack" + actionKeys "Stand" + actionKeys "Crouch" + actionKeys "Prone" + actionKeys "LeanLeft" + actionKeys "LeanRight"; //actionKeys "DefaultAction" == Map Action Only
BR_IsPlayerBoosting = false;
BR_BoostHealPerTick=(.13/40);	// Percent total heal divided by booster amount (40)
BR_LastBoostAttempt=0;
BR_MOUSE_DIK_LOOKUP = [
	65536, // Prim. Mouse Btn. 0
	65537, // Sec. Mouse Btn.	1
	65538, // Middle Mouse Btn. 2
	65539, // Mouse Btn. 3
	65540, // Mouse Btn. 4
	65541, // Mouse Btn. 5
	65542, // Mouse Btn. 6
	65543  // Mouse Btn. 7
];
br_Boost=0;