class CfgAmmo {
	// Tripmine
	class MineBase;
	class ATMine_Range_Ammo: MineBase
	{
		indirectHitRange = 8; // 1
	};
	// RPG Round
	class RocketBase;
	class CUP_R_PG7VR_AT: RocketBase
	{
		hit = 140;	// 100
		indirectHit = 10; 		//8
		indirectHitRange = 8;	// 6     80/24/18   120/16/16too strong
	};
	// STRM: Lee Enfield ammo boost
	class BulletBase;
	class CUP_B_303_Ball: BulletBase
	{
		hit=12;	// 15
		airFriction=-0.000082199002;
	};
	// Katiba + MX
	class B_65x39_Caseless: BulletBase
	{
		hit=10;	// 556 is 8.   10 default
	};
	// 762 buffs
	class CUP_B_762x39_Ball: BulletBase
	{
		hit=14; // 12
	};
	class CUP_B_762x39_Ball_Tracer_Green: CUP_B_762x39_Ball
	{
		hit=14;	// 12
	};
	class B_762x51_Ball: BulletBase
	{
		hit = 14;  //12
	};
	class CUP_B_762x54_Ball_White_Tracer: BulletBase
	{
		hit=14;
	};
	class B_93x64_Ball;
	class CUP_B_93x64_Ball: B_93x64_Ball
	{
		hit=10;	// 18 in default
		airFriction=-0.00060999999;
	};
	// Pellets
	class CUP_B_12Gauge_Pellets: BulletBase
	{
		simulation="shotSpread";
		hit=3;
		indirectHit=0;
		indirectHitRange=0;
		cartridge="FxCartridge_slug";
		cost=2;
		typicalSpeed=400;
		visibleFire=18;
		audibleFire=18;
		airFriction=-0.0049999999;  // 0009999999
		caliber=0.1;
		AGM_BulletMass=1;
	};

	// STRM: Start grenade buffs
	class Grenade;
	class GrenadeBase;
	class GrenadeHand: Grenade 	// RGO
	{
		hit = 12; //8;   			// damage done on impact at typical speed, modified by actual speed depending on explosion coef.
		indirectHit = 8;	//8  	// HIT for object that was found in range (and was not hit directly), modified by distance (0 HIT is at 4x range) and simple occlusion with other objects.
		indirectHitRange = 8;  //6 	// range in meters at that range the indirectHit is 100% and lineary degrades to 4x distance.
	};
	class mini_Grenade: GrenadeHand	// RGN
	{
		hit = 8; //6;
		indirectHit = 6;	//6;
		indirectHitRange = 7;	// 4
	};	
	class G_40mm_HE: GrenadeBase  //  Covers CUP_6Rnd_HE_M203, CUP_1Rnd_HE_M203, CUP_1Rnd_HEDP_M203, CUP_1Rnd_HE_GP25_M  which inherit 1Rnd_HE_Grenade_shell
	{
		hit = 140;	// 100
		indirectHit = 10; 		//8
		indirectHitRange = 8;	// 6     80/24/18   120/16/16too strong
	};
	class G_40mm_HEDP: G_40mm_HE // Covers M32 6 Rnd ammo
	{
		hit = 140;	// 100  (140 nukes cars, 100 does not)
		indirectHit = 10;
		indirectHitRange = 8;
	};
	// STRM: Start reduction in bomb noises.
	class BombCore;
	class Bo_Mk82: BombCore
	{
		soundHit1[] = {"A3\Sounds_F\weapons\Explosion\expl_big_1",1.25,1,700}; //2.5118864,1,1500
		soundHit2[] = {"A3\Sounds_F\weapons\Explosion\expl_big_2",1.25,1,700};
		soundHit3[] = {"A3\Sounds_F\weapons\Explosion\expl_big_3",1.25,1,700};
		soundHit4[] = {"A3\Sounds_F\weapons\Explosion\expl_shell_1",1.25,1,700};
		soundHit5[] = {"A3\Sounds_F\weapons\Explosion\expl_shell_2",1.25,1,700};
	};
	//class Bo_Mk82_MI08: Bo_Mk82  // Inherits from above...
	class LaserBombCore;
	class Bo_GBU12_LGB: LaserBombCore
	{
		soundHit1[] = {"A3\Sounds_F\weapons\Explosion\expl_big_1",2.25,1,700};	// 2.5118864,1,1500
		soundHit2[] = {"A3\Sounds_F\weapons\Explosion\expl_big_2",2.25,1,700};
		soundHit3[] = {"A3\Sounds_F\weapons\Explosion\expl_big_3",2.25,1,700};
		soundHit4[] = {"A3\Sounds_F\weapons\Explosion\expl_shell_1",2.25,1,700};
		soundHit5[] = {"A3\Sounds_F\weapons\Explosion\expl_shell_2",2.25,1,700};
	};
	//class Bo_GBU12_LGB_MI10: Bo_GBU12_LGB 	// Inherits from above...

	// Reduce noise of Osprey's barrell bombs
	class HelicopterExploSmall;
	class HelicopterExploBig: HelicopterExploSmall
	{
		hit = 10000;
		indirectHit = 200;
		indirectHitRange = 7.5;
		explosionEffects = "HelicopterExplosionEffects2";
		explosionSoundEffect = "DefaultExplosion";
		soundHit1[] = {"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_1",3.1622777,1,750};
		soundHit2[] = {"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_2",3.1622777,1,750};
		soundHit3[] = {"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_3",3.1622777,1,750};
		soundHit4[] = {"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_4",3.1622777,1,750};
		multiSoundHit[] = {"soundHit1",0.25,"soundHit2",0.25,"soundHit3",0.25,"soundHit4",0.25};
	};
	// STRM: Holiday Grenade fix
	class xmas_exposive_present_ammo: Grenade
	{
		hit = 7; //6;
		indirectHit = 5;	//6;
		indirectHitRange = 6;	// 4
	};
};