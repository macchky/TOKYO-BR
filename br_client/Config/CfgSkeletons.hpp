// By OfotViking - fix to L85
class CfgSkeletons
{
	class Default
	{
		isDiscrete=1;
		skeletonInherit="";
		skeletonBones[]={};
	};
	class L85A2_Skeleton: Default
	{
		skeletonBones[]=
		{
			"magazine","",
			"bolt", "",
			"fire_switch", "",
			"iron_sight", "",
			"front_sight", "",
			"Trigger", "",
			"ris", "",
			"muzzle", "",
			"zasleh", ""		
		};
	};
	class L86A2_Skeleton: L85A2_Skeleton
	{
		skeletonInherit="L85A2_Skeleton";
		skeletonBones[]=
		{
			// Bipod Skeleton
			"bipod_legs",		"",
			"bipod_leg_L",		"bipod_legs",
			"leg_L",			"bipod_leg_L",
			"bipod_leg_R",		"bipod_legs",
			"leg_R",			"bipod_leg_R",
			
			// Simple forward-folding bipod bones
			"bipod_simple_forward", "",
			
			// Simple backward-folding bipod bones
			"bipod_simple_backward", ""			
		};
	};
	class L85A2_UGL_Skeleton: L85A2_Skeleton
	{
		skeletonInherit="L85A2_Skeleton";
		skeletonBones[]=
		{
			"ladder", ""
		};
	};
};