class CfgSoundSets
{
	// STRM: TRG Silenced buff, thanks to OfotViking
	class Rifle_silencerShot_Base_SoundSet;
	class TRG20_silencerShot_SoundSet: Rifle_silencerShot_Base_SoundSet
	{
		soundShaders[]=
		{
			"TRG20_Closure_SoundShader"
		};
	};
	class Mk20_silencerShot_SoundSet: Rifle_silencerShot_Base_SoundSet
	{
		soundShaders[]=
		{
			"Mk20_Closure_SoundShader"
		};
	};	
	// STRM: Bombs
	class BombsHeavy_Exp_SoundSet
	{
		soundShaders[] = {"BombsHeavy_closeExp_SoundShader","BombsHeavy_midExp_SoundShader","BombsHeavy_distExp_SoundShader"};
		volumeFactor = .1; //2
		volumeCurve = "LinearCurve";
		spatial = 1;
		doppler = 0;
		loop = 0;
		sound3DProcessingType = "ExplosionLight3DProcessingType";  // "ExplosionHeavy3DProcessingType";
		distanceFilter = "explosionDistanceFreqAttenuationFilter";
	};
	class BombsHeavy_Tail_SoundSet
	{
		soundShaders[] = {"BombsHeavy_tailForest_SoundShader","BombsHeavy_tailMeadows_SoundShader","BombsHeavy_tailHouses_SoundShader"};
		volumeFactor = 0.01;  //.95
		volumeCurve = "InverseSquare2Curve";
		spatial = 1;
		doppler = 0;
		loop = 0;
		soundShadersLimit = 2;
		frequencyRandomizer = 0.05;
		sound3DProcessingType = "ExplosionLight3DProcessingType";  // "ExplosionHeavyTail3DProcessingType";
		distanceFilter = "explosionTailDistanceFreqAttenuationFilter";
	};
	class Explosion_Debris_SoundSet
	{
		soundShaders[] = {"Explosion_debrisSoft_SoundShader","Explosion_debrisDirt_SoundShader","Explosion_debrisHard_SoundShader"};
		volumeFactor = 0.01;  // .9
		volumeCurve[] = {{0,1},{30,0.75},{50,0.5},{100,0}};
		spatial = 1;
		doppler = 0;
		loop = 0;
		soundShadersLimit = 2;
		sound3DProcessingType = "ExplosionLight3DProcessingType";  // "ExplosionDebris3DProcessingType";
	};
	// New since 1.66 - osprey bomb sounds 2/3
	class BigIED_Exp_SoundSet
	{
		soundShaders[] = {"BigIED_closeExp_SoundShader","BigIED_midExp_SoundShader","BigIED_distExp_SoundShader"};
		volumeFactor = .3; // 1.5
		volumeCurve = "LinearCurve";
		spatial = 1;
		doppler = 0;
		loop = 0;
		sound3DProcessingType = "ExplosionHeavy3DProcessingType";
		distanceFilter = "explosionDistanceFreqAttenuationFilter";
	};
	class BigIED_Tail_SoundSet
	{
		soundShaders[] = {"BigIED_tailForest_SoundShader","BigIED_tailMeadows_SoundShader","BigIED_tailHouses_SoundShader"};
		volumeFactor = .15; // 1.0
		volumeCurve = "LinearCurve";
		spatial = 1;
		doppler = 0;
		loop = 0;
		soundShadersLimit = 2;
		frequencyRandomizer = 0.05;
		sound3DProcessingType = "ExplosionHeavyTail3DProcessingType";
		distanceFilter = "explosionTailDistanceFreqAttenuationFilter";
	};
};
// class CfgSoundEffects
// {
// 	class WeaponsEffects
// 	{
// 		class Default;
// 		class DefaultExplosion
// 		{
// 			class Begin: Default{};
// 			class Middle: Default
// 			{
// 				distance = 20; // 100
// 				frequency = 0.95;
// 				gain[] = {1.0,1.0,0.56234133,0.3548134};
// 				threshold = 0.099999994;
// 				ratio = 0.5;
// 				attack = 90;
// 			};
// 			class End: Default
// 			{
// 				distance = 30; // 200
// 				frequency = 0.9;
// 				gain[] = {2.5118864,1.4125376,0.31622776,0.12589253};
// 				threshold = 0.099999994;
// 				ratio = 0.6;
// 				attack = 10;
// 			};
// 		};

// 	};
// };