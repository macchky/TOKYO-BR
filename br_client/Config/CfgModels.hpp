class CfgModels
{
	class Default
	{
		sectionsInherit="";
		sections[]={};
		skeletonName="";
	};
	class L85A2: Default
	{
		sections[]=
		{
			"zasleh"
		};
		skeletonName="L85A2_Skeleton";
		class Animations
		{	
			class trigger_move
			
			{
				type="rotation";
				source="reload";
				selection="trigger";
				memory=1;
				sourceAddress="clamp";
				axis = "trigger_Axis";
				minValue=0.0;
				maxValue=0.3;
				minPhase=0.0;
				maxPhase=0.3;
				angle0=0;
				angle1=-0.212389;
			};
			
			class muzzle_hide
			{
				type="hide";
				source="hasSuppressor";
				selection="muzzle";
				minValue = 0.0;
				maxValue = 1.0;
				hideValue = 1;
				animPeriod = 0.0;
				initPhase = 0.0;
			};
			
			class bolt_move
			
			{
				type="translation";
				source="reload";
				selection="bolt";
				memory=1;
				sourceAddress="clamp";
				axis = "bolt_Axis";
				minValue=0.0;
				maxValue=1;
				minPhase=0.0;
				maxPhase=1;
				offset0=0;
				offset1=0.6;
			};
			
			class bolt_empty
			{
				type="translation";
				source="isEmptyNoReload";
				selection="bolt";
				axis="bolt_axis";
				sourceAddress = clamp;
				minValue = 0.0;
				maxValue = 1.0;
				offset0 = 0.0;
				offset1 = 0.6;
				animPeriod = 0.0;
				initPhase = 0.0;
				memory = true;
			};
			class bolt_move_reload1
			
			{
				type="translation";
				source="reloadmagazine";
				selection="bolt";
				memory=1;
				sourceAddress="clamp";
				axis = "bolt_Axis";
				minValue=0.64667;
				maxValue=0.68;
				offset0=0;
				offset1=1;
			};
			class bolt_move_reload2
			
			{
				type="translation";
				source="reloadmagazine";
				selection="bolt";
				memory=1;
				sourceAddress="clamp";
				axis = "bolt_Axis";
				minValue=0.7466667;
				maxValue=0.7666667;
				offset0=0;
				offset1=-1;
			};
			class magazine_reload_hide
			{
				type="hide";
				source="reloadMagazine";
				selection="magazine";
				sourceAddress = clamp;
				minValue = 0.0;
				maxValue = 1.0;
				hideValue = 0.12666667;
				unHideValue = 0.35333333;
				animPeriod = 0.0;
				initPhase = 0.0;
			};
			
			class magazine_reload_move_1
			{
				type="translation";
				source="reloadMagazine";
				selection="magazine";
				axis="magazine_axis";
				sourceAddress = clamp;
				minValue = 0.1;
				maxValue = 0.12;
				offset0 = 0.0;
				offset1 = 1;
				animPeriod = 0.0;
				initPhase = 0.0;
				memory = true;
			};
			
			class magazine_reload_move_2
			{
				type="translation";
				source="reloadMagazine";
				selection="magazine";
				axis="magazine_axis";
				sourceAddress = clamp;

				minValue = 0.36;
				maxValue = 0.4;
				offset0 = 0.0;
				offset1 = -0.8;
				animPeriod = 0.0;
				initPhase = 0.0;
				memory = true;
			};
			class magazine_reload_move_2a
			{
				type="translation";
				source="reloadMagazine";
				selection="magazine";
				axis="magazine_axis";
				sourceAddress = clamp;
				minValue = 0.446666667;
				maxValue = 0.46;
				offset0 = 0.0;
				offset1 = -0.2;
				animPeriod = 0.0;
				initPhase = 0.0;
				memory = true;
			};
			
			class no_magazine
			{
				type="hide";
				source="hasMagazine";
				selection="magazine";
				sourceAddress = clamp;
				minValue = 0.0;
				maxValue = 1.0;
				hideValue = 0.5;
				unHideValue = -1.0;
				animPeriod = 0.0;
				initPhase = 0.0;
			};
			
			class safety_mode_rot
			{
				type="rotation";
				source="weaponMode";
				selection="fire_switch";
				axis="fireswitch_axis";
				sourceAddress = clamp;
				minValue = 0.0;
				maxValue = 0.25;
				angle0 = 0;
				angle1 = -0.5;
				animPeriod = 0.0;
				initPhase = 0.0;
				memory = true;
			};
			
			class ironsight_hide
			{
				type="hide";
				source="hasOptics";
				selection="iron_sight";
				minValue = 0.0;
				maxValue = 1.0;
				hideValue = 1;
				animPeriod = 0.0;
				initPhase = 0.0;
			};
			
			class ris_unhide
			{
				type="hide";
				source="hasOptics";
				selection="ris";
				minValue = 0.0;
				maxValue = 1.0;
				hideValue = 0;
				unhideValue = 1;
				animPeriod = 0.0;
				initPhase = 0.0;
			};
			
			class MuzzleFlashROT
 			{
 				type="rotationX";
 				source="ammoRandom";            //use ammo count as phase for animation
 				sourceAddress="loop";     //loop when phase out of bounds
 				selection="zasleh";       //selection we want to rotate
 				axis="zasleh_axis";                  //no own axis - center of rotation is computed from selection
 				centerFirstVertex=true;   //use first vertex of selection as center of rotation
 				minValue=0;
 				maxValue=4;               //rotation angle will be 360/4 = 90 degrees
 				angle0="rad 0";
 				angle1="rad 360";
 			};
		
		};
	};
	
	class L85A2_RIS: L85A2{};
	class L85A2_RIS_NG: L85A2{};
	
	class L85A2_UGL: L85A2
	{
		sectionsInherit="L85A2";
		skeletonName="L85A2_UGL_Skeleton";
		class Animations: Animations
		{
			class OP_ROT
			{
				type="rotation";
				source="zeroing2";
				selection="ladder";
				axis="op_axis";
				sourceAddress = loop;
				minValue = 0.0;
				maxValue = 1.0;
				angle0 = -0.034906585;
				angle1 = 0.62;
				animPeriod = 1.4013e-045/*#DEN*/;
				initPhase = 0.0;
				memory = true;
			};
			
			class sight_gl_flip
			{
				type="rotation";
				source="weaponMuzzle";
				sourceAddress="loop";
				selection="ladder";
				axis="OP_axis";
				minValue=1;
				maxValue=2;
				angle0="rad 0";
				angle1="rad 81";
			};
		};
	};
	
	class L86A2: L85A2
	{
		sectionsInherit="L85A2";
		skeletonName="L86A2_Skeleton";
		class Animations: Animations
		{
			class bipod_legs 
			{
				type = "rotation";
				source = "bipod";
				sourceAddress = "clamp";
				selection = "bipod_legs";
				axis = "bipod_legs_axis_1";
				angle0 = "(rad 0)";
				angle1 = "(rad 90)";
				memory = 1;
			};
			
			class bipod_leg_l : bipod_legs 
			{
				type = "rotation";
				axis = "bipod_legs_axis_2";
				selection = "bipod_leg_l";
				angle0 = "(rad 0)";
				angle1 = "(rad 33)";
			};
			
			class bipod_leg_r : bipod_leg_l 
			{
				selection = "bipod_leg_r";
				angle1 = "(rad -33)";
			};	
		};
	};
};