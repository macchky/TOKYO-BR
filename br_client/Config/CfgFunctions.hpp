class CfgFunctions
{
	// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)
	class br_client
	{
		class Initialize
		{
			file = "br_client\init";
			// class preStart 
			// {
			// 	preStart = 1;
			// };
			class preInit
			{
				preInit = 1;
			};
			class postInit
			{
				postInit = 1;
			};
		};
		// class Spectator
  //       {
  //           file = "br_client\Spectator";
  //           class BRSpectator{};	// e.g.  br_client_fnc_BRSpectator
  //           class BRObjectiveVisualizer{};
  //           class BRObjectiveVisualizerDraw{};
  //           class BRSpectatorCamera{};
  //           class BRSpectatorCameraTick{};
  //           class BRSpectatorCameraSetTarget{};
  //           class BRSpectatorCameraResetTarget{};
  //           class BRSpectatorCameraPrepareTarget{};
  //           class BRSpectatorDraw2D{};
  //           class BRSpectatorDraw3D{};
  //           class BRSpectatorGetUnitsToDraw{};
		// };
	};
};
