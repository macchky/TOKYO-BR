/*---------------------------------------------------------------------------
STRM: Central place for Resources / GUI
---------------------------------------------------------------------------*/
#define CT_STATIC           0
#define CT_BUTTON           1
#define CT_EDIT             2
#define CT_SLIDER           3
#define CT_COMBO            4
#define CT_LISTBOX          5
#define CT_TOOLBOX          6
#define CT_CHECKBOXES       7
#define CT_PROGRESS         8
#define CT_HTML             9
#define CT_STATIC_SKEW      10
#define CT_ACTIVETEXT       11
#define CT_TREE             12
#define CT_STRUCTURED_TEXT  13
#define CT_CONTEXT_MENU     14
#define CT_CONTROLS_GROUP   15
#define CT_SHORTCUTBUTTON   16
#define CT_HITZONES         17
#define CT_XKEYDESC         40
#define CT_XBUTTON          41
#define CT_XLISTBOX         42
#define CT_XSLIDER          43
#define CT_XCOMBO           44
#define CT_ANIMATED_TEXTURE 45
#define CT_OBJECT           80
#define CT_OBJECT_ZOOM      81
#define CT_OBJECT_CONTAINER 82
#define CT_OBJECT_CONT_ANIM 83
#define CT_LINEBREAK        98
#define CT_USER             99
#define CT_MAP              100
#define CT_MAP_MAIN         101
#define CT_LISTNBOX         102
#define CT_ITEMSLOT         103
#define CT_CHECKBOX         77

// Static styles
#define ST_POS            0x0F
#define ST_HPOS           0x03
#define ST_VPOS           0x0C
#define ST_LEFT           0x00
#define ST_RIGHT          0x01
#define ST_CENTER         0x02
#define ST_DOWN           0x04
#define ST_UP             0x08
#define ST_VCENTER        0x0C

#define ST_TYPE           0xF0
#define ST_SINGLE         0x00
#define ST_MULTI          0x10
#define ST_TITLE_BAR      0x20
#define ST_PICTURE        0x30
#define ST_FRAME          0x40
#define ST_BACKGROUND     0x50
#define ST_GROUP_BOX      0x60
#define ST_GROUP_BOX2     0x70
#define ST_HUD_BACKGROUND 0x80
#define ST_TILE_PICTURE   0x90
#define ST_WITH_RECT      0xA0
#define ST_LINE           0xB0
#define ST_UPPERCASE      0xC0
#define ST_LOWERCASE      0xD0

#define ST_SHADOW         0x100
#define ST_NO_RECT        0x200
#define ST_KEEP_ASPECT_RATIO  0x800

#define ST_TITLE          ST_TITLE_BAR + ST_CENTER

// Slider styles
#define SL_DIR            0x400
#define SL_VERT           0
#define SL_HORZ           0x400

#define SL_TEXTURES       0x10

// progress bar 
#define ST_VERTICAL       0x01
#define ST_HORIZONTAL     0

// Listbox styles
#define LB_TEXTURES       0x10
#define LB_MULTI          0x20

// Tree styles
#define TR_SHOWROOT       1
#define TR_AUTOCOLLAPSE   2

// MessageBox styles
#define MB_BUTTON_OK      1
#define MB_BUTTON_CANCEL  2
#define MB_BUTTON_USER    4
#define MB_ERROR_DIALOG   8

// Xbox buttons
#define KEY_XINPUT                0x00050000
#define KEY_XBOX_A                KEY_XINPUT + 0
#define KEY_XBOX_B                KEY_XINPUT + 1
#define KEY_XBOX_X                KEY_XINPUT + 2
#define KEY_XBOX_Y                KEY_XINPUT + 3
#define KEY_XBOX_Up               KEY_XINPUT + 4
#define KEY_XBOX_Down             KEY_XINPUT + 5
#define KEY_XBOX_Left             KEY_XINPUT + 6
#define KEY_XBOX_Right            KEY_XINPUT + 7
#define KEY_XBOX_Start            KEY_XINPUT + 8
#define KEY_XBOX_Back             KEY_XINPUT + 9
#define KEY_XBOX_LeftBumper       KEY_XINPUT + 10
#define KEY_XBOX_RightBumper      KEY_XINPUT + 11
#define KEY_XBOX_LeftTrigger      KEY_XINPUT + 12
#define KEY_XBOX_RightTrigger     KEY_XINPUT + 13
#define KEY_XBOX_LeftThumb        KEY_XINPUT + 14
#define KEY_XBOX_RightThumb       KEY_XINPUT + 15
#define KEY_XBOX_LeftThumbXRight  KEY_XINPUT + 16
#define KEY_XBOX_LeftThumbYUp     KEY_XINPUT + 17
#define KEY_XBOX_RightThumbXRight KEY_XINPUT + 18
#define KEY_XBOX_RightThumbYUp    KEY_XINPUT + 19
#define KEY_XBOX_LeftThumbXLeft   KEY_XINPUT + 20
#define KEY_XBOX_LeftThumbYDown   KEY_XINPUT + 21
#define KEY_XBOX_RightThumbXLeft  KEY_XINPUT + 22
#define KEY_XBOX_RightThumbYDown  KEY_XINPUT + 23

// Fonts
#define GUI_FONT_NORMAL			PuristaMedium
#define GUI_FONT_BOLD			PuristaSemibold
#define GUI_FONT_THIN			PuristaLight
#define GUI_FONT_MONO			EtelkaMonospacePro
#define GUI_FONT_NARROW			EtelkaNarrowMediumPro
#define GUI_FONT_CODE			LucidaConsoleB
#define GUI_FONT_SYSTEM			TahomaB

#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)	// 1/40 = each grid box is .025
#define GUI_GRID_H	(0.04)	// 1/25 = each grid box is .04
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

#define XALIGN						0.866
#define YALIGN						0.78

#define PSTATUSWIDTH				0.12
#define PSTATUSHEIGHT				0.008
	
#define PADDINGX					0.002
#define PADDINGY					0.001

#define COL_BLUE					{0, 0, 1, 0.2}
#define COL_GREEN					{0, 1, 0, 0.2}
#define COL_BLACK					{0, 0, 0, 0.2}
#define COL_GREY					{0.5, 0.5, 0.5, 0.2}
#define COL_BROWN 					{0.8, 0.4, 0.113, 0.3}

class RscText;	
class RscActiveText;
class RscStructuredText;	
class Attributes;	
class RscPicture;
class RscPictureKeepAspect;
class RscVignette;
class RscProgress;
class ProgressMap;
class RscControlsGroupNoScrollbars;
class RscStandardDisplay;
class Pause1;
class RscTitle;
class RscButtonMenu;
class RscDebugConsole;
class RscTrafficLight;
class RscFeedback;
class RscHTML;
class RscFrame;
class CA_Title;
class ShortcutPos;
class RscMessageBox;
class RscButtonMenuCancel;
class RscEdit;
class RscCombo;
class RscTree;
class RscToolbox;
class RscButton;
class RscMapControl;
class RscListBox;
class RscListNBox {
	class ScrollBar{};
};
class RscCheckbox;

// Delete Stamina bar
class RscInGameUI {
	
	class RscStaminaBar	{
		scriptPath = "IGUI";
		onLoad = "";
		onUnload = "";
		controls[] = {};
		delete StaminaBar;
	};
	class RscHint {
		idd = 301;
		movingEnable = 0;
		controls[] = {"Background", "Hint"};
		onLoad = "uiNamespace setVariable ['UI_Hint', _this select 0];";
		
		class Background : RscText {
			idc = 101;
			style = 128;
			x = "((safezoneX + safezoneW) - 		(12 * 			(			((safezoneW / safezoneH) min 1.2) / 40)) - 1 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			y = "(safezoneY + 6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
			w = "(12 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			h = "(8 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
			text = "";
			colorBackground[] = {0, 0, 0, 0.7};
			shadow = true;
		};
		
		class Hint : RscStructuredText {
			idc = 102;
			x = "((safezoneX + safezoneW) - 		(12 * 			(			((safezoneW / safezoneH) min 1.2) / 40)) - 1 * 			(			((safezoneW / safezoneH) min 1.2) / 40)) + 0.4*					(			((safezoneW / safezoneH) min 1.2) / 40)";
			y = "(safezoneY + 6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)) + 0.3*					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w = "(12 * 			(			((safezoneW / safezoneH) min 1.2) / 40)) - 0.8*					(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "(8 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)) - 0.8*					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			style = 16;
			shadow = true;
			size = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
			
			class Attributes : Attributes {};
		};
	};
};

// STRM: News feed, in pause menu
class RscDisplayMPInterrupt: RscStandardDisplay
{
	scriptName = "RscDisplayMPInterrupt";
	scriptPath = "GUI";
	onLoad = "_this call (uiNamespace getVariable 'Client_OnMPPause_NewsFeed'); [""onLoad"",_this,""RscDisplayMPInterrupt"",'GUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload = "[""onUnload"",_this,""RscDisplayMPInterrupt"",'GUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	movingEnable = 0;
	enableSimulation = 1;
	class controlsBackground
	{
		class Vignette: RscVignette
		{
			idc = 114998;
		};
		class TileGroup: RscControlsGroupNoScrollbars
		{
			idc = 115099;
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			h = "safezoneH";
			disableCustomColors = 1;
			class Controls
			{
				class TileFrame: RscFrame
				{
					idc = 114999;
					x = 0;
					y = 0;
					w = "safezoneW";
					h = "safezoneH";
					colortext[] = {0,0,0,1};
				};
				class Tile_0_0: RscText
				{
					idc = 115000;
					x = "(0 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_1: RscText
				{
					idc = 115001;
					x = "(0 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_2: RscText
				{
					idc = 115002;
					x = "(0 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_3: RscText
				{
					idc = 115003;
					x = "(0 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_4: RscText
				{
					idc = 115004;
					x = "(0 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_0_5: RscText
				{
					idc = 115005;
					x = "(0 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_0: RscText
				{
					idc = 115010;
					x = "(1 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_1: RscText
				{
					idc = 115011;
					x = "(1 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_2: RscText
				{
					idc = 115012;
					x = "(1 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_3: RscText
				{
					idc = 115013;
					x = "(1 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_4: RscText
				{
					idc = 115014;
					x = "(1 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_1_5: RscText
				{
					idc = 115015;
					x = "(1 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_0: RscText
				{
					idc = 115020;
					x = "(2 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_1: RscText
				{
					idc = 115021;
					x = "(2 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_2: RscText
				{
					idc = 115022;
					x = "(2 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_3: RscText
				{
					idc = 115023;
					x = "(2 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_4: RscText
				{
					idc = 115024;
					x = "(2 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_2_5: RscText
				{
					idc = 115025;
					x = "(2 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_0: RscText
				{
					idc = 115030;
					x = "(3 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_1: RscText
				{
					idc = 115031;
					x = "(3 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_2: RscText
				{
					idc = 115032;
					x = "(3 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_3: RscText
				{
					idc = 115033;
					x = "(3 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_4: RscText
				{
					idc = 115034;
					x = "(3 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_3_5: RscText
				{
					idc = 115035;
					x = "(3 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_0: RscText
				{
					idc = 115040;
					x = "(4 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_1: RscText
				{
					idc = 115041;
					x = "(4 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_2: RscText
				{
					idc = 115042;
					x = "(4 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_3: RscText
				{
					idc = 115043;
					x = "(4 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_4: RscText
				{
					idc = 115044;
					x = "(4 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_4_5: RscText
				{
					idc = 115045;
					x = "(4 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_0: RscText
				{
					idc = 115050;
					x = "(5 * 1/6) * safezoneW";
					y = "(0 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_1: RscText
				{
					idc = 115051;
					x = "(5 * 1/6) * safezoneW";
					y = "(1 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_2: RscText
				{
					idc = 115052;
					x = "(5 * 1/6) * safezoneW";
					y = "(2 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_3: RscText
				{
					idc = 115053;
					x = "(5 * 1/6) * safezoneW";
					y = "(3 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_4: RscText
				{
					idc = 115054;
					x = "(5 * 1/6) * safezoneW";
					y = "(4 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
				class Tile_5_5: RscText
				{
					idc = 115055;
					x = "(5 * 1/6) * safezoneW";
					y = "(5 * 1/6) * safezoneH";
					w = "1/6 * safezoneW";
					h = "1/6 * safezoneH";
					colorBackground[] = {0,0,0,0.1};
				};
			};
		};
		class TitleBackground: RscText
		{
			idc = 1050;
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "14.2 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"};
		};
		class MissionNameBackground: RscText
		{
			idc = -1;
			x = "SafezoneX + (1 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			y = "23 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "SafezoneW - (2 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {0,0,0,0.7};
		};
		class Pause1: RscText
		{
			idc = 1000;
			x = "safezoneX + safezoneW - 2.2 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			y = "safezoneY + 1.4 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w = "0.7 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "2 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {1,1,1,1};
			shadow = 2;
		};
		class Pause2: Pause1
		{
			idc = 1001;
			x = "safezoneX + safezoneW - 3.2 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
		};
	};
	class controls
	{
		delete B_Players;
		delete B_Options;
		delete B_Abort;
		delete B_Retry;
		delete B_Load;
		delete B_Save;
		delete B_Continue;
		delete B_Diary;
		delete TrafficLight;
		class BRServerName : RscTitle
		{
			idc=24202;
			x="(safeZoneX + safeZoneW) - .7";
			y="-1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w=.7;
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[]={0,0,0,.75};
			colorText[]={1,1,1,1};
			style = 1;
			font = "RobotoCondensedLight";
			sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
		};
		class BRNews: RscHTML 
		{
			idc=24201;
			// x=0;
			// y=0;
			x="(safeZoneX + safeZoneW) - .7";
			y=0;
			w=.7;
			h=.7;
			colorBackground[]={0,0,0,.75};
			colorText[]={1,1,1,1};
			colorBold[]={1,1,1,1};
			colorLink[]={"190/255","0/255","0/255",1};
			colorLinkActive[]={"0/255","0/255","101/255",1};
			movingEnable=1;
			class H1
			{
				font=GUI_FONT_NORMAL;
				fontBold=GUI_FONT_NORMAL;
				sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.2)";
			};
			class H2: H1
			{
				font = "RobotoCondensedLight";
				fontBold = "RobotoCondensed";
			};
			class P: H2
			{
				sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
				fontBold = "RobotoCondensedLight";
			};
		};
		class Title: RscTitle
		{
			idc = 523;
			style = 0;
			text = "$STR_XBOX_CONTROLER_DP_MENU";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "14.2 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "5 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {0,0,0,0};
		};
		class PlayersName: CA_Title
		{
			idc = 109;
			style = 1;
			colorBackground[] = {0,0,0,0};
			x = "6 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "14.2 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "10 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonCancel: RscButtonMenu
		{
			idc = 2;
			shortcuts[] = {"0x00050000 + 1","0x00050000 + 8"};
			default = 1;
			class ShortcutPos: ShortcutPos
			{
				left = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			};
			text = "$STR_DISP_INT_CONTINUE";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "15.3 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonSAVE: RscButtonMenu
		{
			idc = 103;
			text = "$STR_DISP_INT_SAVE";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "16.4 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonSkip: RscButtonMenu
		{
			idc = 1002;
			text = "$STR_DISP_INT_SKIP";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "16.4 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonRespawn: RscButtonMenu
		{
			idc = 1010;
			text = "$STR_DISP_INT_RESPAWN";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "17.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonOptions: RscButtonMenu
		{
			idc = 101;
			text = "$STR_A3_RscDisplayMain_ButtonOptions";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "18.6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonVideo: RscButtonMenu
		{
			idc = 301;
			text = "$STR_A3_RscDisplayInterrupt_ButtonVideo";
			x = "2 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "15.3 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip = "$STR_TOOLTIP_MAIN_VIDEO";
		};
		class ButtonAudio: RscButtonMenu
		{
			idc = 302;
			text = "$STR_A3_RscDisplayInterrupt_ButtonAudio";
			x = "2 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "16.4 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip = "$STR_TOOLTIP_MAIN_AUDIO";
		};
		class ButtonControls: RscButtonMenu
		{
			idc = 303;
			text = "$STR_A3_RscDisplayInterrupt_ButtonControls";
			x = "2 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "17.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip = "$STR_TOOLTIP_MAIN_CONTROLS";
		};
		class ButtonGame: RscButtonMenu
		{
			idc = 307;
			text = "$STR_A3_RscDisplayInterrupt_ButtonGame";
			x = "2 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "18.6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip = "$STR_TOOLTIP_MAIN_GAME";
		};
		class ButtonTutorialHints: RscButtonMenu
		{
			idc = 122;
			text = "$STR_A3_RscDisplayInterrupt_ButtonTutorialHints";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "19.7 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonAbort: RscButtonMenu
		{
			idc = 104;
			text = "$STR_DISP_INT_ABORT";
			x = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "20.8 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "15 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class DebugConsole: RscDebugConsole
		{
			x = "17 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "0.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "22 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "21.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MissionTitle: RscText
		{
			idc = 120;
			font = "RobotoCondensedLight";
			sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
			shadow = 0;
			x = "SafezoneX + (1 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			w = "SafezoneW - (15 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			y = "23 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Version: RscText
		{
			style = 1;
			font = "RobotoCondensedLight";
			sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
			shadow = 0;
			x = "SafezoneX + SafezoneW - (13 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			idc = 1005;
			y = "23 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "12 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class TraffLight: RscTrafficLight
		{
			idc = 121;
			x = "SafezoneX + SafezoneW - (2 * 			(			((safezoneW / safezoneH) min 1.2) / 40))";
			show = 0;
			y = "23 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "1 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Feedback: RscFeedback{};
		class MessageBox: RscMessageBox{};
	};
};

// STRM:  Redefined here so players can' troll with markers
class RscDisplayInsertMarker
{
	scriptName = "RscDisplayInsertMarker";
	scriptPath = "GUI";
	onLoad = "[""onLoad"",_this,""RscDisplayInsertMarker"",'GUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload = "[""onUnload"",_this,""RscDisplayInsertMarker"",'GUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
	idd = 54;
	movingEnable = 0;
	class controlsBackground
	{
		class RscText_1000: RscText
		{
			idc = 1000;
			x = "0 * GUI_GRID_INSERTMARKER_W + GUI_GRID_INSERTMARKER_X";
			y = "0 * GUI_GRID_INSERTMARKER_H + GUI_GRID_INSERTMARKER_Y";
			w = "8 * GUI_GRID_INSERTMARKER_W";
			h = "2.5 * GUI_GRID_INSERTMARKER_H";
			colorBackground[] = {0,0,0,0.5};
		};
	};
	class controls
	{
		delete ButtonOK;
		delete Info;
		delete ButtonMenuInfo;
		delete Picture;
		delete Text;
		delete ButtonMenuOK;	
		// class ButtonMenuOK: RscButtonMenuOK
		// {
		// 	x = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
		// 	y = "13.6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
		// 	w = "4.9 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
		// 	h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		// };
		class ButtonMenuCancel: RscButtonMenuCancel
		{
			x = "19 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "13.6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "5 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Title: RscText
		{
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"};
			idc = 1001;
			text = "DISABLED IN BATTLEROYALE";
			x = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "8.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "10 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Description: RscStructuredText
		{
			colorBackground[] = {0,0,0,0.7};
			idc = 1100;
			x = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "9.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "10 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class DescriptionChannel: RscStructuredText
		{
			colorBackground[] = {0,0,0,0.7};
			idc = 1101;
			x = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "11.6 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "10 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MarkerPicture: RscPicture
		{
			idc = 102;
			x = 0.259984;
			y = 0.4;
			w = 0.05;
			h = 0.0666667;
		};
		// class MarkerText: RscEdit
		// {
		// 	idc = 101;
		// 	x = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
		// 	y = "10.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
		// 	w = "10 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
		// 	h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		// };
		class MarkerChannel: RscCombo
		{
			idc = 103;
			x = "14 * 			(			((safezoneW / safezoneH) min 1.2) / 40) + 			(safezoneX)";
			y = "12.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 			(safezoneY + safezoneH - 			(			((safezoneW / safezoneH) min 1.2) / 1.2))";
			w = "10 * 			(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};

class RscBRLoadingScreen {
	idd = 25200;
	onLoad = "uiNamespace setVariable ['RscBRLoadingScreen', _this select 0]";
	onUnload = "uiNamespace setVariable ['RscBRLoadingScreen', displayNull];";
	class controlsBackground
	{
		class Black: RscText
		{
			colorBackground[] = {0,0,0,1};
			x = "safezoneXAbs";
			y = "safezoneY";
			w = "safezoneWAbs";
			h = "safezoneH";
		};
		class CA_Vignette: RscVignette
		{
			colorText[] = {0,0,0,1};
		};
	};
	class controls
	{
		class LoadingScreenText: RscText 
		{
			idc=30001;
			colorText[] = {1,1,1,1};
			text="";
			x = "0";
			y = ".4";
			w = "1";
			h = "1";
			sizeEx="0.06";
			style=2;
		};
		class BR_Logo: RscPicture
		{
			text = "br_client\images\br_logo_mission_load.paa";
			x = ".25";
			y = ".15";
			w = ".5";
			h = ".5";
			sizeEx = 1.0 * GUI_GRID_H;
			colorBackground[] = {-1,-1,-1,0};
			style = ST_PICTURE + ST_KEEP_ASPECT_RATIO;
		};
		class MapName: RscText
		{
			text="Welcome to PLAYERUNKNOWN's Battle Royale!";
			x = "safezoneX + 0.2 * (((safezoneW / safezoneH) min 1.2) / 40)";
			y = "safezoneY";
			w = "safezoneW - 0.4 * (((safezoneW / safezoneH) min 1.2) / 40)";
			idc = 1001;
			h = "1.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "1.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressMap: RscProgress
		{
			colorBar[] = {100/255,0,0,.6};			// "(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"};
			texture = "#(argb,8,8,3)color(1,1,1,1)";
			x = "safezoneX";
			y = "2.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + safezoneY";
			w = "safezoneW";
			idc = 104;
			h = "0.2 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};


class RscTitles
{
	titles[] = {};
	class RscBRDamageHud
	{
		idd = 2300;
		duration = 999999;
		fadein = 1;
		fadeout = 1;
		onLoad = "uiNamespace setVariable ['RscBRDamageHud', _this select 0]";
		class Controls
		{
			// NOTE IDC Numbering matches image names!
			class Damage_Legs : RscPicture
			{
				idc = 2510;	// Match 10 to hit index (legs) and paa file, Icon_Body_Damage_HitPart10_White.paa
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart10_White.paa";
				x = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) - 32 * (safeZoneWAbs / (getResolution select 0)) - 8 * (safeZoneWAbs / (getResolution select 0))";
				y = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0])";
				w = "32 * (safeZoneWAbs / (getResolution select 0))";
				h = "32 * (safeZoneH / (getResolution select 1))";
				colorBackground[] = {0,0,0,0};
				style = ST_PICTURE + ST_KEEP_ASPECT_RATIO;
				blinkingPeriod=0;
			};
			class Damage_Chest: Damage_Legs
			{
				idc = 2503;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart3_White.paa";
			};
			class Damage_Head: Damage_Legs
			{
				idc = 2500;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart0_White.paa";
			};
			class Damage_Arms: Damage_Legs
			{
				idc = 2508;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart8_White.paa";
			};
		};
	};
	class RscRespawnCounter
	{
		scriptName = "RscRespawnCounter";
		scriptPath = "IGUI";
		onLoad = "[""onLoad"",_this,""RscRespawnCounter"",'IGUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
		onUnload = "[""onUnload"",_this,""RscRespawnCounter"",'IGUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
		idd = 3000;
		duration = 1e+011;
		fadein = 0;
		movingEnable = 0;
		class Controls
		{
			class MPTable: RscText
			{
				colorBackground[] = {0,0,0,0};
				idc = 1000;
				x = 0.025;
				y = 0.08;
				w = 0.95;
				h = 0.78;
			};
			class TitleBackground: RscText
			{
				colorBackground[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
				idc = 1002;
				x = .5;
				y = 2;
				w = "13 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
				h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
			class Title: RscText
			{
				idc = 1001;
				x = .5;
				y = 2;
				w = "13 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
				h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				colorBackground[] = {0,0,0,0.5};
			};
			class PlayerRespawnTime: RscText
			{
				idc = 1003;
				text = "00:00.000";
				x = .5;
				y = 2;
				w = "7 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
				h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				sizeEx = "1.7 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
			class Description: RscStructuredText
			{
				colorBackground[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
				idc = 1100;
				x = .5;
				y = 2; //"24.2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 		(safezoneY + (safezoneH - 					(			((safezoneW / safezoneH) min 1.2) / 1.2))/2)";
				w = "13 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
				h = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				sizeEx = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
		};
	};
	class BR_RscDisplayPlayerStatus {
		idd = 5001;
		movingEnable = 0;
		duration = 100000000;
		onLoad = "uiNamespace setVariable ['BR_GUI_status', _this select 0];";
		class ControlsBackground 
		{
			class BR_player_StatusBG: RscText
			{	
				idc = -1;
				x = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0])"; //,(safezoneX + safezoneW - 11 * (((safezoneW / safezoneH) min 1.2) / 20))])";
				y = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0])"; //,(safezoneY + safezoneH - 20.5 *((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + 0.71";
				w = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1])"; //,		(10 * 			(			((safezoneW / safezoneH) min 1.2) / 40))])";
				h = (PSTATUSHEIGHT + PSTATUSHEIGHT + PSTATUSHEIGHT + PADDINGY + PADDINGY + PADDINGY + PADDINGY) * safezoneH;
				style = 2;
				type = 13;
				text = "";	
				size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[] = COL_BLACK;
			};
			class BR_player_HealthBG: RscText
			{	
				idc = -1;
				x = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005"; //,(safezoneX + safezoneW - 11 * (((safezoneW / safezoneH) min 1.2) / 20))]) + 0.005";
				y = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.008"; //,(safezoneY + safezoneH - 20.5 *((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + 0.718";
				w = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01"; //,		(10 * 			(			((safezoneW / safezoneH) min 1.2) / 40))]) - 0.01";
				h = PSTATUSHEIGHT * safezoneH;				
				style = 2;
				type = 13;
				text = "";	
				size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[] = COL_GREY;
			};
			
			class BR_player_ThirstBG: RscText
			{	
				idc = -1;
				x = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005"; //,,(safezoneX + safezoneW - 11 * (((safezoneW / safezoneH) min 1.2) / 20))]) + 0.005";
				y = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.03"; //,,(safezoneY + safezoneH - 20.5 *((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + 0.74";
				w = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01"; //,,		(10 * 			(			((safezoneW / safezoneH) min 1.2) / 40))]) - 0.01";
				h = PSTATUSHEIGHT * safezoneH;				
				style = 2;
				type = 13;
				text = "";	
				size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[] = COL_GREY;
			};
		};
		class Controls 
		{
			class BR_player_Health: RscText
			{	
				idc = 1101;
				x = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005"; //,,(safezoneX + safezoneW - 11 * (((safezoneW / safezoneH) min 1.2) / 20))]) + 0.005";
				y = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.008"; //,,(safezoneY + safezoneH - 20.5 *((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + 0.718";
				w = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01"; //,,		(10 * 			(			((safezoneW / safezoneH) min 1.2) / 40))]) - 0.01";
				h = PSTATUSHEIGHT * safezoneH;
				style = 2;
				type = 13;
				text = "";	
				size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[] = COL_GREEN;
			};
			
			class BR_player_Thirst: RscText
			{	
				idc = 1201;
				x = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005"; //,,(safezoneX + safezoneW - 11 * (((safezoneW / safezoneH) min 1.2) / 20))]) + 0.005";
				y = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.03"; //,,(safezoneY + safezoneH - 20.5 *((((safezoneW / safezoneH) min 1.2) / 1.2) / 25))]) + 0.74";
				w = "(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01"; //,,		(10 * 			(			((safezoneW / safezoneH) min 1.2) / 40))]) - 0.01";
				h = PSTATUSHEIGHT * safezoneH;
				style = 2;
				type = 13;
				text = "";	
				size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[] = COL_BLUE;
			};
		};
	};	
};
class RscBRButton : RscButton
{
	sizeEx = 1.2 * GUI_GRID_H;
	font = GUI_FONT_BOLD;
	
	colorText[] = {1,1,1,1}; // Text color
	colorDisabled[] = {1,1,1,0.5}; // Disabled text color

	colorBackground[] = {0.2,0.2,0.2,1}; // Fill color
	colorBackgroundDisabled[] = {0,0,0,0.5}; // Disabled fill color
	colorBackgroundActive[] = {0,0.3,.6,1}; // Mouse hover fill color
	colorFocused[] = {0.2,0.2,0.2,1}; // Selected fill color (oscillates between this and colorBackground)
};
class RscBRControlsGroup  
 {
 	type = CT_CONTROLS_GROUP;
 	idc = -1;
 	style = ST_MULTI;
 	x = (safeZoneX + (SafezoneW * 0.0163));  // scalability code which resizes correctly no matter what gui size or screen dimensions is used
 	y = (safeZoneY + (SafezoneH * 0.132));   
 	w = (SafezoneW  * 0.31);                 
 	h = (SafezoneH  * 0.752);              
 	
 	class VScrollbar 
 	{
 		color[] = {0.5, 0.5, 0.5, 1};
 		width = 0.025;
 		autoScrollSpeed = -1;
 		autoScrollDelay = 0;
 		autoScrollRewind = 0;
	     arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa"; // Arrow 
	     arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa"; // Arrow when clicked on 
	     border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa"; // Slider background (stretched vertically) 
	     thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa"; // Dragging element (stretched vertically) 
 	};
 	
 	class HScrollbar 
 	{
 		color[] = {1, 1, 1, 1};
 		height = 0.028;
 	};
 	
 	class ScrollBar
 	{
 		color[] = {1,1,1,0.6};
 		colorActive[] = {1,1,1,1};
 		colorDisabled[] = {1,1,1,0.3};
	     arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa"; // Arrow 
	     arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa"; // Arrow when clicked on 
	     border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa"; // Slider background (stretched vertically) 
	     thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa"; // Dragging element (stretched vertically) 
 	};
 	
 	class Controls {};
 };

class RscBRRulesMessageBox 
{
	idd = 32200;
	onLoad = "uiNamespace setVariable ['RscBRRulesMessageBox', _this select 0];";
	onUnload = "uiNamespace setVariable ['RscBRRulesMessageBox', displayNull];";
	class controlsBackground
	{
		class RscFrame_1800: RscStructuredText
		{	
			text="";
			idc = 1800;
			x = 0;
			y = 0;
			w = 1;
			h = 1;
			//colorBackground[] = {0,0,0,.4};
			colorBackground[] = {0,0,0,.8};
			colorText[] = {1,1,1,0}; 
		};
	};
	class Controls
	{
		class myControl: RscBRControlsGroup { 
			idc = 200908;
			x = .025;
			y = .025;
			w = .95;
			h = .825;
			colorBackground[] = {0,0,0,.6};
			class Controls {
				class stxtMessage: RscStructuredText
				{
					idc = 1100;
					text = "";
					x = 0;
					y = 0;
					w = .925;
					h = 1;
					colorBackground[] = {0,0,0,.2};
					colorText[] = {1,1,1,1};
				};
			};
		};
		class btnOK: RscBRButton
		{
			action = "closeDialog 0;";
			text = "O.K.";
			idc = 1600;
			x = .5 - .05;
			y = .875;
			w = .1;
			h = .1;
			sizeEx = 2 * GUI_GRID_H;
			font = GUI_FONT_BOLD;
			colorText[] = {1,1,1,1}; // Text color
			colorDisabled[] = {1,1,1,0.7}; // Disabled text color
			colorBackground[] = {0.2,0.2,0.2,1}; // Fill color
			colorBackgroundDisabled[] = {0,0,0,0.5}; // Disabled fill color
			colorBackgroundActive[] = {0,0.3,.6,1}; // Mouse hover fill color
			colorFocused[] = {1,1,1,1}; // Selected fill color (oscillates between this and colorBackground)
		};
	};
};
// Stormridge: Custom Menu and auto-browse server list from the button click.
class RscDisplayMain: RscStandardDisplay
{
	class Spotlight
	{
		delete EastWind;
		delete ApexProtocol;
		class Bootcamp
		{
			text = "play";
			textIsQuote = 0;
			picture = "\br_client\images\br_logo_main_menu.paa";
			video = "";
			action = "_display = ctrlparent(_this select 0); ctrlactivate ((_display) displayctrl 105); _display = findDisplay 8; _control = _display displayctrl 159; ctrlActivate _control;_titleControl = _display displayCtrl 1000;_titleControl ctrlSetText 'Welcome to BATTLE ROYALE!';";
			actionText = "BROWSE BATTLE ROYALE SERVERS";
			condition = "(getstatvalue 'BCFirstDeployment' == 0)";
		};
	};
};
class RscDisplayMainMap
{
	class controls
	{
		class CA_MissionName: RscText
		{
			onload = "_this call BR_Client_GUI_MapMenu_MonitorMenuUpdate";
			idc = 112;
			shadow = 0;
			font = "RobotoCondensedLight";
			text = "";
			x = "1 * 					(			((safezoneW / safezoneH) min 1.2) / 40) + 		(safezoneX)";
			y = "0 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 		(safezoneY)";
			w = "14 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorText[] = {1,1,1,1};
			sizeEx = "1.4 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};
class RscDisplayDiary
{
	class controls
	{
		class CA_MissionName: RscText
		{
			onload = "_this call BR_Client_GUI_MapMenu_MonitorMenuUpdate";
			idc = 112;
			shadow = 0;
			font = "RobotoCondensedLight";
			text = "";
			x = "1 * 					(			((safezoneW / safezoneH) min 1.2) / 40) + 		(safezoneX)";
			y = "0 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 		(safezoneY)";
			w = "14 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorText[] = {1,1,1,1};
			sizeEx = "1.4 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};


class BR_RscListboxTeamManager : RscListBox {
	type = 5;
	style = 16;
	font = "PuristaMedium";
	sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	shadow = 0;
	rowHeight = 0;
	colorShadow[] = {0, 0, 0, 0.5};
	colorText[] = {1, 1, 1, 1.0};
	colorDisabled[] = {1, 1, 1, 0.25};
	colorScrollbar[] = {1, 0, 0, 0};
	colorSelect[] = {0, 0, 0, 1};
	colorSelect2[] = {0, 0, 0, 1};
	colorSelectBackground[] = {0.95, 0.95, 0.95, 1};
	colorSelectBackground2[] = {1, 1, 1, 0.5};
	period = 1.2;
	colorBackground[] = {0, 0, 0, 0.3};
	maxHistoryDelay = 1.0;
	colorPicture[] = {1, 1, 1, 1};
	colorPictureSelected[] = {1, 1, 1, 1};
	colorPictureDisabled[] = {1, 1, 1, 1};
	tooltipColorText[] = {1, 1, 1, 1};
	tooltipColorBox[] = {1, 1, 1, 1};
	tooltipColorShade[] = {0, 0, 0, 0.65};
	soundSelect[] = {"\A3\ui_f\data\sound\RscListbox\soundSelect", 0.09, 1};
	
	class ListScrollBar {
		autoScrollEnabled = 1;
		color[] = {1, 1, 1, 0.6};
		colorActive[] = {1, 1, 1, 1};
		colorDisabled[] = {1, 1, 1, 0.3};
		thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
		arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		shadow = 0;
		scrollSpeed = 0.06;
		width = 0;
		height = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
	};
};
class BR_RscButtonTeamManager : RscButton {
	type = 1;
	style = 2;
	shadow = 2;
	font = "PuristaMedium";
	sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1,1,1,1}; // Text color
	colorDisabled[] = {1,1,1,0.5}; // Disabled text color
	colorBackground[] = {0.4,0.4,0.4,1}; // Fill color
	colorBackgroundDisabled[] = {0,0,0,0.5}; // Disabled fill color
	colorBackgroundActive[] = {0,0.3,.6,1}; // Mouse hover fill color
	colorFocused[] = {.35,.35,.35,1}; // Selected fill color (oscillates between this and colorBackground)
};
class BR_RscTextTeamManager : RscText {
	type = 0;
	style = 0;
	shadow = 0;
	colorShadow[] = {0, 0, 0, 0.5};
	font = "PuristaMedium";
	SizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1,1,1,1};
	colorBackground[] = {0,0.3,.6,.8};
	colorActive[] = {0,0,0,1};
	linespacing = 1;
	tooltipColorText[] = {1, 1, 1, 1};
	tooltipColorBox[] = {1, 1, 1, 1};
	tooltipColorShade[] = {0, 0, 0, 0.65};
};

class RscBRTeamManager
{
	idd = 7200;
	duration = 999999;
	fadein = 1;
	fadeout = 1;
	onLoad = "uiNamespace setVariable ['RscBRTeamManager', _this select 0]; _this call BR_Client_GUI_Teams_InitializeMenu; true call BR_Client_GUI_Teams_UI_ToggleDialogBlur";
	onUnload = "uiNamespace setVariable ['RscBRTeamManager', displayNull]; false call BR_Client_GUI_Teams_UI_ToggleDialogBlur";
	class controlsBackground
	{
		class backDropDark : RscText
		{
			idc = 1803;
			x = safeZoneX;
			y = safeZoneY;
			w = safeZoneW;
			h = safeZoneH;
			colorBackground[] = {0,0,0,.2};
			text="";
		};
		class frameBackground : RscText
		{
			idc = 1803;
			x = 0 * GUI_GRID_W + GUI_GRID_X;
			y = 0 * GUI_GRID_H + GUI_GRID_Y;
			w = 40 * GUI_GRID_W;
			h = 25.5 * GUI_GRID_H; //24
			colorBackground[] = {0,0,0,.8};
			text="";
		};
		class frameYourTeam: RscFrame
		{
			idc = 1800;
			x = 22.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 17 * GUI_GRID_W;
			h = 10.5 * GUI_GRID_H; // 17 usually
			colorText[] = {.7,.7,.7,1};	// Frame Color
		};
		class frameInviteOthers: RscFrame
		{
			idc = 1801;
			x = 0.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 17 * GUI_GRID_W;
			h = 20.5 * GUI_GRID_H;  //11.5
			colorText[] = {.7,.7,.7,1};
		};
		class frameInvites4U: RscFrame
		{
			idc = 1802;
			x = 22.5 * GUI_GRID_W + GUI_GRID_X;
			y = 15 * GUI_GRID_H + GUI_GRID_Y;
			w = 17 * GUI_GRID_W;
			h = 8.5 * GUI_GRID_H;
			colorText[] = {.7,.7,.7,1};
		};		
	};
	class controls
	{
		class Title: RscStructuredText
		{
			idc = 7201;
			style = ST_VCENTER;
			text = "<t size='1.4' valign='bottom' font='PuristaMedium' align='left'>Team Menu</t><t size='.8' valign='bottom' align='right' font='PuristaMedium'>      Create and Manage your team here.</t>";
			x = 0.5 * GUI_GRID_W + GUI_GRID_X;
			y = 0.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 39 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
			colorBackground[] = {0,0,0,1};
			colorActive[] = {0,0,0,1};
		};
		////////////////////////////////////////////////////////////////////////////////////////////////
		class TitleInvitePlayers: BR_RscTextTeamManager
		{
			idc = 7207;
			text = "Invite Players to your Team"; 
			x = 0.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 17 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class InvitePlayerListRefresh : RscActiveText
		{
			style = ST_PICTURE;
			idc = 7220;
			text = "br_client\images\GUI\TeamManager\Refresh.paa";
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3.25 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			color[] = {1,1,1,0.8};
			colorActive[] = {1,1,1,1};
			colorDisabled[] = {1,1,1,1};
		};
		class PlayerList: BR_RscListboxTeamManager
		{
			idc = 7204;
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 16 * GUI_GRID_H; // 7
		};
		class ButtonInvite: BR_RscButtonTeamManager
		{
			idc = 7202;
			text = "Invite Selected Player"; 
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;	//12.5
			w = 16 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		////////////////////////////////////////////////////////////////////////////////////////////////
		class TitleYourTeam: BR_RscTextTeamManager
		{
			idc = 7205;
			text = "Your Team"; 
			x = 22.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 17 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			//colorBackground[] = {0.4,0,0,.8};
		};
		class TeamList: BR_RscListboxTeamManager
		{
			idc = 7203;
			x = 23 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 4 * GUI_GRID_H;
		};
		class ButtomRemovePlayer: BR_RscButtonTeamManager
		{
			idc = 7206;
			text = "Remove Selected Player"; 
			x = 23 * GUI_GRID_W + GUI_GRID_X;
			y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class ButtonLeaveTeam: BR_RscButtonTeamManager
		{
			idc = 7208;
			text = "Leave your Team"; 
			x = 23 * GUI_GRID_W + GUI_GRID_X;
			y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		////////////////////////////////////////////////////////////////////////////////////////////////
		class TitleInvitesForYou: BR_RscTextTeamManager
		{
			idc = 7209;
			text = "Invites for You"; 
			x = 22.5 * GUI_GRID_W + GUI_GRID_X;
			y = 15 * GUI_GRID_H + GUI_GRID_Y;
			w = 17 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class InviteListForYou: BR_RscListboxTeamManager
		{
			idc = 7210;
			x = 23 * GUI_GRID_W + GUI_GRID_X;
			y = 17 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 4 * GUI_GRID_H;
		};
		class ButtonAcceptInvite: BR_RscButtonTeamManager
		{
			idc = 7211;
			text = "Accept Team Invite"; 
			x = 23 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 16 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		////////////////////////////////////////////////////////////////////////////////////////////////
		class CheckboxMuteInvites: RscCheckbox
		{
			idc = 7221;
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 23.75 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			color[] = {0.7,0.7,0.7,0.7};
			colorFocused[] = {0.7,0.7,0.7,1};
			colorHover[] = {0.7,0.7,0.7,1};
			colorPressed[] = {0.7,0.7,0.7,1};
			colorDisabled[] = {1,1,1,0.2};
			colorBackground[] = {0,0,0,0};
			colorBackgroundFocused[] = {0,0,0,0};
			colorBackgroundHover[] = {0,0,0,0};
			colorBackgroundPressed[] = {0,0,0,0};
			colorBackgroundDisabled[] = {0,0,0,0};

		};
		class TitleMuteInvites : RscText {
			type = 0;
			idc=7222;
			text = "Mute invite notifications";
			style = ST_LEFT;
			x = 2.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.75 * GUI_GRID_H + GUI_GRID_Y;
			w = 39 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			font = "PuristaMedium";
			SizeEx = "1 * (			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			colorText[] = {.7,.7,.7,1};
			colorBackground[] = {0,0,0,0};
			shadow = 0;
			colorShadow[] = {0, 0, 0, 0.5};
			colorActive[] = {1,1,1,1};
			linespacing = 1;
			tooltipColorText[] = {1, 1, 1, 1};
			tooltipColorBox[] = {1, 1, 1, 1};
			tooltipColorShade[] = {0, 0, 0, 0.65};
		};
	};	
};

class BR_IGUIBack {
	type = 0;
	idc = 124;
	style = 128;
	text = "";
	colorText[] = {0, 0, 0, 0};
	font = "PuristaMedium";
	sizeEx = 0.0;
	shadow = 0;
	x = 0.1;
	y = 0.1;
	w = 0.1;
	h = 0.1;
	colorbackground[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])", "(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])", "(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"};
};
class BR_RscButton {
	type = 1;
	style = 2;
	x = 0;
	y = 0;
	w = 0.095589;
	h = 0.039216;
	shadow = 2;
	font = "PuristaMedium";
	sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1, 1, 1, 1.0};
	colorDisabled[] = {1, 1, 1, 0.25};
	colorBackground[] = {0, 0, 0, 0.5};
	colorBackgroundActive[] = {0, 0, 0, 1};
	colorBackgroundDisabled[] = {0, 0, 0, 0.5};
	colorFocused[] = {0, 0, 0, 1};
	colorShadow[] = {0, 0, 0, 0};
	offsetX = 0;
	offsetY = 0;
	offsetPressedX = 0;
	offsetPressedY = 0;
	colorBorder[] = {0, 0, 0, 1};
	borderSize = 0.0;
	soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter", 0.09, 1};
	soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush", 0.09, 1};
	soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick", 0.09, 1};
	soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape", 0.09, 1};
};
class BR_RscText {
	type = 0;
	x = 0;
	y = 0;
	h = 0.037;
	w = 0.3;
	style = 0;
	shadow = 1;
	colorShadow[] = {0, 0, 0, 0.5};
	font = "PuristaMedium";
	SizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1, 1, 1, 1.0};
	colorBackground[] = {0, 0, 0, 0};
	linespacing = 1;
	tooltipColorText[] = {1, 1, 1, 1};
	tooltipColorBox[] = {1, 1, 1, 1};
	tooltipColorShade[] = {0, 0, 0, 0.65};
};
class BR_RscListbox {
	type = 5;
	style = 16;
	font = "PuristaMedium";
	sizeEx = "(			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	shadow = 0;
	rowHeight = 0;
	colorShadow[] = {0, 0, 0, 0.5};
	colorText[] = {1, 1, 1, 1.0};
	colorDisabled[] = {1, 1, 1, 0.25};
	colorScrollbar[] = {1, 0, 0, 0};
	colorSelect[] = {0, 0, 0, 1};
	colorSelect2[] = {0, 0, 0, 1};
	colorSelectBackground[] = {0.95, 0.95, 0.95, 1};
	colorSelectBackground2[] = {1, 1, 1, 0.5};
	period = 1.2;
	colorBackground[] = {0, 0, 0, 0.3};
	maxHistoryDelay = 1.0;
	colorPicture[] = {1, 1, 1, 1};
	colorPictureSelected[] = {1, 1, 1, 1};
	colorPictureDisabled[] = {1, 1, 1, 1};
	tooltipColorText[] = {1, 1, 1, 1};
	tooltipColorBox[] = {1, 1, 1, 1};
	tooltipColorShade[] = {0, 0, 0, 0.65};
	soundSelect[] = {"\A3\ui_f\data\sound\RscListbox\soundSelect", 0.09, 1};
	
	class ListScrollBar {
		autoScrollEnabled = 1;
		color[] = {1, 1, 1, 0.6};
		colorActive[] = {1, 1, 1, 1};
		colorDisabled[] = {1, 1, 1, 0.3};
		thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
		arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		shadow = 0;
		scrollSpeed = 0.06;
		width = 0;
		height = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
	};
};
class BR_SkinSelection {
	idd = 1339;
	
	movingEnable = 0;
	enableSimulation = 0;
	name = "BR_SkinSelection";
	onLoad = "[_this select 0] call BR_Client_GUI_Skins_InitSkinSelectionMenu";
	onUnload = "[] call BR_Client_GUI_Skins_ResetTexture;";
	
	class controlsBackground {};
	
	class Controls {
		class IGUIBack_2200: BR_IGUIBack
		{
			idc = 2200;
			x = 0.25;
			y = 0.16;
			w = 0.5;
			h = 0.80;
		};
		class RscText_1000: BR_RscText
		{
			idc = 1000;
			text = "Select a Skin";
			x = 0.425;
			y = 0.16;
			w = 0.15;
			h = 0.04;
		};
		class RscListbox_1500: BR_RscListbox
		{
			idc = 1500;
			x = 0.275;
			y = 0.22;
			w = 0.45;
			h = 0.66;
		};
		class RscButton_1600: BR_RscButton
		{
			idc = 1600;
			text = "Apply Skin";
			x = 0.275;
			y = 0.9;
			w = 0.45;
			h = 0.04;
		};
		class RscButton_1601: BR_RscButton
		{
			idc = 1601;
			text = "X";
			x = 0.7375;
			y = 0.14;
			w = 0.025;
			h = 0.04;
		};
	};
};

class RscNotificationArea: RscControlsGroupNoScrollbars
{
	idc = 312;
	x = "0 * 				(			((safezoneW / safezoneH) min 1.2) / 40) + 	(profilenamespace getvariable [""IGUI_GRID_NOTIFICATION_X"",	(0.5 - 6 * 			(			((safezoneW / safezoneH) min 1.2) / 40))])";
	y = "0 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 	(profilenamespace getvariable [""IGUI_GRID_NOTIFICATION_Y"",	(safezoneY + 6.5 * 			(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25))])";
	w = "12 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
	h = "6 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	class controls
	{
		class Notification: RscControlsGroupNoScrollbars
		{
			x = 0;
			y = 0;
			idc = 13435;
			w = "12 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "3 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			class controls
			{
				class Title: RscText
				{
					colorBackground[] = {0,0,0,0.7};
					idc = 12135;
					x = "0 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					y = "0 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w = "12 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					h = "0.8 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					sizeEx = "0.8 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class PictureBackground: RscText
				{
					colorBackground[] = {0,0.2,.4,.8};
					//	colorBackground[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.7};
					idc = 12136;
					x = "0 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					y = "0.9 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w = "2 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					h = "2 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class Picture: RscPictureKeepAspect
				{
					idc = 12335;
					x = "0.1 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					y = "1 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w = "1.8 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					h = "1.8 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class Score: RscText
				{
					shadow = 0;
					style = 2;
					idc = 12137;
					x = "0 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					y = "0.9 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w = "2 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					h = "2 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					sizeEx = "1.2 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class DescriptionBackground: RscText
				{
					colorBackground[] = {0,0.2,.4,.8};
					//colorBackground[] = {"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])","(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])","(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",0.7};
					idc = 12138;
					x = "2.1 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					y = "0.9 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w = "9.9 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					h = "2 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class Description: RscStructuredText
				{
					class Attributes
					{
						size = 0.8;
						align = "center";
					};
					idc = 12235;
					x = "2.1 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					y = "0.9 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w = "9.9 * 				(			((safezoneW / safezoneH) min 1.2) / 40)";
					h = "2 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					colorBackground[] = {0,0,0,0};
					sizeEx = "1 * 				(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
			};
		};
	};
};