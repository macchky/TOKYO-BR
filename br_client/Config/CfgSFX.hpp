class CfgSFX {
	// STRM: Reduce the silly house destruction noise.
	class DestrHouse
	{
		sounds[] = {"DestrHouse1","DestrHouse2","DestrHouse3"};
		DestrHouse1[] = {"A3\Sounds_F\sfx\special_sfx\house_destroy_1",0.6912509,1,900,0.33,0,2,5};
		DestrHouse2[] = {"A3\Sounds_F\sfx\special_sfx\house_destroy_2",0.6912509,1,900,0.33,0,2,5};
		DestrHouse3[] = {"A3\Sounds_F\sfx\special_sfx\house_destroy_3",0.6912509,1,900,0.34,0,2,5};
		empty[] = {"",0.8912509,1,500,1,0,2,5};
	};
	class DestrHousePart
	{
		sounds[] = {"DestrHousePart1"};
		DestrHousePart1[] = {"A3\Sounds_F\sfx\special_sfx\house_destroy_3",0.8912509,1,900,1,0,2,5};
		empty[] = {"",0,0,0,0,1,5,20};
	};
};
