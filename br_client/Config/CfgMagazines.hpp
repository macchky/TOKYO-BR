class CfgMagazines {

	class CA_Magazine;

	class CUP_20Rnd_B_AA12_Pellets: CA_Magazine
	{
		author="$STR_CUP_AUTHOR_STRING";
		scope=2;
		displayName="$STR_CUP_dn_aa12_20rnd_pellets_M";
		ammo="CUP_B_12Gauge_Pellets";
		count=6;
		initSpeed=396;
		picture="\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_aa12_ca.paa";
		descriptionShort="$STR_CUP_dss_aa12_20rnd_pellets_M";
		displayNameShort="Pellets";
		mass=26;
	};

	class CUP_1Rnd_HEDP_M203;
	class CUP_6Rnd_HE_M203: CUP_1Rnd_HEDP_M203
	{
		mass=80;
	};

	class HandGrenade;
	class xmas_explosive_present: HandGrenade
	{
		mass=15;
		displayName="Explosive Gift";
		displayNameShort="Explosive Gift";
		descriptionShort="Don't shake.  Throw it instead.";
	};

	class ItemRedgull : CA_Magazine {
		scope = 2;
		count = 1;
		type = 256;
		displayName = "Red Gull";
		descriptionUse = "A can of Red Gull.";
		model = "\A3\Structures_F\Items\Food\Can_V3_F.p3d";
		picture = "\br_client\images\items\redgull.paa";
		descriptionShort = "A can of Red Gull.";
		mass = 10;
	};	
	class ItemPainkiller : CA_Magazine {
		scope = 2;
		count = 1;
		type = 256;
		displayName = "Painkillers";
		descriptionUse = "A box of painkillers.";
		model = "\A3\Structures_F_EPA\Items\Medical\PainKillers_F.p3d";
		picture = "\br_client\images\items\paink.paa";
		descriptionShort = "A box of painkillers.";
		mass = 6;
	};
	class ItemBRToolKit: CA_Magazine
	{
		scope = 2;
		displayName = "Vehicle Repair Kit";
		descriptionShort = "Repairs vehicles, but can't bring your fiery wreck back to life.<br/>Satisfaction not guaranteed.";
		picture = "\A3\Weapons_F\Items\data\UI\gear_Toolkit_CA.paa";
		model = "\A3\Weapons_F\Items\Toolkit";
		mass = 40;	// 80 by default  (320 space in big backpack) but 80 doesn't fit in vest.
	};

};