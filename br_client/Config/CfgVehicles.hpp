class CfgVehicles {
	
	class Man;
	
	class CAManBase: Man {
		delete SoundBreathInjured;
		delete SoundInjured;
	};
	
	class C_man_1;
	
	class B_BattleRoyalePlayer_F : C_man_1 {
		side = 3;
		author = "BattleRoyaleGames";
		_generalMacro = "B_BattleRoyalePlayer_F";
		scope = 2;
		scopeArsenal = 2; 
		model = "\A3\Characters_F\Civil\c_poloshirtpants.p3d";
		displayName = "Battle Royale Player";
		cost = 10;
		identityTypes[] = {"Head_Rangemaster", "G_Rangemaster"};
		nakedUniform = "U_BasicBody";
		uniformClass = "U_Rangemaster";
		Items[] = {};
		respawnItems[] = {};
		class EventHandlers {
			init = "";
		};
		hiddenSelectionsTextures[] = {"a3\characters_f_kart\civil\data\c_poloshirtpants_2_co.paa"};
		weapons[] = {"Throw", "Put"};
		
        canDeactivateMines = true;              // Can this character deactivate mines?
        engineer = true;                        // Can this character repair vehicles?
        attendant = true;                          // Can this character heal soldiers?
		respawnWeapons[] = {"Throw", "Put"};
		magazines[] = {};
		respawnMagazines[] = {};
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "ItemGPS"};
		respawnlinkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "ItemGPS"};
		
		class Wounds {
			tex[] = {};
			mat[] = {"A3\Characters_F\Civil\Data\c_poloshirtpants.rvmat", "A3\Characters_F\Civil\Data\c_poloshirtpants_injury.rvmat", "A3\Characters_F\Civil\Data\c_poloshirtpants_injury.rvmat", "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat", "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat", "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat", "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_white_old.rvmat", "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat", "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat", "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat", "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat", "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"};
		};
	};

	// STRM: Removing armor for the following clothing:
	//		IRA_Soldier_Outfit_6, IRA_Soldier_Outfit_8
	//		U_O_Wetsuit, U_O_GhillieSuit, U_O_OfficerUniform_ocamo, U_O_SpecopsUniform_ocamo, U_O_CombatUniform_oucamo
	class SoldierEB;
	class O_Soldier_base_F: SoldierEB
	{
		class HitPoints
		{
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis
			{
				armor = 1;
				material = -1;
				name = "pelvis";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitAbdomen: HitPelvis
			{
				armor = 1;
				material = -1;
				name = "spine1";
				passThrough = 0.1;
				radius = 0.15;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 1;
				material = -1;
				name = "spine2";
				passThrough = 0.1;
				radius = 0.15;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 1;
				material = -1;
				name = "spine3";
				passThrough = 0.1;
				radius = 0.15;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 0.1;
				radius = 0.16;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms
			{
				armor = 1;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitHands: HitArms
			{
				armor = 1;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs
			{
				armor = 1;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.12;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
		};
		armor = 2;
	};

	// STRM : Start SUV fix.
	class Car;
	class Car_F: Car
	{
		crewCrashProtection=.7;
		class HitPoints
		{
			class HitLFWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRF2Wheel;
			class HitBody;
			class HitFuel;
			class HitEngine;

			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
		};
	};
	class SUV_01_base_F: Car_F
	{
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor = .55;
			};
			class HitGlass2: HitGlass2
			{
				armor = .55;
			};
			class HitGlass3: HitGlass3
			{
				armor = .55;
			};
			class HitGlass4: HitGlass4
			{
				armor = .55;
			};
			class HitGlass5: HitGlass5
			{
				armor = .55;
			};
			class HitGlass6: HitGlass6
			{
				armor = .55;
			};
		};
	};
	class Offroad_01_base_F: Car_F
	{
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor = 0.55;
			};
			class HitGlass2: HitGlass2
			{
				armor = 0.55;
			};
		};
	};
	class Hatchback_01_base_F: Car_F
	{
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor = 0.55;
			};
			class HitGlass2: HitGlass2
			{
				armor = 0.55;
			};
			class HitGlass3: HitGlass3
			{
				armor = 0.55;
			};
			class HitGlass4: HitGlass4
			{
				armor = 0.55;
			};

		};
	};
	// STRM : Start beetle fix.
	class beetle_custom: Car_F
	{
		armor = 30; // Same as offroad
		armorStructural=1;	// 1 = no reduction
		wheelDamageThreshold=0.19999999;
		wheelDestroyThreshold=0.39000001;
		wheelDamageRadiusCoef=0.94999999;
		wheelDestroyRadiusCoef=0.44999999;
		class HitPoints: HitPoints
		{
			class HitLFWheel: HitLFWheel
			{
				armor = .75;
				passThrough = 0;
				radius=0.33000001;
			};
			class HitLF2Wheel: HitLF2Wheel
			{
				armor = .75;
				passThrough = 0;
				radius=0.33000001;
			};
			class HitRFWheel: HitRFWheel
			{
				armor = .75;
				passThrough = 0;
				radius=0.33000001;
			};
			class HitRF2Wheel: HitRF2Wheel
			{
				armor = .75;
				passThrough = 0;
				radius=0.33000001;
			};
			class HitFuel
			{
				armor=0.2;
				material=-1;
				name="palivo";
				visual="";
				passThrough=1;
				minimalHit=0.0099999998;
				explosionShielding=0.1;
				radius=0.25;
			};
			class HitEngine
			{
				armor=0.89999998;
				material=-1;
				name="motor";
				visual="";
				passThrough=0.2;
				minimalHit=0.0099999998;
				explosionShielding=0.40000001;
				radius=0.44999999;
			};
			class HitBody
			{
				armor=.3;
				material=-1;
				name="karoserie";
				visual="zbytek";
				passThrough=1;
				minimalHit=0.0099999998;
				explosionShielding=0.60000002;
				radius=0.44999999;
			};
			class HitGlass1: HitGlass1
			{
				armor=.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass2: HitGlass2
			{
				armor=.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass5: HitGlass5
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass6: HitGlass6
			{
				armor=.25;
				explosionShielding=0;
				radius=0.25;
			};
		};
	};
	class Truck_F: Car_F
	{
		crewCrashProtection = .7;
		epeImpulseDamageCoef = 20; // 30=glass 15=Godmode
		class HitPoints: HitPoints
		{
			class HitRGlass;
			class HitLGlass;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitBody;
			class HitFuel;
			class HitLFWheel;
			class HitLBWheel;
			class HitLMWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRBWheel;
			class HitRMWheel;
			class HitRF2Wheel;
			class HitEngine;
		};
	};
	// STRM : Start Boxtruck fix.
	class Van_01_base_F: Truck_F
	{
		armor = 30;	// normally 80
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor = 0.55;	// normally .05
			};
			class HitGlass2: HitGlass2
			{
				armor = 0.55;
			};
			class HitGlass3: HitGlass3
			{
				armor = 0.55;
			};
			class HitGlass4: HitGlass4
			{
				armor = 0.55;
			};
		};
	};
	// STRM : Start Zemak fix.
	class Truck_02_base_F: Truck_F
	{
		armor = 80;  // normally 100.  SUV=30
		wheelDamageThreshold = .7;  // .7
		wheelDestroyThreshold = 0.99; // .99
		wheelDamageRadiusCoef = .95; // .95
		wheelDestroyRadiusCoef = .6; // .6

		// 3 shots.. car tilts after one.
		// wheelDamageThreshold = 0.1;  // .7
		// wheelDestroyThreshold = 0.2; // .99
		// wheelDamageRadiusCoef = 0.1; // .95
		// wheelDestroyRadiusCoef = 0.1; // .6

		class HitPoints: HitPoints
		{
			class HitLFWheel: HitLFWheel
			{
				armor = 0.7;	// .75 by default.
				radius = 0.2;
			};
			class HitLF2Wheel: HitLF2Wheel
			{
				armor = 0.7;
				radius = 0.2;
			};
			class HitRFWheel: HitRFWheel
			{
				armor = 0.7;
				radius = 0.2;
			};
			class HitRF2Wheel: HitRF2Wheel
			{
				armor = 0.7;
				radius = 0.2;
			};
			class HitFuel: HitFuel
			{
				name = "palivo";
				armor = 2;
				radius = 0.45;
			};
			class HitEngine: HitEngine
			{
				name = "engine";
				armor = 4;
				radius = 0.25;
			};
			class HitGlass1: HitGlass1
			{
				armor = .2;	// normally 2 = 7 shots with M4
				radius = 0.5;
			};
			class HitGlass2: HitGlass2
			{
				armor = .2;
				radius = 0.5;
			};
			class HitGlass3: HitGlass3
			{
				armor = .2;
				radius = 0.5;
			};
			class HitGlass4: HitGlass4
			{
				armor = .2;
				radius = 0.5;
			};
			class HitGlass5: HitGlass5
			{
				armor = .2;
				radius = 0.5;
			};
			class HitGlass6: HitGlass6
			{
				armor = .2;
				radius = 0.5;
			};
		};
	};
};