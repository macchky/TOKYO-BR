class CfgRemoteExec     //https://community.bistudio.com/wiki/CfgRemoteExec, https://community.bistudio.com/wiki/Arma_3_Remote_Execution
{
        class Functions
        {
                mode = 1;
                jip = 0;
                class bis_fnc_reviveinitaddplayer
                {
                        allowedTargets = 2;             // server, 1=all clients       
                };
                class BR_Server_Network_ReceiveMessage
                {
                        allowedTargets = 2;             // server, 1=all clients
                };
                class BIS_fnc_execVM
                {
                        allowedTargets = 2;             // server, 1=all clients
                };
                class BIS_fnc_effectKilledAirDestruction 
                {
                        allowedTargets = 2;             // server, 1=all clients
                };
                class BIS_fnc_effectKilledSecondaries
                {
                        allowedTargets = 2;             // server, 1=all clients
                };
                class BIS_fnc_objectVar 
                {
                        allowedTargets = 2;             // server, 1=all clients
                };
        };
        class Commands
        {
                mode = 1;
                class call {allowedTargets=0; jip=0;};
        };
};
