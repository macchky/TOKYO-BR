class CfgSkins {
	uniform = "U_Rangemaster";

	class Clear {
		name = "Default Skin";
		texture = "a3\characters_f_kart\civil\data\c_poloshirtpants_2_co.paa";
	};
	
	class Glitch {
		name = "Glitch";
		texture = "\br_client\images\Skins\glitch.paa";
	};
	class Glitch2 {
		name = "Glitch2";
		texture = "\br_client\images\Skins\glitch2.paa";
	};
	class Punk {
		name = "Punk";
		texture = "\br_client\images\Skins\punk.paa";
	};
	class Tux {
		name = "Tux";
		texture = "\br_client\images\Skins\tux.paa";
	};	
	class Shark {
		name = "Shark";
		texture = "\br_client\images\Skins\KP.paa";
	};
	class kappa {
		name = "Kappa";
		texture = "\br_client\images\Skins\kappa.paa";
	};
	class salt {
		name = "PJSalt";
		texture = "\br_client\images\Skins\SALT.paa";
	};
	class America {
		name = "Stars";
		texture = "\br_client\images\Skins\america.paa";
	};
	class Merica {
		name = "Merica";
		texture = "\br_client\images\Skins\merica.paa";
	};
	class Ireland {
		name = "Ireland";
		texture = "\br_client\images\Skins\ireland.paa";
	};
	class UK {
		name = "UK";
		texture = "\br_client\images\Skins\UK.paa";
	};
	class Germany {
		name = "Germany";
		texture = "\br_client\images\Skins\germany.paa";
	};
	class Scotland {
		name = "Scotland";
		texture = "\br_client\images\Skins\scotland.paa";
	};
	class France {
		name = "France";
		texture = "\br_client\images\Skins\FR.paa";
	};
	class BleedPurple {
		name = "#BleedPurple";
		texture = "\br_client\images\Skins\twitch.paa";
	};
	class Thump {
		name = "Thump";
		texture = "\br_client\images\Skins\thump.paa";
	};
	class Jumper {
		name = "TisTheSeason";
		texture = "\br_client\images\Skins\xmas.paa";
	};
	class North {
		name = "North";
		texture = "\br_client\images\Skins\North.paa";
	};
	class Canada {
		name = "Canada";
		texture = "\br_client\images\Skins\canada.paa";
	};
	class Romania {
		name = "Romania";
		texture = "\br_client\images\Skins\Romania.paa";
	};
	class Russia {
		name = "Russia";
		texture = "\br_client\images\Skins\Russia.paa";
	};
	class Lederhosen {
		name = "Lederhosen";
		texture = "\br_client\images\Skins\Lederhosen.paa";
	};
	class Netherlands {
		name = "Netherlands";
		texture = "\br_client\images\Skins\Netherlands.paa";
	};
	class Memer4Life {
		name = "Memer4Life";
		texture = "\br_client\images\Skins\Memer4Life.paa";
	};
	class HassanChop {
		name = "HassanChop";
		texture = "\br_client\images\Skins\HassanChop.paa";
	};
	class GrandMaster {
		name = "GrandMaster";
		texture = "\br_client\images\Skins\GrandMaster.paa";
	};
};