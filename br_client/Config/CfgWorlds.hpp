class CfgWorlds 
{
	class DefaultWorld {};
	class CAWorld : DefaultWorld {};
	class Altis : CAWorld
	{	
		class AmbientA3 
		{
			maxCost = 0;
		};		
		class CfgEnvSounds;
		class EnvSounds : CfgEnvSounds
		{
			class Rain
			{
				name = "rain";
				sound[] = {"A3\sounds_f\ambient\rain\rain_new_1",0.1,1,200};  // 0.35481337
				soundNight[] = {"A3\sounds_f\ambient\rain\rain_new_2",0.09,1,200};  //0.31622776
				volume="rain";
			};
			class Sea
			{
				name = "Sea";
				sound[] = {"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",0.12589253,1,200};
				soundNight[] = {"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",0.1,1,200};
				volume = "sea*(1-coast)";
			};
			class Coast
			{
				name = "Coast";
				sound[] = {"A3\sounds_f\ambient\waves\beach_sand_small_waves1",0.12589253,1,200};
				soundNight[] = {"A3\sounds_f\ambient\waves\beach_sand_small_waves1",0.07943282,1,200};
				volume = "coast";
			};
			class WindSlow
			{
				name = "WindSlow";
				sound[] = {"A3\sounds_f\ambient\winds\wind-synth-slow",0.028183825,1};
				volume = "((windy factor[0,0.25])*(windy factor[0.5, 0.25]))-(night*0.25)";
			};
			class WindMedium
			{
				name = "WindMedium";
				sound[] = {"A3\sounds_f\ambient\winds\wind-synth-middle",0.035481334,1};
				volume = "((windy factor[0.33,0.5])*(windy factor[0.8, 0.5]))-(night*0.25)";
			};
			class WindFast
			{
				name = "WindFast";
				sound[] = {"A3\sounds_f\ambient\winds\wind-synth-fast",0.044668358,1};
				volume = "(windy factor[0.66,1])-(night*0.25)";
			};
			class Forest
			{
				name = "Forest";
				sound[] = {"A3\sounds_f\ambient\basics\day_insects_winds5",0.025118863,1};
				volume = "forest*trees*(1-night)";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,12,100,0.1,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,12,100,0.1,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,12,100,0.1,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,12,100,0.1,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_5",0.056234132,1,100,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_9",0.056234132,1,80,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_3",0.031622775,1,20,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,40,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,50,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class ForestNight
			{
				name = "ForestNight";
				sound[] = {"A3\sounds_f\ambient\basics\night_insects_birds_winds4",0.015848929,1};
				volume = "forest*trees*night";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_night_6",0.056234132,1,100,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_night_2",0.031622775,1,70,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_3",0.056234132,1,10,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,40,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,30,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class Houses
			{
				name = "Houses";
				sound[] = {"A3\sounds_f\ambient\basics\day_insects_winds4",0.022387212,1};
				volume = "(houses-0.75)*4";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_7",0.056234132,1,100,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_8",0.031622775,1,100,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_4",0.056234132,1,15,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,20,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,25,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class HousesNight
			{
				name = "HousesNight";
				sound[] = {"A3\sounds_f\ambient\basics\night_insects_birds_nowinds2",0.019952621,1};
				volume = "(houses-0.75)*4*night";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_night_2",0.056234132,1,80,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_night_5",0.031622775,1,80,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\insect_2",0.056234132,1,15,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,15,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,30,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class Meadows
			{
				name = "Meadows";
				sound[] = {"A3\sounds_f\ambient\basics\day_insects_winds5",0.022387212,1};
				volume = "(1-forest)*(1-houses)*(1-night)*(1-sea)";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.063095726,1,70,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.063095726,1,100,0.12,15,25,30};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.063095726,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.063095726,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_7",0.056234132,1,70,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_8",0.031622775,1,70,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_4",0.056234132,1,20,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,20,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,20,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class MeadowsNight
			{
				name = "MeadowsNight";
				sound[] = {"A3\sounds_f\ambient\basics\night_insects_birds_winds1",0.015848929,1};
				volume = "(1-forest)*(1-houses)*night*(1-sea)";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_night_2",0.056234132,12,100,0.1,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_night_5",0.031622775,1,60,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\insect_2",0.056234132,1,20,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,20,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,20,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
		};		
	};
	class Stratis : CAWorld
	{
		class AmbientA3 
		{
			maxCost = 0;
		};	
		class CfgEnvSounds;
		class EnvSounds : CfgEnvSounds
		{
			class Rain
			{
				name = "rain";
				sound[] = {"A3\sounds_f\ambient\rain\rain_new_1",0.1,1,200};  // 0.35481337
				soundNight[] = {"A3\sounds_f\ambient\rain\rain_new_2",0.09,1,200};  //0.31622776
				volume="rain";
			};
			class Sea
			{
				name = "Sea";
				sound[] = {"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",0.12589253,1,200};
				soundNight[] = {"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",0.1,1,200};
				volume = "sea*(1-coast)";
			};
			class Coast
			{
				name = "Coast";
				sound[] = {"A3\sounds_f\ambient\waves\beach_sand_small_waves1",0.12589253,1,200};
				soundNight[] = {"A3\sounds_f\ambient\waves\beach_sand_small_waves1",0.07943282,1,200};
				volume = "coast";
			};
			class WindSlow
			{
				name = "WindSlow";
				sound[] = {"A3\sounds_f\ambient\winds\wind-synth-slow",0.028183825,1};
				volume = "((windy factor[0,0.25])*(windy factor[0.5, 0.25]))-(night*0.25)";
			};
			class WindMedium
			{
				name = "WindMedium";
				sound[] = {"A3\sounds_f\ambient\winds\wind-synth-middle",0.035481334,1};
				volume = "((windy factor[0.33,0.5])*(windy factor[0.8, 0.5]))-(night*0.25)";
			};
			class WindFast
			{
				name = "WindFast";
				sound[] = {"A3\sounds_f\ambient\winds\wind-synth-fast",0.044668358,1};
				volume = "(windy factor[0.66,1])-(night*0.25)";
			};
			class Forest
			{
				name = "Forest";
				sound[] = {"A3\sounds_f\ambient\basics\day_insects_winds5",0.025118863,1};
				volume = "forest*trees*(1-night)";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,12,100,0.1,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,12,100,0.1,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,12,100,0.1,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,12,100,0.1,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_5",0.056234132,1,100,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_9",0.056234132,1,80,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_3",0.031622775,1,20,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,40,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,50,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class ForestNight
			{
				name = "ForestNight";
				sound[] = {"A3\sounds_f\ambient\basics\night_insects_birds_winds4",0.015848929,1};
				volume = "forest*trees*night";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_night_6",0.056234132,1,100,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_night_2",0.031622775,1,70,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_3",0.056234132,1,10,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,40,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,30,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class Houses
			{
				name = "Houses";
				sound[] = {"A3\sounds_f\ambient\basics\day_insects_winds4",0.022387212,1};
				volume = "(houses-0.75)*4";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_7",0.056234132,1,100,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_8",0.031622775,1,100,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_4",0.056234132,1,15,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,20,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,25,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class HousesNight
			{
				name = "HousesNight";
				sound[] = {"A3\sounds_f\ambient\basics\night_insects_birds_nowinds2",0.019952621,1};
				volume = "(houses-0.75)*4*night";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_night_2",0.056234132,1,80,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_night_5",0.031622775,1,80,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\insect_2",0.056234132,1,15,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,15,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,30,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class Meadows
			{
				name = "Meadows";
				sound[] = {"A3\sounds_f\ambient\basics\day_insects_winds5",0.022387212,1};
				volume = "(1-forest)*(1-houses)*(1-night)*(1-sea)";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.063095726,1,70,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.063095726,1,100,0.12,15,25,30};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.063095726,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.063095726,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_7",0.056234132,1,70,0.12,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_8",0.031622775,1,70,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\fly_4",0.056234132,1,20,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,20,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,20,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
			class MeadowsNight
			{
				name = "MeadowsNight";
				sound[] = {"A3\sounds_f\ambient\basics\night_insects_birds_winds1",0.015848929,1};
				volume = "(1-forest)*(1-houses)*night*(1-sea)";
				randSamp0[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_01",0.1,1,100,0.12,10,25,40};
				randSamp1[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_02",0.1,1,100,0.12,10,35,60};
				randSamp2[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_03",0.1,1,100,0.12,10,25,40};
				randSamp3[] = {"A3\sounds_f\ambient\single_sfx\meadow_single_04",0.1,1,100,0.12,10,25,40};
				randSamp4[] = {"A3\sounds_f\ambient\single_sfx\bird_night_2",0.056234132,12,100,0.1,10,25,40};
				randSamp5[] = {"A3\sounds_f\ambient\single_sfx\bird_night_5",0.031622775,1,60,0.1,10,25,40};
				randSamp6[] = {"A3\sounds_f\ambient\single_sfx\insect_2",0.056234132,1,20,0.1,10,25,50};
				randSamp7[] = {"A3\sounds_f\ambient\single_sfx\insect_3",0.056234132,1,20,0.1,10,25,40};
				randSamp8[] = {"A3\sounds_f\ambient\single_sfx\insect_1",0.056234132,1,20,0.1,10,25,40};
				random[] = {"randSamp0","randSamp1","randSamp2","randSamp3","randSamp4","randSamp5","randSamp6","randSamp7","randSamp8"};
			};
		};		
	};
	class Bozcaada : Stratis {		
		class AmbientA3 
		{
			maxCost = 0;
		};
	};
	class Tanoa : Stratis {
		class AmbientA3 
		{
			maxCost = 0;
		};
	};
};
