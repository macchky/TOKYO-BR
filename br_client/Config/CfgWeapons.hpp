/**
 * Overwriting several weapons to:
 * a) Remove faulty magazines: 30Rnd_556x45_Stanag_red and 30Rnd_556x45_Stanag_green will be removed because they are duplicates of 30Rnd_556x45_Stanag
 		(Same goes for the duplicate magazines for the Sting/PDW2000)
 * b) Make the CUP weapons (CZ805A, CZ805B, MK16, L85, L86, G36A, G36C, G36K) as well as the FHQ Weapons (M4 variants) to all use the default Stanag mags
 * 		The reason we do this is that, after all, all the different 5.56 magazines in the game are supposed to be Stanag mags.
 * 		In addition, with the current version of CUP weapons pack (which is V 1.2.1), all CUP weapons can use all CUP magazines.\
 *		Another option is to make all guns use all kinds of ammo, but then the question would be why to even have like 6 different kinds of magazines that
 *		are actually the same.
 * c) Make the CZ805B use the 7.62mm silencer instead of the 6.5mm one since it uses 7.62 rounds.
 * d) Magazine fixes for APEX Weapons and Cleanup (26/07/16 Kami)
 */

#define CRATE_LOOT_ARMOR				36
#define HIGH_VALUE_ARMOR				24

#define CRATE_LOOT_BODY_PASSTHROUGH 	.35
#define HIGH_VALUE_BODY_PASSTHROUGH 	.45

class Mode_SemiAuto;
class Mode_FullAuto;

class CfgWeapons
{
	class Pistol;

	class Pistol_Base_F: Pistol
	{
		class WeaponSlotsInfo;
	};

	//--- Rook-40 9mm pistol

	class hgun_Rook40_F : Pistol_Base_F
	{
		magazines[] = {"16Rnd_9x21_Mag","30Rnd_9x21_Mag"};
	};

	//--- P07 9mm pistol

	class hgun_P07_F : Pistol_Base_F
	{
		magazines[] = {"16Rnd_9x21_Mag","30Rnd_9x21_Mag"};
	};

	class Rifle;

	class Rifle_Base_F : Rifle
	{
		class WeaponSlotsInfo {
			class MuzzleSlot;
		};
	};
	// Lee
	class CUP_srifle_LeeEnfield: Rifle_Base_F
	{
		class Single: Mode_SemiAuto
		{
			reloadTime=3;
			backgroundReload=1;
		};
	};
	// CZ
	class CUP_srifle_CZ550_base: Rifle_Base_F
	{
		class Single: Mode_SemiAuto
		{
			reloadTime=2.5;
			backgroundReload=1;
		};
	};

	// Shotguns
	class CUP_sgun_AA12: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_20Rnd_B_AA12_Pellets"
		};
		fireSpreadAngle=5;
	};
	class CUP_sgun_Saiga12K: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_8Rnd_B_Saiga12_74Pellets_M"
		};
		fireSpreadAngle=4;
	};
	class CUP_sgun_M1014: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_8Rnd_B_Beneli_74Pellets"
		};
		fireSpreadAngle=3;
	};
	// XM8
	class CUP_arifle_XM8_Base: Rifle_Base_F
	{
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};

	};
	// TRG
	class Tavor_base_F: Rifle_Base_F
	{
		class Single: Mode_SemiAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"TRG20_silencerShot_SoundSet",
          			"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
		class FullAuto: Mode_FullAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"TRG20_silencerShot_SoundSet",
          			"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class mk20_base_F : Rifle_Base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
		class Single: Mode_SemiAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"Mk20_silencerShot_SoundSet",
          			"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
		class FullAuto: Mode_FullAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"Mk20_silencerShot_SoundSet",
          			"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};		
	};

	class SDAR_base_F : Rifle_Base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	// --- L85

	class CUP_l85a2_base : Rifle_Base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	// --- CUP CZ805 A

	class CUP_arifle_CZ805_Base : Rifle_Base_F {}; // external class reference

	class CUP_arifle_CZ805_A1 : CUP_arifle_CZ805_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_CZ805_A2 : CUP_arifle_CZ805_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_CZ805_GL : CUP_arifle_CZ805_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	// Katiba
	class arifle_Katiba_Base_F: Rifle_Base_F
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[] = {"muzzle_snds_h"};
			};
		};
	};
	// MX
	class arifle_MX_Base_F: Rifle_Base_F
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[] = {"muzzle_snds_h"};
			};
		};
	};
	// --- CUP CZ805 A
	class CUP_arifle_CZ805_B_Base : CUP_arifle_CZ805_Base
	{
		class WeaponSlotsInfo : WeaponSlotsInfo {
			class MuzzleSlot;
		};
	};

	class CUP_arifle_CZ805_B_GL : CUP_arifle_CZ805_B_Base
	{
		class WeaponSlotsInfo : WeaponSlotsInfo
		{
			class MuzzleSlot : MuzzleSlot
			{
				compatibleItems[] = {"muzzle_snds_B"};
			};
		};
	};

	class CUP_arifle_CZ805_B : CUP_arifle_CZ805_B_Base
	{
		class WeaponSlotsInfo : WeaponSlotsInfo
		{
			class MuzzleSlot : MuzzleSlot
			{
				compatibleItems[] = {"muzzle_snds_B"};
			};
		};
	};

	// --- CUP M16

	class CUP_arifle_SCAR_Base : Rifle_Base_F {}; // external class reference

	class CUP_arifle_SCAR_L_Base : CUP_arifle_SCAR_Base  {}; // external class reference

	class CUP_arifle_Mk16_CQC : CUP_arifle_SCAR_L_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_Mk16_STD : CUP_arifle_SCAR_L_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_Mk16_SV : CUP_arifle_SCAR_L_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_Mk17_Base: CUP_arifle_SCAR_Base
	{
		magazines[]=
		{
			"CUP_20Rnd_762x51_B_SCAR",
			"CUP_20Rnd_TE1_Yellow_Tracer_762x51_SCAR"
		};
	};


	// --- CUP G36

	class CUP_arifle_G36_Base : Rifle_Base_F {}; // external class reference

	class CUP_arifle_G36A : CUP_arifle_G36_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_G36C : Rifle_Base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_MG36: CUP_arifle_G36C
	{
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_M16_Base : Rifle_Base_F {};

	class CUP_arifle_M16A4_Base : CUP_arifle_M16_Base {};

	class CUP_arifle_M4_Base: CUP_arifle_M16A4_Base {};

	class CUP_arifle_M4A1_BUIS_Base : CUP_arifle_M4_Base{};

	class CUP_arifle_M4A1_black : CUP_arifle_M4A1_BUIS_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_M4A1_camo : CUP_arifle_M4A1_BUIS_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_M4A1_BUIS_GL : CUP_arifle_M4A1_BUIS_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	class CUP_arifle_M4A1_BUIS_camo_GL : CUP_arifle_M4A1_BUIS_Base {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	//class CUP_arifle_AK_Base : Rifle_Base_F {};
	class CUP_arifle_AK_Base: Rifle_Base_F {
		magazines[] = {
			"CUP_30Rnd_545x39_AK_M",
			// "CUP_30Rnd_TE1_Green_Tracer_545x39_AK_M",
			// "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
			// "CUP_30Rnd_TE1_Yellow_Tracer_545x39_AK_M",
			// "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M"
		};
	};

	class CUP_arifle_AK74 : CUP_arifle_AK_Base {
		magazines[] = {
			"CUP_30Rnd_545x39_AK_M",
			// "CUP_30Rnd_TE1_Green_Tracer_545x39_AK_M",
			// "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
			// "CUP_30Rnd_TE1_Yellow_Tracer_545x39_AK_M",
			// "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M"
		};
	};

	class CUP_arifle_AK74_GL : CUP_arifle_AK_Base {
		magazines[] = {
			"CUP_30Rnd_545x39_AK_M",
			// "CUP_30Rnd_TE1_Green_Tracer_545x39_AK_M",
			// "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
			// "CUP_30Rnd_TE1_Yellow_Tracer_545x39_AK_M",
			// "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M"
		};
	};

	class CUP_arifle_AKS74 : CUP_arifle_AK_Base {
		magazines[] = {
			"CUP_30Rnd_545x39_AK_M",
			// "CUP_30Rnd_TE1_Green_Tracer_545x39_AK_M",
			// "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
			// "CUP_30Rnd_TE1_Yellow_Tracer_545x39_AK_M",
			// "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M"
		};
	};

	class CUP_arifle_AKS74U : CUP_arifle_AK_Base {
		magazines[] = {
			"CUP_30Rnd_545x39_AK_M",
			// "CUP_30Rnd_TE1_Green_Tracer_545x39_AK_M",
			// "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
			// "CUP_30Rnd_TE1_Yellow_Tracer_545x39_AK_M",
			// "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M"
		};
	};	

	//--- SPAR-16 5.56mm

	class arifle_SPAR_01_base_F : Rifle_Base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	//--- SPAR-16 5.56mm (Black)

	class arifle_SPAR_01_blk_F : arifle_SPAR_01_base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	//--- SPAR-16 5.56mm (Khaki)
	class arifle_SPAR_01_khk_F : arifle_SPAR_01_base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	//--- SPAR-16 5.56mm (Sand)
	class arifle_SPAR_01_snd_F : arifle_SPAR_01_base_F {
		magazines[] = {
			"30Rnd_556x45_Stanag",
			// "30Rnd_556x45_Stanag_green",
			// "30Rnd_556x45_Stanag_red",
			"30Rnd_556x45_Stanag_Tracer_Red",
			//"30Rnd_556x45_Stanag_Tracer_Green",
			//"30Rnd_556x45_Stanag_Tracer_Yellow",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
			//"CUP_20Rnd_556x45_Stanag"
		};
	};

	//--- M32 Grenade Launcher			
	class CUP_glaunch_Base : Rifle_Base_F {}; // external class reference

	/**
	 * Launcher should only use revolver rounds.
	 */
	class CUP_glaunch_M32 : CUP_glaunch_Base
	{
		magazines[] = {
			"CUP_6Rnd_HE_M203"
			//"CUP_6Rnd_HE_M203",
		    // "CUP_6Rnd_FlareWhite_M203",
		    // "CUP_6Rnd_FlareGreen_M203",
		    // "CUP_6Rnd_FlareRed_M203"
		};
	};

	//--- CZ Scorpion EVO 3
	class CUP_smg_EVO : Rifle_Base_F
	{
		magazines[] = {"30Rnd_9x21_Mag"};
	};

	//--- PDW2000

	class pdw2000_base_F : Rifle_Base_F
	{
		magazines[] = {"30Rnd_9x21_Mag"};
	};

	//--- Sting

	class SMG_02_base_F : Rifle_Base_F
	{
		magazines[] = {"30Rnd_9x21_Mag"};
	};
	
	//--- Protector

	class SMG_05_base_F : Rifle_Base_F {};

	class SMG_05_F : SMG_05_base_F 
	{
		magazines[] = {"30Rnd_9x21_Mag"};
	};

	// RPG
	class Launcher_Base_F;
	class CUP_launch_RPG7V: Launcher_Base_F
	{
		magazines[]=
			{
				"CUP_PG7VR_M"
			};
	};

	class ItemCore;
	class InventoryItem_Base_F;
	class VestItem;
	class Vest_Camo_Base : ItemCore
	{
		class ItemInfo;
	};
	class Vest_NoCamo_Base : ItemCore
	{
		class ItemInfo;
	};
	class CUP_acc_sffh: ItemCore	// L85 flash suppressor
	{
		displayName="L85 Surefire Flash Hider";
	};
	///////////////////////////////////// TIER 1 VESTS
	class V_TacVest_khk : Vest_Camo_Base 	//Tactical Vest (Khaki)
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName = "HitChest";
					armor = 8;
					passThrough = 0.5;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 8;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 8;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 8;
					passThrough = 0.5;
				};				
			};
		};
	};
	class V_TacVest_camo : Vest_Camo_Base 	//Tactical Vest (Camo)
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName = "HitChest";
					armor = 8;
					passThrough = 0.5;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 8;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 8;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 8;
					passThrough = 0.5;
				};				
			};
		};
	};
	class V_RebreatherB	: Vest_Camo_Base		//Rebreather [NATO]
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 8;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 8;
					passThrough = 0.5;
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 8;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
				};
			};
		};
	};
	class V_TacVestCamo_khk: Vest_Camo_Base 	//Camouflaged Vest
	{
		class ItemInfo: VestItem
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName = "HitChest";
					armor = 8;
					passThrough = 0.5;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 8;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 8;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION			
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 12;
					passThrough = 0.4;
				};				
			};
		};
	};
	class V_TacVestIR_blk: Vest_NoCamo_Base		// Raven Vest
	{
		class ItemInfo: VestItem
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName = "HitChest";
					armor = 8;
					passThrough = 0.5;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 8;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 8;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 4;
					passThrough = 0.4;
				};
			};
		};
	};	
	// ///////////////////////////////////// TIER 2 VESTS
	class V_TacVest_blk_POLICE : Vest_Camo_Base 	//Tactical Vest (Police)
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName = "HitChest";
					armor = 16;
					passThrough = 0.5;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 16;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 16;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 16;
					passThrough = 0.4;
				};				
			};
		};
	};
	class V_Press_F: Vest_Camo_Base 	// Press Vest
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName = "HitChest";
					armor = 16;
					passThrough = 0.5;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = 16;
					passThrough = 0.5;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = 16;
					passThrough = 0.5;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = 0.5;
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = 16;
					passThrough = 0.4;
				};				
			};
		};
	};
	// ///////////////////////////////////// TIER 3 VESTS
	class V_PlateCarrierIA1_dgtl : Vest_NoCamo_Base 	//GA Carrier Lite (Digi)
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 1;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 1;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = HIGH_VALUE_BODY_PASSTHROUGH;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION					
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};
	class V_PlateCarrier1_rgr  : Vest_NoCamo_Base		// Inheritance Only
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 1;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 1;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = HIGH_VALUE_BODY_PASSTHROUGH;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION					
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};
	class V_PlateCarrierL_CTRG: V_PlateCarrier1_rgr		//CTRG Plate Carrier Rik Mk.2 [Light]
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 1;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 1;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = HIGH_VALUE_BODY_PASSTHROUGH;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION					
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};
	class V_PlateCarrier1_blk: Vest_Camo_Base 	//Carrier Lite (Black)	
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 2;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 2;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = HIGH_VALUE_BODY_PASSTHROUGH;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION					
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};	
	class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr  		// Inheritance Only	
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 1;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 1;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = HIGH_VALUE_BODY_PASSTHROUGH;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION					
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = HIGH_VALUE_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};
	class V_PlateCarrierH_CTRG:  V_PlateCarrier2_rgr  //CTRG Plate Carrier Rik Mk.2 [Heavy]     CRATE LOOT  Still in game?  If not in game, remove.
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 4;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 4;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = CRATE_LOOT_BODY_PASSTHROUGH;	// .5
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION				
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};
	// ///////////////////////////////////// TIER 4 VESTS
	// ///////////////////////////////////// TIER 5 VESTS
	class V_PlateCarrierSpec_rgr: Vest_NoCamo_Base 	//Carrier Special Rig (Green)    CRATE LOOT
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName = "HitNeck";
					armor = 16;
					passThrough = 0.5;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 16;
					passThrough = 0.5;
				};
				class Chest
				{
					hitpointName = "HitChest";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};
				class Diaphragm
				{
					hitpointName = "HitDiaphragm";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};
				class Abdomen
				{
					hitpointName = "HitAbdomen";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};
				class Body
				{
					hitpointName = "HitBody";
					passThrough = CRATE_LOOT_BODY_PASSTHROUGH;
					//armor = 12;	// DON'T DO BODY ARMOR.  IT MESSES UP ARMA DAMAGE PROPAGATION
				};
				class Pelvis
				{
					hitpointName = "HitPelvis";
					armor = CRATE_LOOT_ARMOR;
					passThrough = 0.1;
				};				
			};
		};
	};
	// Helmets
	class H_HelmetB : ItemCore
	{
		class ItemInfo;
	};
	// // Tier 2 Helmets
	// class H_PilotHelmetFighter_B: H_HelmetB
	// {
	// 	class ItemInfo: ItemInfo
	// 	{
	// 		class HitpointsProtectionInfo
	// 		{
	// 			class Head
	// 			{
	// 				hitpointName = "HitHead";
	// 				armor = 5;
	// 				passThrough = 0.5;
	// 			};
	// 			class Neck 
	// 			{
	// 				hitpointName = "HitNeck";
	// 				armor = 5;
	// 				passThrough = 0.5;
	// 			};
	// 			class Face
	// 			{
	// 				hitpointName = "HitFace";
	// 				armor = 1;
	// 				passThrough = 0.5;
	// 			};

	// 		};
	// 	};
	// };
	// // Tier 3 Helmets
	// class H_HelmetCrew_B: H_HelmetB
	// {
	// 	class ItemInfo: ItemInfo
	// 	{
	// 		class HitpointsProtectionInfo
	// 		{
	// 			class Head
	// 			{
	// 				hitpointName = "HitHead";
	// 				armor = 6;
	// 				passThrough = 0.5;
	// 			};
	// 			class Neck 
	// 			{
	// 				hitpointName = "HitNeck";
	// 				armor = 6;
	// 				passThrough = 0.5;
	// 			};
	// 		};
	// 	};
	// };
	// // Tier 4 Helmets
	// class H_HelmetB_plain_mcamo : H_HelmetB
	// {
	// 	class ItemInfo;
	// };
	// class H_HelmetSpecB: H_HelmetB_plain_mcamo 	// Enhanced
	// {
	// 	class ItemInfo: ItemInfo
	// 	{
	// 		class HitpointsProtectionInfo
	// 		{
	// 			class Head
	// 			{
	// 				hitpointName = "HitHead";
	// 				armor = 7;
	// 				passThrough = 0.5;
	// 			};
	// 			class Neck 
	// 			{
	// 				hitpointName = "HitNeck";
	// 				armor = 7;
	// 				passThrough = 0.5;
	// 			};
	// 		};
	// 	};
	// };
	// Tier 5 Helmets
	// class H_HelmetO_ocamo : H_HelmetB
	// {
	// 	class ItemInfo;
	// };
	// class H_HelmetLeaderO_ocamo: H_HelmetO_ocamo
	// {
	// 	class ItemInfo: ItemInfo
	// 	{
	// 		class HitpointsProtectionInfo
	// 		{
	// 			class Head
	// 			{
	// 				hitpointName = "HitHead";
	// 				armor = 8;
	// 				passThrough = 0.5;
	// 			};
	// 			class Face
	// 			{
	// 				hitpointName = "HitFace";
	// 				armor = 1;
	// 				passThrough = 0.5;
	// 			};
	// 			class Neck 
	// 			{
	// 				hitpointName = "HitNeck";
	// 				armor = 6;
	// 				passThrough = 0.5;
	// 			};
	// 		};
	// 	};
	// };
};