class CfgNotifications
{
	class TeamInviteReceived
	{
		title = "Team Invite Received!";
		iconPicture = "\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description = "%2";
		priority = 5;
		sound = "taskAssigned";
		soundClose="";
	};
	class TeamUpdated
	{
		title = "Your Team is Updated!";
		iconPicture = "\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description = "%2";
		priority = 5;
		sound = "";
		soundClose="";
	};
	class TeamMapHelp
	{
		title = "Team Map Help";
		iconPicture = "\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description = "%2";
		priority = 5;
		sound = "";
		soundClose="";
	};
	class GroupCreated
	{
		title = "Group Chat is Ready!";
		iconPicture = "\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description = "%2";
		priority = 5;
		sound = "taskAssigned";
		soundClose="";
	};
	// title = "";
	// iconPicture = "";
	// iconText = "";
	// description = "";
	// color[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_A',0.8])"};
	// colorIconText[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_A',0.8])"};
	// colorIconPicture[] = {"(profilenamespace getvariable ['IGUI_TEXT_RGB_R',0])","(profilenamespace getvariable ['IGUI_TEXT_RGB_G',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_B',1])","(profilenamespace getvariable ['IGUI_TEXT_RGB_A',0.8])"};
	// duration = 5;
	// priority = 0;
	// difficulty[] = {};
	// sound = "defaultNotification";
	// soundClose = "defaultNotificationClose";
	// soundRadio = "";
};