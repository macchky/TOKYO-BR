class CfgPatches
{
	class br_client
	{
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"a3_ui_f",
			"A3_Functions_F",
			"a3_data_f",
			"A3_Map_Data",
			"A3_Characters_F",
			"A3_Data_F",
			"A3_Weapons_F",
			"A3_Dubbing_Radio_F",
			"CUP_Weapons_L85",
			"CUP_Weapons_WeaponsCore",
			"CUP_Weapons_Saiga12K",
			"CUP_Weapons_AA12",
			"CUP_Weapons_SCAR",
			"CUP_Weapons_M1014",
			"CUP_Weapons_G36",
			"CUP_Weapons_LeeEnfield",
			"CUP_Weapons_Huntingrifle",
			"CUP_Weapons_Ammunition",
			"CUP_Weapons_XM8",
			"CUP_Weapons_RPG7",
			"cba_jr",
			"cba_jr_prep",
			"cba_main",
			"beetle_custom",
			"xmas_weapon",
			"jean_sleigh",
			"xmas_character"
		};
		units[]=
		{
			"B_BattleRoyalePlayer_F"
		};
		weapons[]=
		{
			"mk20_base_F",
			"SDAR_base_F",
			"Tavor_base_F",
			"CUP_arifle_G36A",
			"CUP_arifle_G36C",
			"CUP_arifle_CZ805_Base",
			"CUP_arifle_CZ805_A1",
			"CUP_arifle_CZ805_A2",
			"CUP_arifle_CZ805_GL",
			"CUP_arifle_Mk16_CQC",
			"CUP_arifle_Mk16_STD",
			"CUP_arifle_CZ805_B",
			"CUP_arifle_CZ805_B_GL",
			"CUP_glaunch_M32",
			"V_TacVestIR_blk",
			"V_Press_F",
			"V_TacVestCamo_khk",
			"V_RebreatherB",
			"V_PlateCarrierIA1_dgtl",
			"V_PlateCarrier1_rgr",
			"V_PlateCarrierL_CTRG",
			"V_PlateCarrier2_rgr",
			"V_PlateCarrierH_CTRG",
			"V_PlateCarrierSpec_rgr",
			"V_PlateCarrier1_blk",
			"V_TacVest_blk_POLICE",
			"V_TacVest_camo",
			"V_TacVest_khk"
		};
		magazines[]=
		{
			"CUP_20Rnd_B_AA12_Pellets",
			"ItemRedgull",
			"ItemPainkiller",
			"ItemBRToolKit"
		};
		ammo[]=
		{
			"ATMine_Range_Ammo",
			"CUP_R_PG7VR_AT",
			"CUP_B_303_Ball",
			"B_65x39_Caseless",
			"CUP_B_762x39_Ball",
			"CUP_B_762x39_Ball_Tracer_Green",
			"B_762x51_Ball",
			"CUP_B_762x54_Ball_White_Tracer",
			"CUP_B_93x64_Ball",
			"CUP_B_12Gauge_Pellets",
			"GrenadeHand",
			"mini_Grenade",
			"G_40mm_HE",
			"G_40mm_HEDP",
			"Bo_Mk82",
			"Bo_GBU12_LGB",
			"HelicopterExploBig",
			"xmas_exposive_present_ammo"
		};
	};
};
class CfgAmmo
{
	class MineBase;
	class ATMine_Range_Ammo: MineBase
	{
		indirectHitRange=8;
	};
	class RocketBase;
	class CUP_R_PG7VR_AT: RocketBase
	{
		hit=140;
		indirectHit=10;
		indirectHitRange=8;
	};
	class BulletBase;
	class CUP_B_303_Ball: BulletBase
	{
		hit=12;
		airFriction=-8.2199003e-005;
	};
	class B_65x39_Caseless: BulletBase
	{
		hit=10;
	};
	class CUP_B_762x39_Ball: BulletBase
	{
		hit=14;
	};
	class CUP_B_762x39_Ball_Tracer_Green: CUP_B_762x39_Ball
	{
		hit=14;
	};
	class B_762x51_Ball: BulletBase
	{
		hit=14;
	};
	class CUP_B_762x54_Ball_White_Tracer: BulletBase
	{
		hit=14;
	};
	class B_93x64_Ball;
	class CUP_B_93x64_Ball: B_93x64_Ball
	{
		hit=10;
		airFriction=-0.00060999999;
	};
	class CUP_B_12Gauge_Pellets: BulletBase
	{
		simulation="shotSpread";
		hit=3;
		indirectHit=0;
		indirectHitRange=0;
		cartridge="FxCartridge_slug";
		cost=2;
		typicalSpeed=400;
		visibleFire=18;
		audibleFire=18;
		airFriction=-0.0049999999;
		caliber=0.1;
		AGM_BulletMass=1;
	};
	class Grenade;
	class GrenadeBase;
	class GrenadeHand: Grenade
	{
		hit=12;
		indirectHit=8;
		indirectHitRange=8;
	};
	class mini_Grenade: GrenadeHand
	{
		hit=8;
		indirectHit=6;
		indirectHitRange=7;
	};
	class G_40mm_HE: GrenadeBase
	{
		hit=140;
		indirectHit=10;
		indirectHitRange=8;
	};
	class G_40mm_HEDP: G_40mm_HE
	{
		hit=140;
		indirectHit=10;
		indirectHitRange=8;
	};
	class BombCore;
	class Bo_Mk82: BombCore
	{
		soundHit1[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_1",
			1.25,
			1,
			700
		};
		soundHit2[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_2",
			1.25,
			1,
			700
		};
		soundHit3[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_3",
			1.25,
			1,
			700
		};
		soundHit4[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_shell_1",
			1.25,
			1,
			700
		};
		soundHit5[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_shell_2",
			1.25,
			1,
			700
		};
	};
	class LaserBombCore;
	class Bo_GBU12_LGB: LaserBombCore
	{
		soundHit1[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_1",
			2.25,
			1,
			700
		};
		soundHit2[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_2",
			2.25,
			1,
			700
		};
		soundHit3[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_big_3",
			2.25,
			1,
			700
		};
		soundHit4[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_shell_1",
			2.25,
			1,
			700
		};
		soundHit5[]=
		{
			"A3\Sounds_F\weapons\Explosion\expl_shell_2",
			2.25,
			1,
			700
		};
	};
	class HelicopterExploSmall;
	class HelicopterExploBig: HelicopterExploSmall
	{
		hit=10000;
		indirectHit=200;
		indirectHitRange=7.5;
		explosionEffects="HelicopterExplosionEffects2";
		explosionSoundEffect="DefaultExplosion";
		soundHit1[]=
		{
			"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_1",
			3.1622777,
			1,
			750
		};
		soundHit2[]=
		{
			"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_2",
			3.1622777,
			1,
			750
		};
		soundHit3[]=
		{
			"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_3",
			3.1622777,
			1,
			750
		};
		soundHit4[]=
		{
			"A3\Sounds_F\vehicles\crashes\helis\Heli_crash_ground_ext_4",
			3.1622777,
			1,
			750
		};
		multiSoundHit[]=
		{
			"soundHit1",
			0.25,
			"soundHit2",
			0.25,
			"soundHit3",
			0.25,
			"soundHit4",
			0.25
		};
	};
	class xmas_exposive_present_ammo: Grenade
	{
		hit=7;
		indirectHit=5;
		indirectHitRange=6;
	};
};
class CfgCloudlets
{
	class Default;
	class SmokeShellWhite: Default
	{
		animationSpeedCoef=1;
		colorCoef[]=
		{
			"colorR",
			"colorG",
			"colorB",
			"colorA"
		};
		sizeCoef=2;
		position[]={0,0,0};
		interval=0.029999999;
		circleRadius=0;
		circleVelocity[]={0,0,0};
		particleShape="\A3\data_f\ParticleEffects\Universal\Universal";
		particleFSNtieth=16;
		particleFSIndex=7;
		particleFSFrameCount=48;
		particleFSLoop=1;
		angleVar=0.1;
		animationName="";
		particleType="Billboard";
		timerPeriod=1;
		lifeTime=25;
		moveVelocity[]={0.2,0.1,0.1};
		rotationVelocity=1;
		weight=1.2776999;
		volume=1;
		rubbing=0.02;
		size[]={0.2,4,12};
		color[]=
		{
			{0.60000002,0.60000002,0.60000002,0.2},
			{0.60000002,0.60000002,0.60000002,0.050000001},
			{0.60000002,0.60000002,0.60000002,0}
		};
		animationSpeed[]={1.5,0.5};
		randomDirectionPeriod=1;
		randomDirectionIntensity=0.039999999;
		onTimerScript="";
		beforeDestroyScript="";
		destroyOnWaterSurface=1;
		destroyOnWaterSurfaceOffset=-0.60000002;
		lifeTimeVar=2;
		positionVar[]={0,0,0};
		MoveVelocityVar[]={0.25,0.25,0.25};
		rotationVelocityVar=20;
		sizeVar=0.5;
		colorVar[]={0,0,0,0.34999999};
		randomDirectionPeriodVar=0;
		randomDirectionIntensityVar=0;
	};
};
class CfgCommands
{
	allowedHTMLLoadURIs[]+=
	{
		"http://battleroyalegames.com/*",
		"http://www.battleroyalegames.com/*",
		"https://www.bistudio.com*",
		"br_client\*"
	};
};
class CfgFunctions
{
	class br_client
	{
		class Initialize
		{
			file="br_client\init";
			class preInit
			{
				preInit=1;
			};
			class postInit
			{
				postInit=1;
			};
		};
	};
};
class CfgMagazines
{
	class CA_Magazine;
	class CUP_20Rnd_B_AA12_Pellets: CA_Magazine
	{
		author="$STR_CUP_AUTHOR_STRING";
		scope=2;
		displayName="$STR_CUP_dn_aa12_20rnd_pellets_M";
		ammo="CUP_B_12Gauge_Pellets";
		count=6;
		initSpeed=396;
		picture="\CUP\Weapons\CUP_Weapons_Ammunition\data\ui\m_aa12_ca.paa";
		descriptionShort="$STR_CUP_dss_aa12_20rnd_pellets_M";
		displayNameShort="Pellets";
		mass=26;
	};
	class CUP_1Rnd_HEDP_M203;
	class CUP_6Rnd_HE_M203: CUP_1Rnd_HEDP_M203
	{
		mass=80;
	};
	class HandGrenade;
	class xmas_explosive_present: HandGrenade
	{
		mass=15;
		displayName="Explosive Gift";
		displayNameShort="Explosive Gift";
		descriptionShort="Don't shake.  Throw it instead.";
	};
	class ItemRedgull: CA_Magazine
	{
		scope=2;
		count=1;
		type=256;
		displayName="Red Gull";
		descriptionUse="A can of Red Gull.";
		model="\A3\Structures_F\Items\Food\Can_V3_F.p3d";
		picture="\br_client\images\items\redgull.paa";
		descriptionShort="A can of Red Gull.";
		mass=10;
	};
	class ItemPainkiller: CA_Magazine
	{
		scope=2;
		count=1;
		type=256;
		displayName="Painkillers";
		descriptionUse="A box of painkillers.";
		model="\A3\Structures_F_EPA\Items\Medical\PainKillers_F.p3d";
		picture="\br_client\images\items\paink.paa";
		descriptionShort="A box of painkillers.";
		mass=6;
	};
	class ItemBRToolKit: CA_Magazine
	{
		scope=2;
		displayName="Vehicle Repair Kit";
		descriptionShort="Repairs vehicles, but can't bring your fiery wreck back to life.<br/>Satisfaction not guaranteed.";
		picture="\A3\Weapons_F\Items\data\UI\gear_Toolkit_CA.paa";
		model="\A3\Weapons_F\Items\Toolkit";
		mass=40;
	};
};
class CfgModels
{
	class Default
	{
		sectionsInherit="";
		sections[]={};
		skeletonName="";
	};
	class L85A2: Default
	{
		sections[]=
		{
			"zasleh"
		};
		skeletonName="L85A2_Skeleton";
		class Animations
		{
			class trigger_move
			{
				type="rotation";
				source="reload";
				selection="trigger";
				memory=1;
				sourceAddress="clamp";
				axis="trigger_Axis";
				minValue=0;
				maxValue=0.30000001;
				minPhase=0;
				maxPhase=0.30000001;
				angle0=0;
				angle1=-0.21238901;
			};
			class muzzle_hide
			{
				type="hide";
				source="hasSuppressor";
				selection="muzzle";
				minValue=0;
				maxValue=1;
				hideValue=1;
				animPeriod=0;
				initPhase=0;
			};
			class bolt_move
			{
				type="translation";
				source="reload";
				selection="bolt";
				memory=1;
				sourceAddress="clamp";
				axis="bolt_Axis";
				minValue=0;
				maxValue=1;
				minPhase=0;
				maxPhase=1;
				offset0=0;
				offset1=0.60000002;
			};
			class bolt_empty
			{
				type="translation";
				source="isEmptyNoReload";
				selection="bolt";
				axis="bolt_axis";
				sourceAddress="clamp";
				minValue=0;
				maxValue=1;
				offset0=0;
				offset1=0.60000002;
				animPeriod=0;
				initPhase=0;
				memory=1;
			};
			class bolt_move_reload1
			{
				type="translation";
				source="reloadmagazine";
				selection="bolt";
				memory=1;
				sourceAddress="clamp";
				axis="bolt_Axis";
				minValue=0.64666998;
				maxValue=0.68000001;
				offset0=0;
				offset1=1;
			};
			class bolt_move_reload2
			{
				type="translation";
				source="reloadmagazine";
				selection="bolt";
				memory=1;
				sourceAddress="clamp";
				axis="bolt_Axis";
				minValue=0.74666673;
				maxValue=0.76666671;
				offset0=0;
				offset1=-1;
			};
			class magazine_reload_hide
			{
				type="hide";
				source="reloadMagazine";
				selection="magazine";
				sourceAddress="clamp";
				minValue=0;
				maxValue=1;
				hideValue=0.12666667;
				unHideValue=0.35333332;
				animPeriod=0;
				initPhase=0;
			};
			class magazine_reload_move_1
			{
				type="translation";
				source="reloadMagazine";
				selection="magazine";
				axis="magazine_axis";
				sourceAddress="clamp";
				minValue=0.1;
				maxValue=0.12;
				offset0=0;
				offset1=1;
				animPeriod=0;
				initPhase=0;
				memory=1;
			};
			class magazine_reload_move_2
			{
				type="translation";
				source="reloadMagazine";
				selection="magazine";
				axis="magazine_axis";
				sourceAddress="clamp";
				minValue=0.36000001;
				maxValue=0.40000001;
				offset0=0;
				offset1=-0.80000001;
				animPeriod=0;
				initPhase=0;
				memory=1;
			};
			class magazine_reload_move_2a
			{
				type="translation";
				source="reloadMagazine";
				selection="magazine";
				axis="magazine_axis";
				sourceAddress="clamp";
				minValue=0.44666666;
				maxValue=0.46000001;
				offset0=0;
				offset1=-0.2;
				animPeriod=0;
				initPhase=0;
				memory=1;
			};
			class no_magazine
			{
				type="hide";
				source="hasMagazine";
				selection="magazine";
				sourceAddress="clamp";
				minValue=0;
				maxValue=1;
				hideValue=0.5;
				unHideValue=-1;
				animPeriod=0;
				initPhase=0;
			};
			class safety_mode_rot
			{
				type="rotation";
				source="weaponMode";
				selection="fire_switch";
				axis="fireswitch_axis";
				sourceAddress="clamp";
				minValue=0;
				maxValue=0.25;
				angle0=0;
				angle1=-0.5;
				animPeriod=0;
				initPhase=0;
				memory=1;
			};
			class ironsight_hide
			{
				type="hide";
				source="hasOptics";
				selection="iron_sight";
				minValue=0;
				maxValue=1;
				hideValue=1;
				animPeriod=0;
				initPhase=0;
			};
			class ris_unhide
			{
				type="hide";
				source="hasOptics";
				selection="ris";
				minValue=0;
				maxValue=1;
				hideValue=0;
				unhideValue=1;
				animPeriod=0;
				initPhase=0;
			};
			class MuzzleFlashROT
			{
				type="rotationX";
				source="ammoRandom";
				sourceAddress="loop";
				selection="zasleh";
				axis="zasleh_axis";
				centerFirstVertex=1;
				minValue=0;
				maxValue=4;
				angle0="rad 0";
				angle1="rad 360";
			};
		};
	};
	class L85A2_RIS: L85A2
	{
	};
	class L85A2_RIS_NG: L85A2
	{
	};
	class L85A2_UGL: L85A2
	{
		sectionsInherit="L85A2";
		skeletonName="L85A2_UGL_Skeleton";
		class Animations: Animations
		{
			class OP_ROT
			{
				type="rotation";
				source="zeroing2";
				selection="ladder";
				axis="op_axis";
				sourceAddress="loop";
				minValue=0;
				maxValue=1;
				angle0=-0.034906585;
				angle1=0.62;
				animPeriod=1.4012985e-045;
				initPhase=0;
				memory=1;
			};
			class sight_gl_flip
			{
				type="rotation";
				source="weaponMuzzle";
				sourceAddress="loop";
				selection="ladder";
				axis="OP_axis";
				minValue=1;
				maxValue=2;
				angle0="rad 0";
				angle1="rad 81";
			};
		};
	};
	class L86A2: L85A2
	{
		sectionsInherit="L85A2";
		skeletonName="L86A2_Skeleton";
		class Animations: Animations
		{
			class bipod_legs
			{
				type="rotation";
				source="bipod";
				sourceAddress="clamp";
				selection="bipod_legs";
				axis="bipod_legs_axis_1";
				angle0="(rad 0)";
				angle1="(rad 90)";
				memory=1;
			};
			class bipod_leg_l: bipod_legs
			{
				type="rotation";
				axis="bipod_legs_axis_2";
				selection="bipod_leg_l";
				angle0="(rad 0)";
				angle1="(rad 33)";
			};
			class bipod_leg_r: bipod_leg_l
			{
				selection="bipod_leg_r";
				angle1="(rad -33)";
			};
		};
	};
};
class CfgNotifications
{
	class TeamInviteReceived
	{
		title="Team Invite Received!";
		iconPicture="\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description="%2";
		priority=5;
		sound="taskAssigned";
		soundClose="";
	};
	class TeamUpdated
	{
		title="Your Team is Updated!";
		iconPicture="\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description="%2";
		priority=5;
		sound="";
		soundClose="";
	};
	class TeamMapHelp
	{
		title="Team Map Help";
		iconPicture="\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description="%2";
		priority=5;
		sound="";
		soundClose="";
	};
	class GroupCreated
	{
		title="Group Chat is Ready!";
		iconPicture="\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa";
		description="%2";
		priority=5;
		sound="taskAssigned";
		soundClose="";
	};
};
class CfgRemoteExec
{
	class Functions
	{
		mode=1;
		jip=0;
		class bis_fnc_reviveinitaddplayer
		{
			allowedTargets=2;
		};
		class BR_Server_Network_ReceiveMessage
		{
			allowedTargets=2;
		};
		class BIS_fnc_execVM
		{
			allowedTargets=2;
		};
		class BIS_fnc_effectKilledAirDestruction
		{
			allowedTargets=2;
		};
		class BIS_fnc_effectKilledSecondaries
		{
			allowedTargets=2;
		};
		class BIS_fnc_objectVar
		{
			allowedTargets=2;
		};
	};
	class Commands
	{
		mode=1;
		class call
		{
			allowedTargets=0;
			jip=0;
		};
	};
};
class RscText;
class RscActiveText;
class RscStructuredText;
class Attributes;
class RscPicture;
class RscPictureKeepAspect;
class RscVignette;
class RscProgress;
class ProgressMap;
class RscControlsGroupNoScrollbars;
class RscStandardDisplay;
class Pause1;
class RscTitle;
class RscButtonMenu;
class RscDebugConsole;
class RscTrafficLight;
class RscFeedback;
class RscHTML;
class RscFrame;
class CA_Title;
class ShortcutPos;
class RscMessageBox;
class RscButtonMenuCancel;
class RscEdit;
class RscCombo;
class RscTree;
class RscToolbox;
class RscButton;
class RscMapControl;
class RscListBox;
class RscListNBox
{
	class ScrollBar
	{
	};
};
class RscCheckbox;
class RscInGameUI
{
	class RscStaminaBar
	{
		scriptPath="IGUI";
		onLoad="";
		onUnload="";
		controls[]={};
		delete StaminaBar;
	};
	class RscHint
	{
		idd=301;
		movingEnable=0;
		controls[]=
		{
			"Background",
			"Hint"
		};
		onLoad="uiNamespace setVariable ['UI_Hint', _this select 0];";
		class Background: RscText
		{
			idc=101;
			style=128;
			x="((safezoneX + safezoneW) -   (12 *    (   ((safezoneW / safezoneH) min 1.2) / 40)) - 1 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			y="(safezoneY + 6 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
			w="(12 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			h="(8 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))";
			text="";
			colorBackground[]={0,0,0,0.69999999};
			shadow=1;
		};
		class Hint: RscStructuredText
		{
			idc=102;
			x="((safezoneX + safezoneW) -   (12 *    (   ((safezoneW / safezoneH) min 1.2) / 40)) - 1 *    (   ((safezoneW / safezoneH) min 1.2) / 40)) + 0.4*     (   ((safezoneW / safezoneH) min 1.2) / 40)";
			y="(safezoneY + 6 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)) + 0.3*     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w="(12 *    (   ((safezoneW / safezoneH) min 1.2) / 40)) - 0.8*     (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="(8 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)) - 0.8*     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			style=16;
			shadow=1;
			size="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
			class Attributes: Attributes
			{
			};
		};
	};
};
class RscDisplayMPInterrupt: RscStandardDisplay
{
	scriptName="RscDisplayMPInterrupt";
	scriptPath="GUI";
	onLoad="_this call (uiNamespace getVariable 'Client_OnMPPause_NewsFeed'); [""onLoad"",_this,""RscDisplayMPInterrupt"",'GUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload="[""onUnload"",_this,""RscDisplayMPInterrupt"",'GUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay')";
	movingEnable=0;
	enableSimulation=1;
	class controlsBackground
	{
		class Vignette: RscVignette
		{
			idc=114998;
		};
		class TileGroup: RscControlsGroupNoScrollbars
		{
			idc=115099;
			x="safezoneX";
			y="safezoneY";
			w="safezoneW";
			h="safezoneH";
			disableCustomColors=1;
			class Controls
			{
				class TileFrame: RscFrame
				{
					idc=114999;
					x=0;
					y=0;
					w="safezoneW";
					h="safezoneH";
					colortext[]={0,0,0,1};
				};
				class Tile_0_0: RscText
				{
					idc=115000;
					x="(0 * 1/6) * safezoneW";
					y="(0 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_0_1: RscText
				{
					idc=115001;
					x="(0 * 1/6) * safezoneW";
					y="(1 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_0_2: RscText
				{
					idc=115002;
					x="(0 * 1/6) * safezoneW";
					y="(2 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_0_3: RscText
				{
					idc=115003;
					x="(0 * 1/6) * safezoneW";
					y="(3 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_0_4: RscText
				{
					idc=115004;
					x="(0 * 1/6) * safezoneW";
					y="(4 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_0_5: RscText
				{
					idc=115005;
					x="(0 * 1/6) * safezoneW";
					y="(5 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_1_0: RscText
				{
					idc=115010;
					x="(1 * 1/6) * safezoneW";
					y="(0 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_1_1: RscText
				{
					idc=115011;
					x="(1 * 1/6) * safezoneW";
					y="(1 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_1_2: RscText
				{
					idc=115012;
					x="(1 * 1/6) * safezoneW";
					y="(2 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_1_3: RscText
				{
					idc=115013;
					x="(1 * 1/6) * safezoneW";
					y="(3 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_1_4: RscText
				{
					idc=115014;
					x="(1 * 1/6) * safezoneW";
					y="(4 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_1_5: RscText
				{
					idc=115015;
					x="(1 * 1/6) * safezoneW";
					y="(5 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_2_0: RscText
				{
					idc=115020;
					x="(2 * 1/6) * safezoneW";
					y="(0 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_2_1: RscText
				{
					idc=115021;
					x="(2 * 1/6) * safezoneW";
					y="(1 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_2_2: RscText
				{
					idc=115022;
					x="(2 * 1/6) * safezoneW";
					y="(2 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_2_3: RscText
				{
					idc=115023;
					x="(2 * 1/6) * safezoneW";
					y="(3 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_2_4: RscText
				{
					idc=115024;
					x="(2 * 1/6) * safezoneW";
					y="(4 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_2_5: RscText
				{
					idc=115025;
					x="(2 * 1/6) * safezoneW";
					y="(5 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_3_0: RscText
				{
					idc=115030;
					x="(3 * 1/6) * safezoneW";
					y="(0 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_3_1: RscText
				{
					idc=115031;
					x="(3 * 1/6) * safezoneW";
					y="(1 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_3_2: RscText
				{
					idc=115032;
					x="(3 * 1/6) * safezoneW";
					y="(2 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_3_3: RscText
				{
					idc=115033;
					x="(3 * 1/6) * safezoneW";
					y="(3 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_3_4: RscText
				{
					idc=115034;
					x="(3 * 1/6) * safezoneW";
					y="(4 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_3_5: RscText
				{
					idc=115035;
					x="(3 * 1/6) * safezoneW";
					y="(5 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_4_0: RscText
				{
					idc=115040;
					x="(4 * 1/6) * safezoneW";
					y="(0 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_4_1: RscText
				{
					idc=115041;
					x="(4 * 1/6) * safezoneW";
					y="(1 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_4_2: RscText
				{
					idc=115042;
					x="(4 * 1/6) * safezoneW";
					y="(2 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_4_3: RscText
				{
					idc=115043;
					x="(4 * 1/6) * safezoneW";
					y="(3 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_4_4: RscText
				{
					idc=115044;
					x="(4 * 1/6) * safezoneW";
					y="(4 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_4_5: RscText
				{
					idc=115045;
					x="(4 * 1/6) * safezoneW";
					y="(5 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_5_0: RscText
				{
					idc=115050;
					x="(5 * 1/6) * safezoneW";
					y="(0 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_5_1: RscText
				{
					idc=115051;
					x="(5 * 1/6) * safezoneW";
					y="(1 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_5_2: RscText
				{
					idc=115052;
					x="(5 * 1/6) * safezoneW";
					y="(2 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_5_3: RscText
				{
					idc=115053;
					x="(5 * 1/6) * safezoneW";
					y="(3 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_5_4: RscText
				{
					idc=115054;
					x="(5 * 1/6) * safezoneW";
					y="(4 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
				class Tile_5_5: RscText
				{
					idc=115055;
					x="(5 * 1/6) * safezoneW";
					y="(5 * 1/6) * safezoneH";
					w="1/6 * safezoneW";
					h="1/6 * safezoneH";
					colorBackground[]={0,0,0,0.1};
				};
			};
		};
		class TitleBackground: RscText
		{
			idc=1050;
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="14.2 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[]=
			{
				"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"
			};
		};
		class MissionNameBackground: RscText
		{
			idc=-1;
			x="SafezoneX + (1 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			y="23 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="SafezoneW - (2 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[]={0,0,0,0.69999999};
		};
		class Pause1: RscText
		{
			idc=1000;
			x="safezoneX + safezoneW - 2.2 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			y="safezoneY + 1.4 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w="0.7 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="2 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[]={1,1,1,1};
			shadow=2;
		};
		class Pause2: Pause1
		{
			idc=1001;
			x="safezoneX + safezoneW - 3.2 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
		};
	};
	class controls
	{
		delete B_Players;
		delete B_Options;
		delete B_Abort;
		delete B_Retry;
		delete B_Load;
		delete B_Save;
		delete B_Continue;
		delete B_Diary;
		delete TrafficLight;
		class BRServerName: RscTitle
		{
			idc=24202;
			x="(safeZoneX + safeZoneW) - .7";
			y="-1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w=0.69999999;
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[]={0,0,0,0.75};
			colorText[]={1,1,1,1};
			style=1;
			font="RobotoCondensedLight";
			sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
		};
		class BRNews: RscHTML
		{
			idc=24201;
			x="(safeZoneX + safeZoneW) - .7";
			y=0;
			w=0.69999999;
			h=0.69999999;
			colorBackground[]={0,0,0,0.75};
			colorText[]={1,1,1,1};
			colorBold[]={1,1,1,1};
			colorLink[]=
			{
				"190/255",
				"0/255",
				"0/255",
				1
			};
			colorLinkActive[]=
			{
				"0/255",
				"0/255",
				"101/255",
				1
			};
			movingEnable=1;
			class H1
			{
				font="PuristaMedium";
				fontBold="PuristaMedium";
				sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1.2)";
			};
			class H2: H1
			{
				font="RobotoCondensedLight";
				fontBold="RobotoCondensed";
			};
			class P: H2
			{
				sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
				fontBold="RobotoCondensedLight";
			};
		};
		class Title: RscTitle
		{
			idc=523;
			style=0;
			text="$STR_XBOX_CONTROLER_DP_MENU";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="14.2 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="5 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[]={0,0,0,0};
		};
		class PlayersName: CA_Title
		{
			idc=109;
			style=1;
			colorBackground[]={0,0,0,0};
			x="6 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="14.2 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="10 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonCancel: RscButtonMenu
		{
			idc=2;
			shortcuts[]=
			{
				"0x00050000 + 1",
				"0x00050000 + 8"
			};
			default=1;
			class ShortcutPos: ShortcutPos
			{
				left="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			};
			text="$STR_DISP_INT_CONTINUE";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="15.3 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonSAVE: RscButtonMenu
		{
			idc=103;
			text="$STR_DISP_INT_SAVE";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="16.4 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonSkip: RscButtonMenu
		{
			idc=1002;
			text="$STR_DISP_INT_SKIP";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="16.4 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonRespawn: RscButtonMenu
		{
			idc=1010;
			text="$STR_DISP_INT_RESPAWN";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="17.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonOptions: RscButtonMenu
		{
			idc=101;
			text="$STR_A3_RscDisplayMain_ButtonOptions";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="18.6 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonVideo: RscButtonMenu
		{
			idc=301;
			text="$STR_A3_RscDisplayInterrupt_ButtonVideo";
			x="2 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="15.3 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip="$STR_TOOLTIP_MAIN_VIDEO";
		};
		class ButtonAudio: RscButtonMenu
		{
			idc=302;
			text="$STR_A3_RscDisplayInterrupt_ButtonAudio";
			x="2 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="16.4 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip="$STR_TOOLTIP_MAIN_AUDIO";
		};
		class ButtonControls: RscButtonMenu
		{
			idc=303;
			text="$STR_A3_RscDisplayInterrupt_ButtonControls";
			x="2 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="17.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip="$STR_TOOLTIP_MAIN_CONTROLS";
		};
		class ButtonGame: RscButtonMenu
		{
			idc=307;
			text="$STR_A3_RscDisplayInterrupt_ButtonGame";
			x="2 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="18.6 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			tooltip="$STR_TOOLTIP_MAIN_GAME";
		};
		class ButtonTutorialHints: RscButtonMenu
		{
			idc=122;
			text="$STR_A3_RscDisplayInterrupt_ButtonTutorialHints";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="19.7 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ButtonAbort: RscButtonMenu
		{
			idc=104;
			text="$STR_DISP_INT_ABORT";
			x="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="20.8 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="15 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class DebugConsole: RscDebugConsole
		{
			x="17 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="0.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="22 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="21.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MissionTitle: RscText
		{
			idc=120;
			font="RobotoCondensedLight";
			sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
			shadow=0;
			x="SafezoneX + (1 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			w="SafezoneW - (15 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			y="23 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Version: RscText
		{
			style=1;
			font="RobotoCondensedLight";
			sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
			shadow=0;
			x="SafezoneX + SafezoneW - (13 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			idc=1005;
			y="23 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="12 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class TraffLight: RscTrafficLight
		{
			idc=121;
			x="SafezoneX + SafezoneW - (2 *    (   ((safezoneW / safezoneH) min 1.2) / 40))";
			show=0;
			y="23 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="1 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Feedback: RscFeedback
		{
		};
		class MessageBox: RscMessageBox
		{
		};
	};
};
class RscDisplayInsertMarker
{
	scriptName="RscDisplayInsertMarker";
	scriptPath="GUI";
	onLoad="[""onLoad"",_this,""RscDisplayInsertMarker"",'GUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay')";
	onUnload="[""onUnload"",_this,""RscDisplayInsertMarker"",'GUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay')";
	idd=54;
	movingEnable=0;
	class controlsBackground
	{
		class RscText_1000: RscText
		{
			idc=1000;
			x="0 * GUI_GRID_INSERTMARKER_W + GUI_GRID_INSERTMARKER_X";
			y="0 * GUI_GRID_INSERTMARKER_H + GUI_GRID_INSERTMARKER_Y";
			w="8 * GUI_GRID_INSERTMARKER_W";
			h="2.5 * GUI_GRID_INSERTMARKER_H";
			colorBackground[]={0,0,0,0.5};
		};
	};
	class controls
	{
		delete ButtonOK;
		delete Info;
		delete ButtonMenuInfo;
		delete Picture;
		delete Text;
		delete ButtonMenuOK;
		class ButtonMenuCancel: RscButtonMenuCancel
		{
			x="19 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="13.6 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="5 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Title: RscText
		{
			colorBackground[]=
			{
				"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.77])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.51])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.08])",
				"(profilenamespace getvariable ['GUI_BCG_RGB_A',0.8])"
			};
			idc=1001;
			text="DISABLED IN BATTLEROYALE";
			x="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="8.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="10 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class Description: RscStructuredText
		{
			colorBackground[]={0,0,0,0.69999999};
			idc=1100;
			x="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="9.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="10 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class DescriptionChannel: RscStructuredText
		{
			colorBackground[]={0,0,0,0.69999999};
			idc=1101;
			x="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="11.6 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="10 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MarkerPicture: RscPicture
		{
			idc=102;
			x=0.25998399;
			y=0.40000001;
			w=0.050000001;
			h=0.0666667;
		};
		class MarkerChannel: RscCombo
		{
			idc=103;
			x="14 *    (   ((safezoneW / safezoneH) min 1.2) / 40) +    (safezoneX)";
			y="12.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +    (safezoneY + safezoneH -    (   ((safezoneW / safezoneH) min 1.2) / 1.2))";
			w="10 *    (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};
class RscBRLoadingScreen
{
	idd=25200;
	onLoad="uiNamespace setVariable ['RscBRLoadingScreen', _this select 0]";
	onUnload="uiNamespace setVariable ['RscBRLoadingScreen', displayNull];";
	class controlsBackground
	{
		class Black: RscText
		{
			colorBackground[]={0,0,0,1};
			x="safezoneXAbs";
			y="safezoneY";
			w="safezoneWAbs";
			h="safezoneH";
		};
		class CA_Vignette: RscVignette
		{
			colorText[]={0,0,0,1};
		};
	};
	class controls
	{
		class LoadingScreenText: RscText
		{
			idc=30001;
			colorText[]={1,1,1,1};
			text="";
			x="0";
			y=".4";
			w="1";
			h="1";
			sizeEx="0.06";
			style=2;
		};
		class BR_Logo: RscPicture
		{
			text="br_client\images\br_logo_mission_load.paa";
			x=".25";
			y=".15";
			w=".5";
			h=".5";
			sizeEx="1.0 * (0.04)";
			colorBackground[]={-1,-1,-1,0};
			style="0x30+ 0x800";
		};
		class MapName: RscText
		{
			text="Welcome to PLAYERUNKNOWN's Battle Royale!";
			x="safezoneX + 0.2 * (((safezoneW / safezoneH) min 1.2) / 40)";
			y="safezoneY";
			w="safezoneW - 0.4 * (((safezoneW / safezoneH) min 1.2) / 40)";
			idc=1001;
			h="1.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx="1.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressMap: RscProgress
		{
			colorBar[]=
			{
				"100/255",
				0,
				0,
				0.60000002
			};
			texture="#(argb,8,8,3)color(1,1,1,1)";
			x="safezoneX";
			y="2.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + safezoneY";
			w="safezoneW";
			idc=104;
			h="0.2 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};
class RscTitles
{
	titles[]={};
	class RscBRDamageHud
	{
		idd=2300;
		duration=999999;
		fadein=1;
		fadeout=1;
		onLoad="uiNamespace setVariable ['RscBRDamageHud', _this select 0]";
		class Controls
		{
			class Damage_Legs: RscPicture
			{
				idc=2510;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart10_White.paa";
				x="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) - 32 * (safeZoneWAbs / (getResolution select 0)) - 8 * (safeZoneWAbs / (getResolution select 0))";
				y="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0])";
				w="32 * (safeZoneWAbs / (getResolution select 0))";
				h="32 * (safeZoneH / (getResolution select 1))";
				colorBackground[]={0,0,0,0};
				style="0x30+ 0x800";
				blinkingPeriod=0;
			};
			class Damage_Chest: Damage_Legs
			{
				idc=2503;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart3_White.paa";
			};
			class Damage_Head: Damage_Legs
			{
				idc=2500;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart0_White.paa";
			};
			class Damage_Arms: Damage_Legs
			{
				idc=2508;
				text="br_client\images\GUI\BodyDamage\Icon_Body_Damage_HitPart8_White.paa";
			};
		};
	};
	class RscRespawnCounter
	{
		scriptName="RscRespawnCounter";
		scriptPath="IGUI";
		onLoad="[""onLoad"",_this,""RscRespawnCounter"",'IGUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay')";
		onUnload="[""onUnload"",_this,""RscRespawnCounter"",'IGUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay')";
		idd=3000;
		duration=9.9999998e+010;
		fadein=0;
		movingEnable=0;
		class Controls
		{
			class MPTable: RscText
			{
				colorBackground[]={0,0,0,0};
				idc=1000;
				x=0.025;
				y=0.079999998;
				w=0.94999999;
				h=0.77999997;
			};
			class TitleBackground: RscText
			{
				colorBackground[]=
				{
					"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])",
					"(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])",
					"(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",
					"(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"
				};
				idc=1002;
				x=0.5;
				y=2;
				w="13 *      (   ((safezoneW / safezoneH) min 1.2) / 40)";
				h="1.5 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
			class Title: RscText
			{
				idc=1001;
				x=0.5;
				y=2;
				w="13 *      (   ((safezoneW / safezoneH) min 1.2) / 40)";
				h="1.5 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				colorBackground[]={0,0,0,0.5};
			};
			class PlayerRespawnTime: RscText
			{
				idc=1003;
				text="00:00.000";
				x=0.5;
				y=2;
				w="7 *      (   ((safezoneW / safezoneH) min 1.2) / 40)";
				h="1.5 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				sizeEx="1.7 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
			class Description: RscStructuredText
			{
				colorBackground[]=
				{
					"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])",
					"(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])",
					"(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",
					"(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"
				};
				idc=1100;
				x=0.5;
				y=2;
				w="13 *      (   ((safezoneW / safezoneH) min 1.2) / 40)";
				h="0.8 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				sizeEx="0.8 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			};
		};
	};
	class BR_RscDisplayPlayerStatus
	{
		idd=5001;
		movingEnable=0;
		duration=100000000;
		onLoad="uiNamespace setVariable ['BR_GUI_status', _this select 0];";
		class ControlsBackground
		{
			class BR_player_StatusBG: RscText
			{
				idc=-1;
				x="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0])";
				y="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0])";
				w="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1])";
				h="(0.008+ 0.008+ 0.008+ 0.001+ 0.001+ 0.001+ 0.001) * safezoneH";
				style=2;
				type=13;
				text="";
				size="(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[]={0,0,0,0.2};
			};
			class BR_player_HealthBG: RscText
			{
				idc=-1;
				x="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005";
				y="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.008";
				w="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01";
				h="0.008* safezoneH";
				style=2;
				type=13;
				text="";
				size="(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[]={0.5,0.5,0.5,0.2};
			};
			class BR_player_ThirstBG: RscText
			{
				idc=-1;
				x="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005";
				y="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.03";
				w="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01";
				h="0.008* safezoneH";
				style=2;
				type=13;
				text="";
				size="(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[]={0.5,0.5,0.5,0.2};
			};
		};
		class Controls
		{
			class BR_player_Health: RscText
			{
				idc=1101;
				x="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005";
				y="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.008";
				w="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01";
				h="0.008* safezoneH";
				style=2;
				type=13;
				text="";
				size="(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[]={0,1,0,0.2};
			};
			class BR_player_Thirst: RscText
			{
				idc=1201;
				x="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_X',0]) + 0.005";
				y="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_Y',0]) + 0.03";
				w="(profilenamespace getvariable ['IGUI_GRID_AVCAMERA_W',1]) - 0.01";
				h="0.008* safezoneH";
				style=2;
				type=13;
				text="";
				size="(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 0.8)";
				colorBackground[]={0,0,1,0.2};
			};
		};
	};
};
class RscBRButton: RscButton
{
	sizeEx="1.2 * (0.04)";
	font="PuristaSemibold";
	colorText[]={1,1,1,1};
	colorDisabled[]={1,1,1,0.5};
	colorBackground[]={0.2,0.2,0.2,1};
	colorBackgroundDisabled[]={0,0,0,0.5};
	colorBackgroundActive[]={0,0.30000001,0.60000002,1};
	colorFocused[]={0.2,0.2,0.2,1};
};
class RscBRControlsGroup
{
	type=15;
	idc=-1;
	style=16;
	x="(safeZoneX + (SafezoneW * 0.0163))";
	y="(safeZoneY + (SafezoneH * 0.132))";
	w="(SafezoneW * 0.31)";
	h="(SafezoneH * 0.752)";
	class VScrollbar
	{
		color[]={0.5,0.5,0.5,1};
		width=0.025;
		autoScrollSpeed=-1;
		autoScrollDelay=0;
		autoScrollRewind=0;
		arrowEmpty="\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull="\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border="\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		thumb="\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
	};
	class HScrollbar
	{
		color[]={1,1,1,1};
		height=0.028000001;
	};
	class ScrollBar
	{
		color[]={1,1,1,0.60000002};
		colorActive[]={1,1,1,1};
		colorDisabled[]={1,1,1,0.30000001};
		arrowEmpty="\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull="\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border="\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		thumb="\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
	};
	class Controls
	{
	};
};
class RscBRRulesMessageBox
{
	idd=32200;
	onLoad="uiNamespace setVariable ['RscBRRulesMessageBox', _this select 0];";
	onUnload="uiNamespace setVariable ['RscBRRulesMessageBox', displayNull];";
	class controlsBackground
	{
		class RscFrame_1800: RscStructuredText
		{
			text="";
			idc=1800;
			x=0;
			y=0;
			w=1;
			h=1;
			colorBackground[]={0,0,0,0.80000001};
			colorText[]={1,1,1,0};
		};
	};
	class Controls
	{
		class myControl: RscBRControlsGroup
		{
			idc=200908;
			x=0.025;
			y=0.025;
			w=0.94999999;
			h=0.82499999;
			colorBackground[]={0,0,0,0.60000002};
			class Controls
			{
				class stxtMessage: RscStructuredText
				{
					idc=1100;
					text="";
					x=0;
					y=0;
					w=0.92500001;
					h=1;
					colorBackground[]={0,0,0,0.2};
					colorText[]={1,1,1,1};
				};
			};
		};
		class btnOK: RscBRButton
		{
			action="closeDialog 0;";
			text="O.K.";
			idc=1600;
			x=".5 - .05";
			y=0.875;
			w=0.1;
			h=0.1;
			sizeEx="2 * (0.04)";
			font="PuristaSemibold";
			colorText[]={1,1,1,1};
			colorDisabled[]={1,1,1,0.69999999};
			colorBackground[]={0.2,0.2,0.2,1};
			colorBackgroundDisabled[]={0,0,0,0.5};
			colorBackgroundActive[]={0,0.30000001,0.60000002,1};
			colorFocused[]={1,1,1,1};
		};
	};
};
class RscDisplayMain: RscStandardDisplay
{
	class Spotlight
	{
		delete EastWind;
		delete ApexProtocol;
		class Bootcamp
		{
			text="play";
			textIsQuote=0;
			picture="\br_client\images\br_logo_main_menu.paa";
			video="";
			action="_display = ctrlparent(_this select 0); ctrlactivate ((_display) displayctrl 105); _display = findDisplay 8; _control = _display displayctrl 159; ctrlActivate _control;_titleControl = _display displayCtrl 1000;_titleControl ctrlSetText 'Welcome to BATTLE ROYALE!';";
			actionText="BROWSE BATTLE ROYALE SERVERS";
			condition="(getstatvalue 'BCFirstDeployment' == 0)";
		};
	};
};
class RscDisplayMainMap
{
	class controls
	{
		class CA_MissionName: RscText
		{
			onload="_this call BR_Client_GUI_MapMenu_MonitorMenuUpdate";
			idc=112;
			shadow=0;
			font="RobotoCondensedLight";
			text="";
			x="1 *      (   ((safezoneW / safezoneH) min 1.2) / 40) +   (safezoneX)";
			y="0 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +   (safezoneY)";
			w="14 *      (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1.5 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorText[]={1,1,1,1};
			sizeEx="1.4 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};
class RscDisplayDiary
{
	class controls
	{
		class CA_MissionName: RscText
		{
			onload="_this call BR_Client_GUI_MapMenu_MonitorMenuUpdate";
			idc=112;
			shadow=0;
			font="RobotoCondensedLight";
			text="";
			x="1 *      (   ((safezoneW / safezoneH) min 1.2) / 40) +   (safezoneX)";
			y="0 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +   (safezoneY)";
			w="14 *      (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="1.5 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorText[]={1,1,1,1};
			sizeEx="1.4 *      (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
	};
};
class BR_RscListboxTeamManager: RscListBox
{
	type=5;
	style=16;
	font="PuristaMedium";
	sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	shadow=0;
	rowHeight=0;
	colorShadow[]={0,0,0,0.5};
	colorText[]={1,1,1,1};
	colorDisabled[]={1,1,1,0.25};
	colorScrollbar[]={1,0,0,0};
	colorSelect[]={0,0,0,1};
	colorSelect2[]={0,0,0,1};
	colorSelectBackground[]={0.94999999,0.94999999,0.94999999,1};
	colorSelectBackground2[]={1,1,1,0.5};
	period=1.2;
	colorBackground[]={0,0,0,0.30000001};
	maxHistoryDelay=1;
	colorPicture[]={1,1,1,1};
	colorPictureSelected[]={1,1,1,1};
	colorPictureDisabled[]={1,1,1,1};
	tooltipColorText[]={1,1,1,1};
	tooltipColorBox[]={1,1,1,1};
	tooltipColorShade[]={0,0,0,0.64999998};
	soundSelect[]=
	{
		"\A3\ui_f\data\sound\RscListbox\soundSelect",
		0.090000004,
		1
	};
	class ListScrollBar
	{
		autoScrollEnabled=1;
		color[]={1,1,1,0.60000002};
		colorActive[]={1,1,1,1};
		colorDisabled[]={1,1,1,0.30000001};
		thumb="\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
		arrowEmpty="\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull="\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border="\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		shadow=0;
		scrollSpeed=0.059999999;
		width=0;
		height=0;
		autoScrollSpeed=-1;
		autoScrollDelay=5;
		autoScrollRewind=0;
	};
};
class BR_RscButtonTeamManager: RscButton
{
	type=1;
	style=2;
	shadow=2;
	font="PuristaMedium";
	sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[]={1,1,1,1};
	colorDisabled[]={1,1,1,0.5};
	colorBackground[]={0.40000001,0.40000001,0.40000001,1};
	colorBackgroundDisabled[]={0,0,0,0.5};
	colorBackgroundActive[]={0,0.30000001,0.60000002,1};
	colorFocused[]={0.34999999,0.34999999,0.34999999,1};
};
class BR_RscTextTeamManager: RscText
{
	type=0;
	style=0;
	shadow=0;
	colorShadow[]={0,0,0,0.5};
	font="PuristaMedium";
	SizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[]={1,1,1,1};
	colorBackground[]={0,0.30000001,0.60000002,0.80000001};
	colorActive[]={0,0,0,1};
	linespacing=1;
	tooltipColorText[]={1,1,1,1};
	tooltipColorBox[]={1,1,1,1};
	tooltipColorShade[]={0,0,0,0.64999998};
};
class RscBRTeamManager
{
	idd=7200;
	duration=999999;
	fadein=1;
	fadeout=1;
	onLoad="uiNamespace setVariable ['RscBRTeamManager', _this select 0]; _this call BR_Client_GUI_Teams_InitializeMenu; true call BR_Client_GUI_Teams_UI_ToggleDialogBlur";
	onUnload="uiNamespace setVariable ['RscBRTeamManager', displayNull]; false call BR_Client_GUI_Teams_UI_ToggleDialogBlur";
	class controlsBackground
	{
		class backDropDark: RscText
		{
			idc=1803;
			x="safeZoneX";
			y="safeZoneY";
			w="safeZoneW";
			h="safeZoneH";
			colorBackground[]={0,0,0,0.2};
			text="";
		};
		class frameBackground: RscText
		{
			idc=1803;
			x="0 * (0.025)+ (0)";
			y="0 * (0.04)+ (0)";
			w="40 * (0.025)";
			h="25.5 * (0.04)";
			colorBackground[]={0,0,0,0.80000001};
			text="";
		};
		class frameYourTeam: RscFrame
		{
			idc=1800;
			x="22.5 * (0.025)+ (0)";
			y="3 * (0.04)+ (0)";
			w="17 * (0.025)";
			h="10.5 * (0.04)";
			colorText[]={0.69999999,0.69999999,0.69999999,1};
		};
		class frameInviteOthers: RscFrame
		{
			idc=1801;
			x="0.5 * (0.025)+ (0)";
			y="3 * (0.04)+ (0)";
			w="17 * (0.025)";
			h="20.5 * (0.04)";
			colorText[]={0.69999999,0.69999999,0.69999999,1};
		};
		class frameInvites4U: RscFrame
		{
			idc=1802;
			x="22.5 * (0.025)+ (0)";
			y="15 * (0.04)+ (0)";
			w="17 * (0.025)";
			h="8.5 * (0.04)";
			colorText[]={0.69999999,0.69999999,0.69999999,1};
		};
	};
	class controls
	{
		class Title: RscStructuredText
		{
			idc=7201;
			style="0x0C";
			text="<t size='1.4' valign='bottom' font='PuristaMedium' align='left'>Team Menu</t><t size='.8' valign='bottom' align='right' font='PuristaMedium'>      Create and Manage your team here.</t>";
			x="0.5 * (0.025)+ (0)";
			y="0.5 * (0.04)+ (0)";
			w="39 * (0.025)";
			h="1.5 * (0.04)";
			colorText[]={1,1,1,1};
			colorBackground[]={0,0,0,1};
			colorActive[]={0,0,0,1};
		};
		class TitleInvitePlayers: BR_RscTextTeamManager
		{
			idc=7207;
			text="Invite Players to your Team";
			x="0.5 * (0.025)+ (0)";
			y="3 * (0.04)+ (0)";
			w="17 * (0.025)";
			h="1.5 * (0.04)";
		};
		class InvitePlayerListRefresh: RscActiveText
		{
			style=48;
			idc=7220;
			text="br_client\images\GUI\TeamManager\Refresh.paa";
			x="15.5 * (0.025)+ (0)";
			y="3.25 * (0.04)+ (0)";
			w="1.5 * (0.025)";
			h="1 * (0.04)";
			color[]={1,1,1,0.80000001};
			colorActive[]={1,1,1,1};
			colorDisabled[]={1,1,1,1};
		};
		class PlayerList: BR_RscListboxTeamManager
		{
			idc=7204;
			x="1 * (0.025)+ (0)";
			y="5 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="16 * (0.04)";
		};
		class ButtonInvite: BR_RscButtonTeamManager
		{
			idc=7202;
			text="Invite Selected Player";
			x="1 * (0.025)+ (0)";
			y="21.5 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="1.5 * (0.04)";
		};
		class TitleYourTeam: BR_RscTextTeamManager
		{
			idc=7205;
			text="Your Team";
			x="22.5 * (0.025)+ (0)";
			y="3 * (0.04)+ (0)";
			w="17 * (0.025)";
			h="1.5 * (0.04)";
		};
		class TeamList: BR_RscListboxTeamManager
		{
			idc=7203;
			x="23 * (0.025)+ (0)";
			y="5 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="4 * (0.04)";
		};
		class ButtomRemovePlayer: BR_RscButtonTeamManager
		{
			idc=7206;
			text="Remove Selected Player";
			x="23 * (0.025)+ (0)";
			y="9.5 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="1.5 * (0.04)";
		};
		class ButtonLeaveTeam: BR_RscButtonTeamManager
		{
			idc=7208;
			text="Leave your Team";
			x="23 * (0.025)+ (0)";
			y="11.5 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="1.5 * (0.04)";
		};
		class TitleInvitesForYou: BR_RscTextTeamManager
		{
			idc=7209;
			text="Invites for You";
			x="22.5 * (0.025)+ (0)";
			y="15 * (0.04)+ (0)";
			w="17 * (0.025)";
			h="1.5 * (0.04)";
		};
		class InviteListForYou: BR_RscListboxTeamManager
		{
			idc=7210;
			x="23 * (0.025)+ (0)";
			y="17 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="4 * (0.04)";
		};
		class ButtonAcceptInvite: BR_RscButtonTeamManager
		{
			idc=7211;
			text="Accept Team Invite";
			x="23 * (0.025)+ (0)";
			y="21.5 * (0.04)+ (0)";
			w="16 * (0.025)";
			h="1.5 * (0.04)";
		};
		class CheckboxMuteInvites: RscCheckbox
		{
			idc=7221;
			x="1 * (0.025)+ (0)";
			y="23.75 * (0.04)+ (0)";
			w="1.5 * (0.025)";
			h="1.5 * (0.04)";
			color[]={0.69999999,0.69999999,0.69999999,0.69999999};
			colorFocused[]={0.69999999,0.69999999,0.69999999,1};
			colorHover[]={0.69999999,0.69999999,0.69999999,1};
			colorPressed[]={0.69999999,0.69999999,0.69999999,1};
			colorDisabled[]={1,1,1,0.2};
			colorBackground[]={0,0,0,0};
			colorBackgroundFocused[]={0,0,0,0};
			colorBackgroundHover[]={0,0,0,0};
			colorBackgroundPressed[]={0,0,0,0};
			colorBackgroundDisabled[]={0,0,0,0};
		};
		class TitleMuteInvites: RscText
		{
			type=0;
			idc=7222;
			text="Mute invite notifications";
			style=0;
			x="2.5 * (0.025)+ (0)";
			y="23.75 * (0.04)+ (0)";
			w="39 * (0.025)";
			h="1.5 * (0.04)";
			font="PuristaMedium";
			SizeEx="1 * (   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
			colorText[]={0.69999999,0.69999999,0.69999999,1};
			colorBackground[]={0,0,0,0};
			shadow=0;
			colorShadow[]={0,0,0,0.5};
			colorActive[]={1,1,1,1};
			linespacing=1;
			tooltipColorText[]={1,1,1,1};
			tooltipColorBox[]={1,1,1,1};
			tooltipColorShade[]={0,0,0,0.64999998};
		};
	};
};
class BR_IGUIBack
{
	type=0;
	idc=124;
	style=128;
	text="";
	colorText[]={0,0,0,0};
	font="PuristaMedium";
	sizeEx=0;
	shadow=0;
	x=0.1;
	y=0.1;
	w=0.1;
	h=0.1;
	colorbackground[]=
	{
		"(profilenamespace getvariable ['IGUI_BCG_RGB_R',0])",
		"(profilenamespace getvariable ['IGUI_BCG_RGB_G',1])",
		"(profilenamespace getvariable ['IGUI_BCG_RGB_B',1])",
		"(profilenamespace getvariable ['IGUI_BCG_RGB_A',0.8])"
	};
};
class BR_RscButton
{
	type=1;
	style=2;
	x=0;
	y=0;
	w=0.095588997;
	h=0.039216001;
	shadow=2;
	font="PuristaMedium";
	sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[]={1,1,1,1};
	colorDisabled[]={1,1,1,0.25};
	colorBackground[]={0,0,0,0.5};
	colorBackgroundActive[]={0,0,0,1};
	colorBackgroundDisabled[]={0,0,0,0.5};
	colorFocused[]={0,0,0,1};
	colorShadow[]={0,0,0,0};
	offsetX=0;
	offsetY=0;
	offsetPressedX=0;
	offsetPressedY=0;
	colorBorder[]={0,0,0,1};
	borderSize=0;
	soundEnter[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundEnter",
		0.090000004,
		1
	};
	soundPush[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundPush",
		0.090000004,
		1
	};
	soundClick[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundClick",
		0.090000004,
		1
	};
	soundEscape[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundEscape",
		0.090000004,
		1
	};
};
class BR_RscText
{
	type=0;
	x=0;
	y=0;
	h=0.037;
	w=0.30000001;
	style=0;
	shadow=1;
	colorShadow[]={0,0,0,0.5};
	font="PuristaMedium";
	SizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[]={1,1,1,1};
	colorBackground[]={0,0,0,0};
	linespacing=1;
	tooltipColorText[]={1,1,1,1};
	tooltipColorBox[]={1,1,1,1};
	tooltipColorShade[]={0,0,0,0.64999998};
};
class BR_RscListbox
{
	type=5;
	style=16;
	font="PuristaMedium";
	sizeEx="(   (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	shadow=0;
	rowHeight=0;
	colorShadow[]={0,0,0,0.5};
	colorText[]={1,1,1,1};
	colorDisabled[]={1,1,1,0.25};
	colorScrollbar[]={1,0,0,0};
	colorSelect[]={0,0,0,1};
	colorSelect2[]={0,0,0,1};
	colorSelectBackground[]={0.94999999,0.94999999,0.94999999,1};
	colorSelectBackground2[]={1,1,1,0.5};
	period=1.2;
	colorBackground[]={0,0,0,0.30000001};
	maxHistoryDelay=1;
	colorPicture[]={1,1,1,1};
	colorPictureSelected[]={1,1,1,1};
	colorPictureDisabled[]={1,1,1,1};
	tooltipColorText[]={1,1,1,1};
	tooltipColorBox[]={1,1,1,1};
	tooltipColorShade[]={0,0,0,0.64999998};
	soundSelect[]=
	{
		"\A3\ui_f\data\sound\RscListbox\soundSelect",
		0.090000004,
		1
	};
	class ListScrollBar
	{
		autoScrollEnabled=1;
		color[]={1,1,1,0.60000002};
		colorActive[]={1,1,1,1};
		colorDisabled[]={1,1,1,0.30000001};
		thumb="\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
		arrowEmpty="\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
		arrowFull="\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
		border="\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
		shadow=0;
		scrollSpeed=0.059999999;
		width=0;
		height=0;
		autoScrollSpeed=-1;
		autoScrollDelay=5;
		autoScrollRewind=0;
	};
};
class BR_SkinSelection
{
	idd=1339;
	movingEnable=0;
	enableSimulation=0;
	name="BR_SkinSelection";
	onLoad="[_this select 0] call BR_Client_GUI_Skins_InitSkinSelectionMenu";
	onUnload="[] call BR_Client_GUI_Skins_ResetTexture;";
	class controlsBackground
	{
	};
	class Controls
	{
		class IGUIBack_2200: BR_IGUIBack
		{
			idc=2200;
			x=0.25;
			y=0.16;
			w=0.5;
			h=0.80000001;
		};
		class RscText_1000: BR_RscText
		{
			idc=1000;
			text="Select a Skin";
			x=0.42500001;
			y=0.16;
			w=0.15000001;
			h=0.039999999;
		};
		class RscListbox_1500: BR_RscListbox
		{
			idc=1500;
			x=0.27500001;
			y=0.22;
			w=0.44999999;
			h=0.66000003;
		};
		class RscButton_1600: BR_RscButton
		{
			idc=1600;
			text="Apply Skin";
			x=0.27500001;
			y=0.89999998;
			w=0.44999999;
			h=0.039999999;
		};
		class RscButton_1601: BR_RscButton
		{
			idc=1601;
			text="X";
			x=0.73750001;
			y=0.14;
			w=0.025;
			h=0.039999999;
		};
	};
};
class RscNotificationArea: RscControlsGroupNoScrollbars
{
	idc=312;
	x="0 *     (   ((safezoneW / safezoneH) min 1.2) / 40) +  (profilenamespace getvariable [""IGUI_GRID_NOTIFICATION_X"", (0.5 - 6 *    (   ((safezoneW / safezoneH) min 1.2) / 40))])";
	y="0 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25) +  (profilenamespace getvariable [""IGUI_GRID_NOTIFICATION_Y"", (safezoneY + 6.5 *    (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25))])";
	w="12 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
	h="6 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
	class controls
	{
		class Notification: RscControlsGroupNoScrollbars
		{
			x=0;
			y=0;
			idc=13435;
			w="12 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
			h="3 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			class controls
			{
				class Title: RscText
				{
					colorBackground[]={0,0,0,0.69999999};
					idc=12135;
					x="0 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					y="0 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w="12 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					h="0.8 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					sizeEx="0.8 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class PictureBackground: RscText
				{
					colorBackground[]={0,0.2,0.40000001,0.80000001};
					idc=12136;
					x="0 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					y="0.9 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w="2 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					h="2 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class Picture: RscPictureKeepAspect
				{
					idc=12335;
					x="0.1 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					y="1 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w="1.8 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					h="1.8 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class Score: RscText
				{
					shadow=0;
					style=2;
					idc=12137;
					x="0 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					y="0.9 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w="2 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					h="2 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					sizeEx="1.2 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class DescriptionBackground: RscText
				{
					colorBackground[]={0,0.2,0.40000001,0.80000001};
					idc=12138;
					x="2.1 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					y="0.9 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w="9.9 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					h="2 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
				class Description: RscStructuredText
				{
					class Attributes
					{
						size=0.80000001;
						align="center";
					};
					idc=12235;
					x="2.1 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					y="0.9 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					w="9.9 *     (   ((safezoneW / safezoneH) min 1.2) / 40)";
					h="2 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
					colorBackground[]={0,0,0,0};
					sizeEx="1 *     (   (   ((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				};
			};
		};
	};
};
class CfgSFX
{
	class DestrHouse
	{
		sounds[]=
		{
			"DestrHouse1",
			"DestrHouse2",
			"DestrHouse3"
		};
		DestrHouse1[]=
		{
			"A3\Sounds_F\sfx\special_sfx\house_destroy_1",
			0.69125092,
			1,
			900,
			0.33000001,
			0,
			2,
			5
		};
		DestrHouse2[]=
		{
			"A3\Sounds_F\sfx\special_sfx\house_destroy_2",
			0.69125092,
			1,
			900,
			0.33000001,
			0,
			2,
			5
		};
		DestrHouse3[]=
		{
			"A3\Sounds_F\sfx\special_sfx\house_destroy_3",
			0.69125092,
			1,
			900,
			0.34,
			0,
			2,
			5
		};
		empty[]=
		{
			"",
			0.89125091,
			1,
			500,
			1,
			0,
			2,
			5
		};
	};
	class DestrHousePart
	{
		sounds[]=
		{
			"DestrHousePart1"
		};
		DestrHousePart1[]=
		{
			"A3\Sounds_F\sfx\special_sfx\house_destroy_3",
			0.89125091,
			1,
			900,
			1,
			0,
			2,
			5
		};
		empty[]=
		{
			"",
			0,
			0,
			0,
			0,
			1,
			5,
			20
		};
	};
};
class CfgSkins
{
	uniform="U_Rangemaster";
	class Clear
	{
		name="Default Skin";
		texture="a3\characters_f_kart\civil\data\c_poloshirtpants_2_co.paa";
	};
	class Glitch
	{
		name="Glitch";
		texture="\br_client\images\Skins\glitch.paa";
	};
	class Glitch2
	{
		name="Glitch2";
		texture="\br_client\images\Skins\glitch2.paa";
	};
	class Punk
	{
		name="Punk";
		texture="\br_client\images\Skins\punk.paa";
	};
	class Tux
	{
		name="Tux";
		texture="\br_client\images\Skins\tux.paa";
	};
	class Shark
	{
		name="Shark";
		texture="\br_client\images\Skins\KP.paa";
	};
	class kappa
	{
		name="Kappa";
		texture="\br_client\images\Skins\kappa.paa";
	};
	class salt
	{
		name="PJSalt";
		texture="\br_client\images\Skins\SALT.paa";
	};
	class America
	{
		name="Stars";
		texture="\br_client\images\Skins\america.paa";
	};
	class Merica
	{
		name="Merica";
		texture="\br_client\images\Skins\merica.paa";
	};
	class Ireland
	{
		name="Ireland";
		texture="\br_client\images\Skins\ireland.paa";
	};
	class UK
	{
		name="UK";
		texture="\br_client\images\Skins\UK.paa";
	};
	class Germany
	{
		name="Germany";
		texture="\br_client\images\Skins\germany.paa";
	};
	class Scotland
	{
		name="Scotland";
		texture="\br_client\images\Skins\scotland.paa";
	};
	class France
	{
		name="France";
		texture="\br_client\images\Skins\FR.paa";
	};
	class BleedPurple
	{
		name="#BleedPurple";
		texture="\br_client\images\Skins\twitch.paa";
	};
	class Thump
	{
		name="Thump";
		texture="\br_client\images\Skins\thump.paa";
	};
	class Jumper
	{
		name="TisTheSeason";
		texture="\br_client\images\Skins\xmas.paa";
	};
	class North
	{
		name="North";
		texture="\br_client\images\Skins\North.paa";
	};
	class Canada
	{
		name="Canada";
		texture="\br_client\images\Skins\canada.paa";
	};
	class Romania
	{
		name="Romania";
		texture="\br_client\images\Skins\Romania.paa";
	};
	class Russia
	{
		name="Russia";
		texture="\br_client\images\Skins\Russia.paa";
	};
	class Lederhosen
	{
		name="Lederhosen";
		texture="\br_client\images\Skins\Lederhosen.paa";
	};
	class Netherlands
	{
		name="Netherlands";
		texture="\br_client\images\Skins\Netherlands.paa";
	};
	class Memer4Life
	{
		name="Memer4Life";
		texture="\br_client\images\Skins\Memer4Life.paa";
	};
	class HassanChop
	{
		name="HassanChop";
		texture="\br_client\images\Skins\HassanChop.paa";
	};
	class GrandMaster
	{
		name="GrandMaster";
		texture="\br_client\images\Skins\GrandMaster.paa";
	};
};
class CfgSkeletons
{
	class Default
	{
		isDiscrete=1;
		skeletonInherit="";
		skeletonBones[]={};
	};
	class L85A2_Skeleton: Default
	{
		skeletonBones[]=
		{
			"magazine",
			"",
			"bolt",
			"",
			"fire_switch",
			"",
			"iron_sight",
			"",
			"front_sight",
			"",
			"Trigger",
			"",
			"ris",
			"",
			"muzzle",
			"",
			"zasleh",
			""
		};
	};
	class L86A2_Skeleton: L85A2_Skeleton
	{
		skeletonInherit="L85A2_Skeleton";
		skeletonBones[]=
		{
			"bipod_legs",
			"",
			"bipod_leg_L",
			"bipod_legs",
			"leg_L",
			"bipod_leg_L",
			"bipod_leg_R",
			"bipod_legs",
			"leg_R",
			"bipod_leg_R",
			"bipod_simple_forward",
			"",
			"bipod_simple_backward",
			""
		};
	};
	class L85A2_UGL_Skeleton: L85A2_Skeleton
	{
		skeletonInherit="L85A2_Skeleton";
		skeletonBones[]=
		{
			"ladder",
			""
		};
	};
};
class CfgSoundSets
{
	class Rifle_silencerShot_Base_SoundSet;
	class TRG20_silencerShot_SoundSet: Rifle_silencerShot_Base_SoundSet
	{
		soundShaders[]=
		{
			"TRG20_Closure_SoundShader"
		};
	};
	class Mk20_silencerShot_SoundSet: Rifle_silencerShot_Base_SoundSet
	{
		soundShaders[]=
		{
			"Mk20_Closure_SoundShader"
		};
	};
	class BombsHeavy_Exp_SoundSet
	{
		soundShaders[]=
		{
			"BombsHeavy_closeExp_SoundShader",
			"BombsHeavy_midExp_SoundShader",
			"BombsHeavy_distExp_SoundShader"
		};
		volumeFactor=0.1;
		volumeCurve="LinearCurve";
		spatial=1;
		doppler=0;
		loop=0;
		sound3DProcessingType="ExplosionLight3DProcessingType";
		distanceFilter="explosionDistanceFreqAttenuationFilter";
	};
	class BombsHeavy_Tail_SoundSet
	{
		soundShaders[]=
		{
			"BombsHeavy_tailForest_SoundShader",
			"BombsHeavy_tailMeadows_SoundShader",
			"BombsHeavy_tailHouses_SoundShader"
		};
		volumeFactor=0.0099999998;
		volumeCurve="InverseSquare2Curve";
		spatial=1;
		doppler=0;
		loop=0;
		soundShadersLimit=2;
		frequencyRandomizer=0.050000001;
		sound3DProcessingType="ExplosionLight3DProcessingType";
		distanceFilter="explosionTailDistanceFreqAttenuationFilter";
	};
	class Explosion_Debris_SoundSet
	{
		soundShaders[]=
		{
			"Explosion_debrisSoft_SoundShader",
			"Explosion_debrisDirt_SoundShader",
			"Explosion_debrisHard_SoundShader"
		};
		volumeFactor=0.0099999998;
		volumeCurve[]=
		{
			{0,1},
			{30,0.75},
			{50,0.5},
			{100,0}
		};
		spatial=1;
		doppler=0;
		loop=0;
		soundShadersLimit=2;
		sound3DProcessingType="ExplosionLight3DProcessingType";
	};
	class BigIED_Exp_SoundSet
	{
		soundShaders[]=
		{
			"BigIED_closeExp_SoundShader",
			"BigIED_midExp_SoundShader",
			"BigIED_distExp_SoundShader"
		};
		volumeFactor=0.30000001;
		volumeCurve="LinearCurve";
		spatial=1;
		doppler=0;
		loop=0;
		sound3DProcessingType="ExplosionHeavy3DProcessingType";
		distanceFilter="explosionDistanceFreqAttenuationFilter";
	};
	class BigIED_Tail_SoundSet
	{
		soundShaders[]=
		{
			"BigIED_tailForest_SoundShader",
			"BigIED_tailMeadows_SoundShader",
			"BigIED_tailHouses_SoundShader"
		};
		volumeFactor=0.15000001;
		volumeCurve="LinearCurve";
		spatial=1;
		doppler=0;
		loop=0;
		soundShadersLimit=2;
		frequencyRandomizer=0.050000001;
		sound3DProcessingType="ExplosionHeavyTail3DProcessingType";
		distanceFilter="explosionTailDistanceFreqAttenuationFilter";
	};
};
class CfgVehicles
{
	class Man;
	class CAManBase: Man
	{
		delete SoundBreathInjured;
		delete SoundInjured;
	};
	class C_man_1;
	class B_BattleRoyalePlayer_F: C_man_1
	{
		side=3;
		author="BattleRoyaleGames";
		_generalMacro="B_BattleRoyalePlayer_F";
		scope=2;
		scopeArsenal=2;
		model="\A3\Characters_F\Civil\c_poloshirtpants.p3d";
		displayName="Battle Royale Player";
		cost=10;
		identityTypes[]=
		{
			"Head_Rangemaster",
			"G_Rangemaster"
		};
		nakedUniform="U_BasicBody";
		uniformClass="U_Rangemaster";
		Items[]={};
		respawnItems[]={};
		class EventHandlers
		{
			init="";
		};
		hiddenSelectionsTextures[]=
		{
			"a3\characters_f_kart\civil\data\c_poloshirtpants_2_co.paa"
		};
		weapons[]=
		{
			"Throw",
			"Put"
		};
		canDeactivateMines=1;
		engineer=1;
		attendant=1;
		respawnWeapons[]=
		{
			"Throw",
			"Put"
		};
		magazines[]={};
		respawnMagazines[]={};
		linkedItems[]=
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"ItemGPS"
		};
		respawnlinkedItems[]=
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"ItemGPS"
		};
		class Wounds
		{
			tex[]={};
			mat[]=
			{
				"A3\Characters_F\Civil\Data\c_poloshirtpants.rvmat",
				"A3\Characters_F\Civil\Data\c_poloshirtpants_injury.rvmat",
				"A3\Characters_F\Civil\Data\c_poloshirtpants_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_old.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"
			};
		};
	};
	class SoldierEB;
	class O_Soldier_base_F: SoldierEB
	{
		class HitPoints
		{
			class HitFace
			{
				armor=1;
				material=-1;
				name="face_hub";
				passThrough=0.1;
				radius=0.079999998;
				explosionShielding=0.1;
				minimalHit=0.0099999998;
			};
			class HitNeck: HitFace
			{
				armor=1;
				material=-1;
				name="neck";
				passThrough=0.1;
				radius=0.1;
				explosionShielding=0.5;
				minimalHit=0.0099999998;
			};
			class HitHead: HitNeck
			{
				armor=1;
				material=-1;
				name="head";
				passThrough=0.1;
				radius=0.2;
				explosionShielding=0.5;
				minimalHit=0.0099999998;
				depends="HitFace max HitNeck";
			};
			class HitPelvis
			{
				armor=1;
				material=-1;
				name="pelvis";
				passThrough=0.1;
				radius=0.2;
				explosionShielding=1;
				visual="injury_body";
				minimalHit=0.0099999998;
			};
			class HitAbdomen: HitPelvis
			{
				armor=1;
				material=-1;
				name="spine1";
				passThrough=0.1;
				radius=0.15000001;
				explosionShielding=1;
				visual="injury_body";
				minimalHit=0.0099999998;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor=1;
				material=-1;
				name="spine2";
				passThrough=0.1;
				radius=0.15000001;
				explosionShielding=6;
				visual="injury_body";
				minimalHit=0.0099999998;
			};
			class HitChest: HitDiaphragm
			{
				armor=1;
				material=-1;
				name="spine3";
				passThrough=0.1;
				radius=0.15000001;
				explosionShielding=6;
				visual="injury_body";
				minimalHit=0.0099999998;
			};
			class HitBody: HitChest
			{
				armor=1000;
				material=-1;
				name="body";
				passThrough=0.1;
				radius=0.16;
				explosionShielding=6;
				visual="injury_body";
				minimalHit=0.0099999998;
				depends="HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms
			{
				armor=1;
				material=-1;
				name="arms";
				passThrough=1;
				radius=0.1;
				explosionShielding=1;
				visual="injury_hands";
				minimalHit=0.0099999998;
			};
			class HitHands: HitArms
			{
				armor=1;
				material=-1;
				name="hands";
				passThrough=1;
				radius=0.1;
				explosionShielding=1;
				visual="injury_hands";
				minimalHit=0.0099999998;
				depends="HitArms";
			};
			class HitLegs
			{
				armor=1;
				material=-1;
				name="legs";
				passThrough=1;
				radius=0.12;
				explosionShielding=1;
				visual="injury_legs";
				minimalHit=0.0099999998;
			};
		};
		armor=2;
	};
	class Car;
	class Car_F: Car
	{
		crewCrashProtection=0.69999999;
		class HitPoints
		{
			class HitLFWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRF2Wheel;
			class HitBody;
			class HitFuel;
			class HitEngine;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
		};
	};
	class SUV_01_base_F: Car_F
	{
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor=0.55000001;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.55000001;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.55000001;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.55000001;
			};
			class HitGlass5: HitGlass5
			{
				armor=0.55000001;
			};
			class HitGlass6: HitGlass6
			{
				armor=0.55000001;
			};
		};
	};
	class Offroad_01_base_F: Car_F
	{
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor=0.55000001;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.55000001;
			};
		};
	};
	class Hatchback_01_base_F: Car_F
	{
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor=0.55000001;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.55000001;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.55000001;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.55000001;
			};
		};
	};
	class beetle_custom: Car_F
	{
		armor=30;
		armorStructural=1;
		wheelDamageThreshold=0.19999999;
		wheelDestroyThreshold=0.39000002;
		wheelDamageRadiusCoef=0.94999999;
		wheelDestroyRadiusCoef=0.44999999;
		class HitPoints: HitPoints
		{
			class HitLFWheel: HitLFWheel
			{
				armor=0.75;
				passThrough=0;
				radius=0.33000001;
			};
			class HitLF2Wheel: HitLF2Wheel
			{
				armor=0.75;
				passThrough=0;
				radius=0.33000001;
			};
			class HitRFWheel: HitRFWheel
			{
				armor=0.75;
				passThrough=0;
				radius=0.33000001;
			};
			class HitRF2Wheel: HitRF2Wheel
			{
				armor=0.75;
				passThrough=0;
				radius=0.33000001;
			};
			class HitFuel
			{
				armor=0.2;
				material=-1;
				name="palivo";
				visual="";
				passThrough=1;
				minimalHit=0.0099999998;
				explosionShielding=0.1;
				radius=0.25;
			};
			class HitEngine
			{
				armor=0.89999998;
				material=-1;
				name="motor";
				visual="";
				passThrough=0.2;
				minimalHit=0.0099999998;
				explosionShielding=0.40000001;
				radius=0.44999999;
			};
			class HitBody
			{
				armor=0.30000001;
				material=-1;
				name="karoserie";
				visual="zbytek";
				passThrough=1;
				minimalHit=0.0099999998;
				explosionShielding=0.60000002;
				radius=0.44999999;
			};
			class HitGlass1: HitGlass1
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass5: HitGlass5
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
			class HitGlass6: HitGlass6
			{
				armor=0.25;
				explosionShielding=0;
				radius=0.25;
			};
		};
	};
	class Truck_F: Car_F
	{
		crewCrashProtection=0.69999999;
		epeImpulseDamageCoef=20;
		class HitPoints: HitPoints
		{
			class HitRGlass;
			class HitLGlass;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitBody;
			class HitFuel;
			class HitLFWheel;
			class HitLBWheel;
			class HitLMWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRBWheel;
			class HitRMWheel;
			class HitRF2Wheel;
			class HitEngine;
		};
	};
	class Van_01_base_F: Truck_F
	{
		armor=30;
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor=0.55000001;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.55000001;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.55000001;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.55000001;
			};
		};
	};
	class Truck_02_base_F: Truck_F
	{
		armor=80;
		wheelDamageThreshold=0.69999999;
		wheelDestroyThreshold=0.99000001;
		wheelDamageRadiusCoef=0.94999999;
		wheelDestroyRadiusCoef=0.60000002;
		class HitPoints: HitPoints
		{
			class HitLFWheel: HitLFWheel
			{
				armor=0.69999999;
				radius=0.2;
			};
			class HitLF2Wheel: HitLF2Wheel
			{
				armor=0.69999999;
				radius=0.2;
			};
			class HitRFWheel: HitRFWheel
			{
				armor=0.69999999;
				radius=0.2;
			};
			class HitRF2Wheel: HitRF2Wheel
			{
				armor=0.69999999;
				radius=0.2;
			};
			class HitFuel: HitFuel
			{
				name="palivo";
				armor=2;
				radius=0.44999999;
			};
			class HitEngine: HitEngine
			{
				name="engine";
				armor=4;
				radius=0.25;
			};
			class HitGlass1: HitGlass1
			{
				armor=0.2;
				radius=0.5;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.2;
				radius=0.5;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.2;
				radius=0.5;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.2;
				radius=0.5;
			};
			class HitGlass5: HitGlass5
			{
				armor=0.2;
				radius=0.5;
			};
			class HitGlass6: HitGlass6
			{
				armor=0.2;
				radius=0.5;
			};
		};
	};
};
class CfgVoice
{
	class Base;
	class ENG: Base
	{
		protocol="";
	};
	class ENGB: Base
	{
		protocol="";
	};
	class GRE: Base
	{
		protocol="";
	};
	class PER: Base
	{
		protocol="";
	};
	class ENGVR: Base
	{
		protocol="";
	};
	class GREVR: Base
	{
		protocol="";
	};
	class PERVR: Base
	{
		protocol="";
	};
};
class Mode_SemiAuto;
class Mode_FullAuto;
class CfgWeapons
{
	class Pistol;
	class Pistol_Base_F: Pistol
	{
		class WeaponSlotsInfo;
	};
	class hgun_Rook40_F: Pistol_Base_F
	{
		magazines[]=
		{
			"16Rnd_9x21_Mag",
			"30Rnd_9x21_Mag"
		};
	};
	class hgun_P07_F: Pistol_Base_F
	{
		magazines[]=
		{
			"16Rnd_9x21_Mag",
			"30Rnd_9x21_Mag"
		};
	};
	class Rifle;
	class Rifle_Base_F: Rifle
	{
		class WeaponSlotsInfo
		{
			class MuzzleSlot;
		};
	};
	class CUP_srifle_LeeEnfield: Rifle_Base_F
	{
		class Single: Mode_SemiAuto
		{
			reloadTime=3;
			backgroundReload=1;
		};
	};
	class CUP_srifle_CZ550_base: Rifle_Base_F
	{
		class Single: Mode_SemiAuto
		{
			reloadTime=2.5;
			backgroundReload=1;
		};
	};
	class CUP_sgun_AA12: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_20Rnd_B_AA12_Pellets"
		};
		fireSpreadAngle=5;
	};
	class CUP_sgun_Saiga12K: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_8Rnd_B_Saiga12_74Pellets_M"
		};
		fireSpreadAngle=4;
	};
	class CUP_sgun_M1014: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_8Rnd_B_Beneli_74Pellets"
		};
		fireSpreadAngle=3;
	};
	class CUP_arifle_XM8_Base: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class Tavor_base_F: Rifle_Base_F
	{
		class Single: Mode_SemiAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"TRG20_silencerShot_SoundSet",
					"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
		class FullAuto: Mode_FullAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"TRG20_silencerShot_SoundSet",
					"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class mk20_base_F: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
		class Single: Mode_SemiAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"Mk20_silencerShot_SoundSet",
					"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
		class FullAuto: Mode_FullAuto
		{
			class BaseSoundModeType;
			class SilencedSound: BaseSoundModeType
			{
				soundsetshot[]=
				{
					"Mk20_silencerShot_SoundSet",
					"CUP_m16_ShotSD_SoundSet",
					"CUP_rifle1_SD_Tail_SoundSet"
				};
			};
		};
	};
	class SDAR_base_F: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_l85a2_base: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_CZ805_Base: Rifle_Base_F
	{
	};
	class CUP_arifle_CZ805_A1: CUP_arifle_CZ805_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_CZ805_A2: CUP_arifle_CZ805_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_CZ805_GL: CUP_arifle_CZ805_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class arifle_Katiba_Base_F: Rifle_Base_F
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[]=
				{
					"muzzle_snds_h"
				};
			};
		};
	};
	class arifle_MX_Base_F: Rifle_Base_F
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[]=
				{
					"muzzle_snds_h"
				};
			};
		};
	};
	class CUP_arifle_CZ805_B_Base: CUP_arifle_CZ805_Base
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot;
		};
	};
	class CUP_arifle_CZ805_B_GL: CUP_arifle_CZ805_B_Base
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[]=
				{
					"muzzle_snds_B"
				};
			};
		};
	};
	class CUP_arifle_CZ805_B: CUP_arifle_CZ805_B_Base
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[]=
				{
					"muzzle_snds_B"
				};
			};
		};
	};
	class CUP_arifle_SCAR_Base: Rifle_Base_F
	{
	};
	class CUP_arifle_SCAR_L_Base: CUP_arifle_SCAR_Base
	{
	};
	class CUP_arifle_Mk16_CQC: CUP_arifle_SCAR_L_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_Mk16_STD: CUP_arifle_SCAR_L_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_Mk16_SV: CUP_arifle_SCAR_L_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_Mk17_Base: CUP_arifle_SCAR_Base
	{
		magazines[]=
		{
			"CUP_20Rnd_762x51_B_SCAR",
			"CUP_20Rnd_TE1_Yellow_Tracer_762x51_SCAR"
		};
	};
	class CUP_arifle_G36_Base: Rifle_Base_F
	{
	};
	class CUP_arifle_G36A: CUP_arifle_G36_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_G36C: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_MG36: CUP_arifle_G36C
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_M16_Base: Rifle_Base_F
	{
	};
	class CUP_arifle_M16A4_Base: CUP_arifle_M16_Base
	{
	};
	class CUP_arifle_M4_Base: CUP_arifle_M16A4_Base
	{
	};
	class CUP_arifle_M4A1_BUIS_Base: CUP_arifle_M4_Base
	{
	};
	class CUP_arifle_M4A1_black: CUP_arifle_M4A1_BUIS_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_M4A1_camo: CUP_arifle_M4A1_BUIS_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_M4A1_BUIS_GL: CUP_arifle_M4A1_BUIS_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_M4A1_BUIS_camo_GL: CUP_arifle_M4A1_BUIS_Base
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_arifle_AK_Base: Rifle_Base_F
	{
		magazines[]=
		{
			"CUP_30Rnd_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
		};
	};
	class CUP_arifle_AK74: CUP_arifle_AK_Base
	{
		magazines[]=
		{
			"CUP_30Rnd_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
		};
	};
	class CUP_arifle_AK74_GL: CUP_arifle_AK_Base
	{
		magazines[]=
		{
			"CUP_30Rnd_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
		};
	};
	class CUP_arifle_AKS74: CUP_arifle_AK_Base
	{
		magazines[]=
		{
			"CUP_30Rnd_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
		};
	};
	class CUP_arifle_AKS74U: CUP_arifle_AK_Base
	{
		magazines[]=
		{
			"CUP_30Rnd_545x39_AK_M",
			"CUP_30Rnd_TE1_White_Tracer_545x39_AK_M"
		};
	};
	class arifle_SPAR_01_base_F: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class arifle_SPAR_01_blk_F: arifle_SPAR_01_base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class arifle_SPAR_01_khk_F: arifle_SPAR_01_base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class arifle_SPAR_01_snd_F: arifle_SPAR_01_base_F
	{
		magazines[]=
		{
			"30Rnd_556x45_Stanag",
			"30Rnd_556x45_Stanag_Tracer_Red",
			"FHQ_30Rnd_556x45_Stanag_Tracer_IR"
		};
	};
	class CUP_glaunch_Base: Rifle_Base_F
	{
	};
	class CUP_glaunch_M32: CUP_glaunch_Base
	{
		magazines[]=
		{
			"CUP_6Rnd_HE_M203"
		};
	};
	class CUP_smg_EVO: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_9x21_Mag"
		};
	};
	class pdw2000_base_F: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_9x21_Mag"
		};
	};
	class SMG_02_base_F: Rifle_Base_F
	{
		magazines[]=
		{
			"30Rnd_9x21_Mag"
		};
	};
	class SMG_05_base_F: Rifle_Base_F
	{
	};
	class SMG_05_F: SMG_05_base_F
	{
		magazines[]=
		{
			"30Rnd_9x21_Mag"
		};
	};
	class Launcher_Base_F;
	class CUP_launch_RPG7V: Launcher_Base_F
	{
		magazines[]=
		{
			"CUP_PG7VR_M"
		};
	};
	class ItemCore;
	class InventoryItem_Base_F;
	class VestItem;
	class Vest_Camo_Base: ItemCore
	{
		class ItemInfo;
	};
	class Vest_NoCamo_Base: ItemCore
	{
		class ItemInfo;
	};
	class CUP_acc_sffh: ItemCore
	{
		displayName="L85 Surefire Flash Hider";
	};
	class V_TacVest_khk: Vest_Camo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=8;
					passThrough=0.5;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=8;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=8;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=8;
					passThrough=0.5;
				};
			};
		};
	};
	class V_TacVest_camo: Vest_Camo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=8;
					passThrough=0.5;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=8;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=8;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=8;
					passThrough=0.5;
				};
			};
		};
	};
	class V_RebreatherB: Vest_Camo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=8;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=8;
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=8;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
			};
		};
	};
	class V_TacVestCamo_khk: Vest_Camo_Base
	{
		class ItemInfo: VestItem
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=8;
					passThrough=0.5;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=8;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=8;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=12;
					passThrough=0.40000001;
				};
			};
		};
	};
	class V_TacVestIR_blk: Vest_NoCamo_Base
	{
		class ItemInfo: VestItem
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=8;
					passThrough=0.5;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=8;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=8;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=4;
					passThrough=0.40000001;
				};
			};
		};
	};
	class V_TacVest_blk_POLICE: Vest_Camo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=16;
					passThrough=0.5;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=16;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=16;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=16;
					passThrough=0.40000001;
				};
			};
		};
	};
	class V_Press_F: Vest_Camo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Chest
				{
					hitpointName="HitChest";
					armor=16;
					passThrough=0.5;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=16;
					passThrough=0.5;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=16;
					passThrough=0.5;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.5;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=16;
					passThrough=0.40000001;
				};
			};
		};
	};
	class V_PlateCarrierIA1_dgtl: Vest_NoCamo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=1;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=1;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=24;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=24;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=24;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.44999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=24;
					passThrough=0.1;
				};
			};
		};
	};
	class V_PlateCarrier1_rgr: Vest_NoCamo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=1;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=1;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=24;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=24;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=24;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.44999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=24;
					passThrough=0.1;
				};
			};
		};
	};
	class V_PlateCarrierL_CTRG: V_PlateCarrier1_rgr
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=1;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=1;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=24;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=24;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=24;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.44999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=24;
					passThrough=0.1;
				};
			};
		};
	};
	class V_PlateCarrier1_blk: Vest_Camo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=2;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=2;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=24;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=24;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=24;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.44999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=24;
					passThrough=0.1;
				};
			};
		};
	};
	class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=1;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=1;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=24;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=24;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=24;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.44999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=24;
					passThrough=0.1;
				};
			};
		};
	};
	class V_PlateCarrierH_CTRG: V_PlateCarrier2_rgr
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=4;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=4;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=36;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=36;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=36;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.34999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=36;
					passThrough=0.1;
				};
			};
		};
	};
	class V_PlateCarrierSpec_rgr: Vest_NoCamo_Base
	{
		class ItemInfo: ItemInfo
		{
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=16;
					passThrough=0.5;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=16;
					passThrough=0.5;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=36;
					passThrough=0.1;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=36;
					passThrough=0.1;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=36;
					passThrough=0.1;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=0.34999999;
				};
				class Pelvis
				{
					hitpointName="HitPelvis";
					armor=36;
					passThrough=0.1;
				};
			};
		};
	};
	class H_HelmetB: ItemCore
	{
		class ItemInfo;
	};
};
class CfgWorlds
{
	class DefaultWorld
	{
	};
	class CAWorld: DefaultWorld
	{
	};
	class Altis: CAWorld
	{
		class AmbientA3
		{
			maxCost=0;
		};
		class CfgEnvSounds;
		class EnvSounds: CfgEnvSounds
		{
			class Rain
			{
				name="rain";
				sound[]=
				{
					"A3\sounds_f\ambient\rain\rain_new_1",
					0.1,
					1,
					200
				};
				soundNight[]=
				{
					"A3\sounds_f\ambient\rain\rain_new_2",
					0.090000004,
					1,
					200
				};
				volume="rain";
			};
			class Sea
			{
				name="Sea";
				sound[]=
				{
					"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",
					0.12589253,
					1,
					200
				};
				soundNight[]=
				{
					"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",
					0.1,
					1,
					200
				};
				volume="sea*(1-coast)";
			};
			class Coast
			{
				name="Coast";
				sound[]=
				{
					"A3\sounds_f\ambient\waves\beach_sand_small_waves1",
					0.12589253,
					1,
					200
				};
				soundNight[]=
				{
					"A3\sounds_f\ambient\waves\beach_sand_small_waves1",
					0.079432823,
					1,
					200
				};
				volume="coast";
			};
			class WindSlow
			{
				name="WindSlow";
				sound[]=
				{
					"A3\sounds_f\ambient\winds\wind-synth-slow",
					0.028183825,
					1
				};
				volume="((windy factor[0,0.25])*(windy factor[0.5, 0.25]))-(night*0.25)";
			};
			class WindMedium
			{
				name="WindMedium";
				sound[]=
				{
					"A3\sounds_f\ambient\winds\wind-synth-middle",
					0.035481334,
					1
				};
				volume="((windy factor[0.33,0.5])*(windy factor[0.8, 0.5]))-(night*0.25)";
			};
			class WindFast
			{
				name="WindFast";
				sound[]=
				{
					"A3\sounds_f\ambient\winds\wind-synth-fast",
					0.044668358,
					1
				};
				volume="(windy factor[0.66,1])-(night*0.25)";
			};
			class Forest
			{
				name="Forest";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\day_insects_winds5",
					0.025118863,
					1
				};
				volume="forest*trees*(1-night)";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					12,
					100,
					0.1,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_5",
					0.056234132,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_9",
					0.056234132,
					1,
					80,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_3",
					0.031622775,
					1,
					20,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					40,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					50,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class ForestNight
			{
				name="ForestNight";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\night_insects_birds_winds4",
					0.015848929,
					1
				};
				volume="forest*trees*night";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_6",
					0.056234132,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_2",
					0.031622775,
					1,
					70,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_3",
					0.056234132,
					1,
					10,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					40,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					30,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class Houses
			{
				name="Houses";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\day_insects_winds4",
					0.022387212,
					1
				};
				volume="(houses-0.75)*4";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_7",
					0.056234132,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_8",
					0.031622775,
					1,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_4",
					0.056234132,
					1,
					15,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					25,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class HousesNight
			{
				name="HousesNight";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\night_insects_birds_nowinds2",
					0.019952621,
					1
				};
				volume="(houses-0.75)*4*night";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_2",
					0.056234132,
					1,
					80,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_5",
					0.031622775,
					1,
					80,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_2",
					0.056234132,
					1,
					15,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					15,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					30,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class Meadows
			{
				name="Meadows";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\day_insects_winds5",
					0.022387212,
					1
				};
				volume="(1-forest)*(1-houses)*(1-night)*(1-sea)";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.063095726,
					1,
					70,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.063095726,
					1,
					100,
					0.12,
					15,
					25,
					30
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.063095726,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.063095726,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_7",
					0.056234132,
					1,
					70,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_8",
					0.031622775,
					1,
					70,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_4",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class MeadowsNight
			{
				name="MeadowsNight";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\night_insects_birds_winds1",
					0.015848929,
					1
				};
				volume="(1-forest)*(1-houses)*night*(1-sea)";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_2",
					0.056234132,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_5",
					0.031622775,
					1,
					60,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_2",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
		};
	};
	class Stratis: CAWorld
	{
		class AmbientA3
		{
			maxCost=0;
		};
		class CfgEnvSounds;
		class EnvSounds: CfgEnvSounds
		{
			class Rain
			{
				name="rain";
				sound[]=
				{
					"A3\sounds_f\ambient\rain\rain_new_1",
					0.1,
					1,
					200
				};
				soundNight[]=
				{
					"A3\sounds_f\ambient\rain\rain_new_2",
					0.090000004,
					1,
					200
				};
				volume="rain";
			};
			class Sea
			{
				name="Sea";
				sound[]=
				{
					"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",
					0.12589253,
					1,
					200
				};
				soundNight[]=
				{
					"A3\sounds_f\ambient\waves\sea-1-sand-beach-stereo",
					0.1,
					1,
					200
				};
				volume="sea*(1-coast)";
			};
			class Coast
			{
				name="Coast";
				sound[]=
				{
					"A3\sounds_f\ambient\waves\beach_sand_small_waves1",
					0.12589253,
					1,
					200
				};
				soundNight[]=
				{
					"A3\sounds_f\ambient\waves\beach_sand_small_waves1",
					0.079432823,
					1,
					200
				};
				volume="coast";
			};
			class WindSlow
			{
				name="WindSlow";
				sound[]=
				{
					"A3\sounds_f\ambient\winds\wind-synth-slow",
					0.028183825,
					1
				};
				volume="((windy factor[0,0.25])*(windy factor[0.5, 0.25]))-(night*0.25)";
			};
			class WindMedium
			{
				name="WindMedium";
				sound[]=
				{
					"A3\sounds_f\ambient\winds\wind-synth-middle",
					0.035481334,
					1
				};
				volume="((windy factor[0.33,0.5])*(windy factor[0.8, 0.5]))-(night*0.25)";
			};
			class WindFast
			{
				name="WindFast";
				sound[]=
				{
					"A3\sounds_f\ambient\winds\wind-synth-fast",
					0.044668358,
					1
				};
				volume="(windy factor[0.66,1])-(night*0.25)";
			};
			class Forest
			{
				name="Forest";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\day_insects_winds5",
					0.025118863,
					1
				};
				volume="forest*trees*(1-night)";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					12,
					100,
					0.1,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_5",
					0.056234132,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_9",
					0.056234132,
					1,
					80,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_3",
					0.031622775,
					1,
					20,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					40,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					50,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class ForestNight
			{
				name="ForestNight";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\night_insects_birds_winds4",
					0.015848929,
					1
				};
				volume="forest*trees*night";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_6",
					0.056234132,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_2",
					0.031622775,
					1,
					70,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_3",
					0.056234132,
					1,
					10,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					40,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					30,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class Houses
			{
				name="Houses";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\day_insects_winds4",
					0.022387212,
					1
				};
				volume="(houses-0.75)*4";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_7",
					0.056234132,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_8",
					0.031622775,
					1,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_4",
					0.056234132,
					1,
					15,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					25,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class HousesNight
			{
				name="HousesNight";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\night_insects_birds_nowinds2",
					0.019952621,
					1
				};
				volume="(houses-0.75)*4*night";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_2",
					0.056234132,
					1,
					80,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_5",
					0.031622775,
					1,
					80,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_2",
					0.056234132,
					1,
					15,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					15,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					30,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class Meadows
			{
				name="Meadows";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\day_insects_winds5",
					0.022387212,
					1
				};
				volume="(1-forest)*(1-houses)*(1-night)*(1-sea)";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.063095726,
					1,
					70,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.063095726,
					1,
					100,
					0.12,
					15,
					25,
					30
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.063095726,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.063095726,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_7",
					0.056234132,
					1,
					70,
					0.12,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_8",
					0.031622775,
					1,
					70,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\fly_4",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
			class MeadowsNight
			{
				name="MeadowsNight";
				sound[]=
				{
					"A3\sounds_f\ambient\basics\night_insects_birds_winds1",
					0.015848929,
					1
				};
				volume="(1-forest)*(1-houses)*night*(1-sea)";
				randSamp0[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_01",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp1[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_02",
					0.1,
					1,
					100,
					0.12,
					10,
					35,
					60
				};
				randSamp2[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_03",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp3[]=
				{
					"A3\sounds_f\ambient\single_sfx\meadow_single_04",
					0.1,
					1,
					100,
					0.12,
					10,
					25,
					40
				};
				randSamp4[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_2",
					0.056234132,
					12,
					100,
					0.1,
					10,
					25,
					40
				};
				randSamp5[]=
				{
					"A3\sounds_f\ambient\single_sfx\bird_night_5",
					0.031622775,
					1,
					60,
					0.1,
					10,
					25,
					40
				};
				randSamp6[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_2",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					50
				};
				randSamp7[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_3",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				randSamp8[]=
				{
					"A3\sounds_f\ambient\single_sfx\insect_1",
					0.056234132,
					1,
					20,
					0.1,
					10,
					25,
					40
				};
				random[]=
				{
					"randSamp0",
					"randSamp1",
					"randSamp2",
					"randSamp3",
					"randSamp4",
					"randSamp5",
					"randSamp6",
					"randSamp7",
					"randSamp8"
				};
			};
		};
	};
	class Bozcaada: Stratis
	{
		class AmbientA3
		{
			maxCost=0;
		};
	};
	class Tanoa: Stratis
	{
		class AmbientA3
		{
			maxCost=0;
		};
	};
};
class asdg_MuzzleSlot_762;
class asdg_MuzzleSlot_65: asdg_MuzzleSlot_762
{
	class compatibleItems
	{
		muzzle_snds_h=1;
		muzzle_snds_h_khk_F=1;
		muzzle_snds_h_snd_F=1;
		muzzle_snds_65_TI_blk_F=1;
		muzzle_snds_65_TI_hex_F=1;
		muzzle_snds_65_TI_ghex_F=1;
	};
};
