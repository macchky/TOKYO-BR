/*---------------------------------------------------------------------------
Initialize the damage portion of the HUD, or keyboard event handlers
	related to damage.
---------------------------------------------------------------------------*/
[] spawn {

	// hack to wait for BR UI to appear
	waitUntil {! isNull (uiNamespace getVariable ["br_GUI_status",objNull]) };
	// Toggle HUD
	true call BR_Client_GUI_HUDToggle;
	
	// Handle spectator modes and respawns
	while {true} do 
	{
		sleep 10;
		if ( isNull (uiNamespace getVariable ["br_GUI_status",objNull]) && BR_Client_Damage_HudIsVisible) then 
		{
			sleep 10;
			false call BR_Client_GUI_HUDToggle;
		}
		else
		{
			if ((!isNull (uiNamespace getVariable ["br_GUI_status",objNull])) && 
				(!BR_Client_Damage_HudIsVisible)) then 
			{
				true call BR_Client_GUI_HUDToggle;
			};
		};
	};
};

[] spawn
{
	waituntil {!(IsNull findDisplay 46)};
	(findDisplay 46) displayAddEventHandler ["KeyDown",{_this call BR_Client_GUI_Events_onKeyDown;}];
	//(findDisplay 46) displayAddEventHandler ["MouseButtonDown",{_this call BR_Client_GUI_Events_onMouseButtonDown;}];

	//Do this last.  This is to suppress drawing in map.
	waituntil {!(IsNull findDisplay 12)};
	(findDisplay 12) displayAddEventHandler ["KeyDown",{_this call BR_Client_GUI_Events_Map_onKeyDown;}];
};

[] spawn 
{
	waitUntil {!isNil "br_deploy_players"};
	waitUntil {br_deploy_players};
	[] spawn BR_Client_GUI_Notifications_MonitorRemainingPlayers;
};

[] spawn
{
	waitUntil {br_game_started};
	br_teamAllowTeamChanges = false;
};
true