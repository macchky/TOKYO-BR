/*---------------------------------------------------------------------------
Stormridge: Code that sets the mission name on the in-game Diary (menu pulled up using right-ctrl + P)

Also monitors for map screen displayed.
---------------------------------------------------------------------------*/
[_this select 0] spawn
{

	private ["_ctrl", "_message", "_playerCount", "_display","_previousCount"];
	disableSerialization;
	_message="";
	_previousCount=-1;
	_ctrl = _this select 0;

	while {true} do
	{
		//####################################################
		// Player List
		//####################################################
		sleep 1;	// sleep here to allow BI's RscDiary.sqf to set the title, then we'll change it.
		_playerCount = {((alive _x) && (str(side _x) != 'CIV'))} count allPlayers;
		if !(_playerCount isEqualTo _previousCount) then
		{
			if (_playerCount == 1) then
			{
				_message = "   1 PLAYER REMAINING!";
			} else {
				_message = format["   %1 PLAYERS REMAINING!", _playerCount];
			};
			_ctrl ctrlSetText _message;
			_previousCount = _playerCount;
		};
		if (ctrlIDD (ctrlParent _ctrl) == 129) exitWith {};	// 129 = in-game menu.  12 = map overlay

		//####################################################
		// Map Hooks
		//####################################################
		if (visibleMap && !br_mapVisibleMapHooked) then 
		{
			br_mapVisibleMapHooked = true;
			[] spawn
			{
				call BR_Client_GUI_Map_onMapShown;
				waitUntil {!visibleMap};
				br_mapVisibleMapHooked=false;
				call BR_Client_GUI_Map_onMapClosed;
			};
		};
	};
};