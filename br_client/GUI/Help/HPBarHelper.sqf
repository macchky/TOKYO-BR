/*---------------------------------------------------------------------------
STRM: HP Bar help text
---------------------------------------------------------------------------*/
disableSerialization;
waitUntil {! (isnull (uiNamespace getVariable ["BR_GUI_status", objNull]))};
if ((profileNamespace getVariable ["HPBarInitializedPositionV1", -1]) < 1) then {
	diag_log "User has NOT initialized HP Bar yet";
	1 cutText ["", "PLAIN"];
	profilenamespace setvariable ["IGUI_GRID_AVCAMERA_X", 1];
	profilenamespace setvariable ["IGUI_GRID_AVCAMERA_Y", 1];
	1 cutRsc ["br_RscDisplayPlayerStatus","PLAIN"];
	profileNamespace setVariable ["HPBarInitializedPositionV1", 1];
	systemChat "Please Note:  You can move your HP bar by going into Configure > Game > Layout, and adjusting the 'AV Camera' position";
	systemChat "The change will take effect in the next game";
} else {
	diag_log "User has already initialized the HB Bar";
};
true 