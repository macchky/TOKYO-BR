/*---------------------------------------------------------------------------
Checks a group of hit parts and displays the HUD icons.
If one hitpart is damaged, then it will display red.
---------------------------------------------------------------------------*/
private ["_thisPartDamaged", "_hitPartValueArray", "_hitPartDisplayNumber", "_hitPartControl", "_highestDamageValue", "_thisHitBoxDamage", "_fadeValue"];
_hitPartValueArray = _this select 1;
_hitPartDisplayNumber = BR_Client_Damage_DisplayHitPartNumberLookup select ((_this select 0) select 0);
_hitPartControl = (_this select 2) displayCtrl (2500 + _hitPartDisplayNumber);
_thisPartDamaged=false;
_highestDamageValue=0;
_thisHitBoxDamage=0;

{
	//if ((_hitPartValueArray select _x) >= .1) exitWith{_thisPartDamaged=true}; 	// Legs damaged to .5 will force walking
	
	_thisHitBoxDamage = _hitPartValueArray select _x;

	if (_thisHitBoxDamage > _highestDamageValue) then 
	{
		_highestDamageValue = _thisHitBoxDamage;
	};
	if (_thisHitBoxDamage > .1) then 
	{
		_thisPartDamaged=true;
	};

} foreach (_this select 0);

if (_thisPartDamaged) then
{
	_fadeValue=0;
	if (_highestDamageValue > .65) then {
		_fadeValue=0.1;
	} else {
		if (_highestDamageValue > .35) then {
			_fadeValue=.4;
		} else {
			_fadeValue = .7;
		};
	};
	_hitPartControl ctrlSetText (BR_Client_Damage_DisplayHitPart_ImagePathBase + str(_hitPartDisplayNumber) + "_Red.paa");
	_hitPartControl ctrlSetFade _fadeValue;
	_hitPartControl ctrlCommit .5;
}
else
{
	_hitPartControl ctrlSetText (BR_Client_Damage_DisplayHitPart_ImagePathBase + str(_hitPartDisplayNumber) + "_White.paa"); 
	_hitPartControl ctrlSetFade 0;
	_hitPartControl ctrlCommit .5;	
};