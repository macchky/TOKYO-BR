/*---------------------------------------------------------------------------
STRM: Display rules to user.
---------------------------------------------------------------------------*/
waitUntil {!isNull findDisplay 46};
waitUntil {br_ClientSidePlayerInitComplete};
if (br_solo) then {[br_gameRulesMessageText] spawn BIS_fnc_guiMessage;};
diag_log "[CLIENT] SOLO UI loaded";
// private _rulesDisplayCounter = profileNamespace getVariable ["BR_rulesDisplayCounter", 0];
// _rulesDisplayCounter = _rulesDisplayCounter + 1;
// if (_rulesDisplayCounter > 5) then {
//	_rulesDisplayCounter=0;
createDialog "RscBRRulesMessageBox";
waitUntil {!isNull findDisplay 32200};
disableSerialization;
private _ctrlText = ((findDisplay 32200) displayCtrl 1100);
private _ctrlButtonOK = ((findDisplay 32200) displayCtrl 1600);
private _ctrlTextHeightOriginal = (ctrlPosition _ctrlText) select 3;
_ctrlText ctrlSetStructuredText br_rulesMessage;
private _ctrlTextHeightNew = ctrlTextHeight _ctrlText;
private _ctrlTextPosOriginal = ctrlPosition _ctrlText;
private _ctrlTextPosNew = [_ctrlTextPosOriginal select 0,
					_ctrlTextPosOriginal select 1,
					_ctrlTextPosOriginal select 2,
					(_ctrlTextPosOriginal select 3) + (_ctrlTextHeightNew - _ctrlTextHeightOriginal)];
_ctrlText ctrlSetPosition _ctrlTextPosNew;
_ctrlText ctrlCommit 0;
setMousePosition [0.5, 0.5];
//};
//profileNamespace setVariable ["BR_rulesDisplayCounter", _rulesDisplayCounter];