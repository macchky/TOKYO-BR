/*---------------------------------------------------------------------------
News feed displayed during pause
---------------------------------------------------------------------------*/
if !(uinamespace getvariable ["BR_GUI_MenuNewsFeed_disable",false]) then {

	private ["_display", "_ctrl", "_build", "_isDemo", "_newsOnline", "_newsOffline", "_htmlLoaded", "_serverNameCtrl"];

	_display = _this select 0;
	_ctrl = _display displayCtrl 24201;

	_serverNameCtrl = _display displayCtrl 24202;
	_serverNameCtrl ctrlSetText serverName;

	if (player getVariable ["IsSpectator",false] || br_isThisPlayerSpectator) then
	{
		_ctrl htmlLoad "br_client\GUI\Help\SpectatorHelp.html";
	}
	else
	{
		//Determine which version of the game is running
		_build = "";
		_isDemo = getNumber (configFile >> "isDemo");

		if (_isDemo == 1) then 
		{
			switch (productVersion select 1) do 
			{
				case "Arma3": {_build = "beta";};
			};
		} 
		else 
		{
			//Linux & Mac port switch (if a port platform is not specifically detected, default to primary Windows)
			if ((toLower (productVersion select 6)) in ["linux", "osx"]) then 
			{
				_build = "port";
			} 
			else
			{
				if ((configName (configFile >> "CfgPatches" >> "A3_Missions_F_EPC")) != "") then 
				{	
					_build = "main";
				} 
				else 
				{
					_build = "trial";
				};
			};
		};

		// Replace with BR leaderboard link, when available.
		_newsOnline = "http://www.bistudio.com/newsfeed/arma3_news.php?build=" + _build + "&language=" + language;
		_newsOffline = "a3\Ui_f\data\news.html";

		//_ctrl htmlLoad _newsOnline;
		_ctrl htmlLoad br_newsFeedURL;	// Defined by ClientSidePlayerInit.sqf, delivered by the server mission file

		_htmlLoaded = ctrlHTMLLoaded _ctrl;
		
		if (!_htmlLoaded) then {
			_ctrl htmlLoad _newsOffline;
			//uinamespace setvariable ["BR_GUI_MenuNewsFeed_disable",true];  // We're not going to use this now, since we're pulling HTML locally.
		};

		_htmlLoaded
	};
} else {
	false
};