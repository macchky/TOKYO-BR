/*---------------------------------------------------------------------------
Some legacy code to do HP and Boost bar
---------------------------------------------------------------------------*/
waitUntil{br_Boost > 0};

private ["_totalHealth","_percent","_totalDmg"];

_ctrlHP = 1101;
_ctrlThirst = 1201;
disableserialization;
_display = uiNamespace getVariable ["br_GUI_status",displayNull];

if(isNull _display) then {
    1 cutRsc ["br_RscDisplayPlayerStatus","PLAIN"];
    _display = uiNamespace getVariable ["br_GUI_status",displayNull];
};

_ctrl = _display displayCtrl _ctrlHP;
_pos = ctrlPosition _ctrl;
_HPOrigWidth = _pos select 2;
_ctrl = _display displayCtrl _ctrlThirst;
_pos = ctrlPosition _ctrl;
_boostOrigWidth = _pos select 2;

while{true} do {
	_dmg = damage player;
	_boost = br_Boost;
	waitUntil{damage player != _dmg || br_Boost != _boost || isNull _display || !(alive player)};
	
	if(isNull _display) then {
		1 cutRsc ["br_RscDisplayPlayerStatus","PLAIN"];
		_display = uiNamespace getVariable ["br_GUI_status",displayNull];
		
		_percent = 1 - (damage player);
		_ctrl = _display displayCtrl _ctrlHP;
		_pos = ctrlPosition _ctrl;
		_newPos = [_pos select 0,_pos select 1,_HPOrigWidth*_percent,_pos select 3];
		_ctrl ctrlSetPosition _newPos;
		_ctrl ctrlCommit 0;
		
		_percent = br_Boost/100;

		_ctrl = _display displayCtrl _ctrlThirst;
		_pos = ctrlPosition _ctrl;
		_newPos = [_pos select 0,_pos select 1,_boostOrigWidth*_percent,_pos select 3];
		_ctrl ctrlSetPosition _newPos;
		_ctrl ctrlCommit 0;
	} else {
		
		_percent = 1 - (damage player);
		_ctrl = _display displayCtrl _ctrlHP;
		_pos = ctrlPosition _ctrl;
		_newPos = [_pos select 0,_pos select 1,_HPOrigWidth*_percent,_pos select 3];
		_ctrl ctrlSetPosition _newPos;
		_ctrl ctrlCommit 1;

		_percent = br_Boost/100;

		_ctrl = _display displayCtrl _ctrlThirst;
		_pos = ctrlPosition _ctrl;
		_newPos = [_pos select 0,_pos select 1,_boostOrigWidth*_percent,_pos select 3];
		_ctrl ctrlSetPosition _newPos;
		_ctrl ctrlCommit 1;
	};
	
	if !(alive player) exitWith {
		// Give spectators and regular players a few moments to stare at their paper doll
		uisleep 10;
		1 cutText ["","PLAIN"];
	};
};
