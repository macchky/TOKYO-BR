/*---------------------------------------------------------------------------
STRM: Repopulate invite list.
---------------------------------------------------------------------------*/
disableSerialization;
private _display = _this;
if !(_display isEqualTo displayNull) then 
{
	// Create visible Team Member List.  If teammates die, their player objects
	//	get lost due to NetId changes.  So, names can't be resolved by other clients.
	private _visibleInviteList=[];
	{
		private _memberObject = objectFromNetId (_x select 1);
		if (!isNull _memberObject && isPlayer _memberObject && alive _memberObject) then { _visibleInviteList pushBack _x; };
	} foreach br_teamInvites;

	private _ctrl = _display displayCtrl 7210;
	lbClear _ctrl;
	{
		_index = _ctrl lbAdd (name (objectFromNetId (_x select 1)));
		_ctrl lbSetData [_index, (_x select 0)];
	} foreach _visibleInviteList;

	if (count _visibleInviteList > 0) then 
	{
		_display displayCtrl 7209 ctrlSetBackgroundColor br_teamGUICtrlBGColorRed;
	}
	else
	{
		_display displayCtrl 7209 ctrlSetBackgroundColor br_teamGUICtrlBGColor;
	};
};
true