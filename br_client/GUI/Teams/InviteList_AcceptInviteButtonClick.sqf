/*---------------------------------------------------------------------------
STRM: Accepts an invite
---------------------------------------------------------------------------*/
disableSerialization;

private ["_ctrl", "_from", "_teamID", "_inviteListbox", "_display", "_selectedIndex"];

if (!br_teamAllowTeamChanges) exitWith { systemChat "Team edits cannot be made after the game has started."};

_ctrl = _this select 0;
_display = ctrlParent _ctrl;
_inviteListbox = _display displayCtrl 7210;
_selectedIndex = lbCurSel _inviteListbox;

// If you only have 1 invite, don't force user to highlight anything...
if (count br_teamInvites == 1) then { _selectedIndex = 0;};

if (_selectedIndex < 0) exitWith {};

_teamID = _inviteListbox lbData _selectedIndex;

diag_log format["[Team::PlayerList_AcceptInviteButtonClick] Display calculated as %1", _display];
diag_log format["[Team::PlayerList_AcceptInviteButtonClick] _selectedIndex as %1", _selectedIndex];
diag_log format["[Team::PlayerList_AcceptInviteButtonClick] _teamID as %1", _teamID];

// Protect against spam
if ((diag_tickTime - br_teamGUIButtonInviteLastPressed) > 2) then
{
	// Remove the invite from UI and from local array
	_inviteListbox lbDelete _selectedIndex;
	{
		if ((_x select 0) isEqualTo _teamID) exitWith {br_teamInvites deleteAt _forEachIndex};
	} foreach br_teamInvites;  // [teamID, netID Player who invited]

	_display displayCtrl 7209 ctrlSetBackgroundColor br_teamGUICtrlBGColor;

	br_teamGUIButtonInviteLastPressed = diag_tickTime;
	["Client_Teams_AcceptInviteRequest", [_teamID]] call BR_Client_Network_SendMessage;
	systemChat format[">>> Attempting to join team..."];
};

true