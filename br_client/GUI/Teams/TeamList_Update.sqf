/*---------------------------------------------------------------------------
STRM: Repopulate team list.
---------------------------------------------------------------------------*/
disableSerialization;
private _display = _this;
if !(_display isEqualTo displayNull) then 
{
	// Create visible Team Member List.  If teammates die, their player objects
	//	get lost due to NetId changes.  So, names can't be resolved by other clients.
	private _visibleMemberListNetIDs=[];
	{
		private _memberObject = objectFromNetId _x;
		if (!isNull _memberObject && isPlayer _memberObject && alive _memberObject) then { _visibleMemberListNetIDs pushBack _x; };
	} foreach br_teamMembers;

	private _ctrl = _display displayCtrl 7203;
	lbClear _ctrl;
	{
		_index = _ctrl lbAdd (name (objectFromNetId _x));
		_ctrl lbSetData [_index, _x];
	} foreach _visibleMemberListNetIDs;

	// if (br_inTeam) then
	// {
	// 	// Fix this later.
	// 	private _teamCount = 0;
	// 	{
	// 		if (alive _x &&
	// 			isPlayer _x &&
	// 			side _x != civilian &&
	// 			!(_x getVariable["IsSpectator",false]) && 
	// 			!(_x getVariable["IsTeamSpectator",false])) then {_teamCount=_teamCount+1;};
	// 	} foreach br_teamMembers;
	// 	_display displayCtrl 7205 ctrlSetText format["Your Team (%1)", count br_teamMembers];
	// } 
	// else
	// {
	//	_display displayCtrl 7205 ctrlSetText "Your Team";
	//};
};
true 