/*---------------------------------------------------------------------------
STRM: Adds invite to queue
---------------------------------------------------------------------------*/
disableSerialization;
params ["_teamID", "_senderNetID"];
private _senderName = name (objectFromNetId _senderNetID);
if (!br_teamAllowTeamChanges) exitWith { systemChat "Team edits cannot be made after the game has started."};

try {
	// Do I already have an invite for this team?
	{
		if ((_x select 0) isEqualTo _teamID) throw format[">>> You received a duplicate team invite from %1", name (objectFromNetId (_x select 1))];
	} foreach br_teamInvites;

	// Do I already have an invite from this player?
	{
		if (_senderNetID isEqualTo (_x select 1)) exitWith { br_teamInvites deleteAt _forEachIndex };
	} foreach br_teamInvites;

	// Do I have too many invites?
	if ((count br_teamInvites) > br_teamInvitesMaxAllowed) throw format[">>> You received an invite from %1 but you have too many pending invites.", name (objectFromNetId _senderNetID)];

	// OK - add it.		
	br_teamInvites pushBackUnique _this;
	systemChat format[">>> You have received a Team invite from %1", _senderName];
	systemChat "	 To mute invite notifications, use the Team Menu.";

	if (!(profileNamespace getVariable["teamMuteInviteNotification", false])) then
	{
		["TeamInviteReceived",["","Press U to see team invites."]] call bis_fnc_showNotification;
	};

	// Update GUI list
	if (br_teamGUIMenuIsVisible) then 
	{
		call BR_Client_GUI_Teams_InviteList_UpdateIfVisible;
	}
	else
	{
		systemChat ">>> Open the team menu with your TeamSwitch key. (U is default)";
	};
}
catch
{
	systemChat _exception;
};
true