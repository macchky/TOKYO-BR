/*---------------------------------------------------------------------------
STRM: Cheesy event hookups
---------------------------------------------------------------------------*/
diag_log format["[Team::InitializeMenu] parameter _this = %1",_this];
private ["_display", "_ctrl", "_index", "_teamID"];

_display = _this select 0;

disableSerialization;

// ##################################
// Populate  player list
// ##################################
_display call BR_Client_GUI_Teams_PlayerList_Update;

//_display displayCtrl 7204 ctrlSetEventHandler ["LBSelChanged", "_this call BR_Client_GUI_Teams_PlayerList_onSelChanged"]; 

// Wire invite button
_display displayCtrl 7202 ctrlSetEventHandler ["ButtonClick", "_this call BR_Client_GUI_Teams_PlayerList_InviteButtonClick"]; 

// Wire refresh button
_display displayCtrl 7220 ctrlSetEventHandler ["MouseButtonDown", "_this call BR_Client_GUI_Teams_PlayerList_UpdateIfVisible"]; 

// ##################################
// Populate Invite List
// ##################################
_display call BR_Client_GUI_Teams_InviteList_Update;

_display displayCtrl 7211 ctrlSetEventHandler ["ButtonClick", "_this call BR_Client_GUI_Teams_InviteList_AcceptInviteButtonClick"]; 

// ##################################
// Populate Team Members List
// ##################################
_display call BR_Client_GUI_Teams_TeamList_Update;

_display displayCtrl 7206 ctrlSetEventHandler ["ButtonClick", "_this call BR_Client_GUI_Teams_TeamList_RemovePlayer"]; 
_display displayCtrl 7208 ctrlSetEventHandler ["ButtonClick", "_this call BR_Client_GUI_Teams_TeamList_LeaveTeam"]; 

// ##################################
// Populate Mute Checkbox
// ##################################
_display displayCtrl 7221 cbSetChecked (profileNamespace getVariable["teamMuteInviteNotification", false]);
_display displayCtrl 7221 ctrlSetEventHandler ["CheckedChanged", "_this call BR_Client_GUI_Teams_Mute_InviteMuteChanged"];

true 