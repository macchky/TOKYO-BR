/*---------------------------------------------------------------------------
STRM: Refresh player list control.
---------------------------------------------------------------------------*/
disableSerialization;
private _display = _this;
if !(_display isEqualTo displayNull) then 
{
	private _ctrl = _display displayCtrl 7204;
	lbClear _ctrl;
	{
		if (player != _x) then 
		{
			_index = _ctrl lbAdd (name _x);
			_ctrl lbSetData [_index, (netId _x)];
		};
	} forEach allPlayers;
	_ctrl lbSetCurSel -1;
	lbSort _ctrl;
};
true