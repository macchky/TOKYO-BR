/*---------------------------------------------------------------------------
STRM: If GUI is visible, will repopulate invite list.
---------------------------------------------------------------------------*/
disableSerialization;
if (br_teamGUIMenuIsVisible) then 
{
	private _display = findDisplay 7200;
	if !(_display isEqualTo displayNull) then 
	{
		_display call BR_Client_GUI_Teams_InviteList_Update;
	};
};
true