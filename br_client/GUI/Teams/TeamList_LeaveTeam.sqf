/*---------------------------------------------------------------------------
STRM: Bye
---------------------------------------------------------------------------*/
disableSerialization;

if (!br_teamAllowTeamChanges) exitWith { systemChat "Team edits cannot be made after the game has started."};

if ((diag_tickTime - br_teamGUIButtonInviteLastPressed) > 2) then
{
	br_teamGUIButtonInviteLastPressed = diag_tickTime;

	private _netIDPlayerToKick = netId player;

	diag_log format["[Team::TeamList_LeaveTeam] _netIDPlayerToKick as %1", _netIDPlayerToKick];

	if (br_inTeam) then {
		systemChat format[">>> Leaving team..."];
	};

	// We're still going to ask the server, just in case of client/server mismatch.
	["Client_Teams_PlayerRequestRemovePlayerFromTeam", [_netIDPlayerToKick]] call BR_Client_Network_SendMessage;
};

true