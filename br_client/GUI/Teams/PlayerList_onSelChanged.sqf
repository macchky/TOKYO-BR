/*---------------------------------------------------------------------------
STRM: Player selection changed.
---------------------------------------------------------------------------*/
disableSerialization;
diag_log format["[Team::PlayerList_onSelChanged] parameter _this = %1",_this];
params ["_ctrl", "_index"];