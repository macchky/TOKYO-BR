/*---------------------------------------------------------------------------
STRM: Displays teammates
---------------------------------------------------------------------------*/
private ["_teamMember", "_ctrl", "_color", "_iScale", "_name"];
_ctrl = _this select 0;
_iScale = ((1 - ctrlMapScale _ctrl) max .2) * 30;
_color = [0, 0, .2, .7];
{
	_teamMember = objectFromNetId _x;
	
	if (!isNull _teamMember) then 
	{
		if (alive _teamMember && (str(side _teamMember) != "CIV") && (getPlayerUID _teamMember != "")) then
		{
			_name = name _teamMember;
			if (_teamMember isEqualTo player) then {_name = "YOU"};
			_ctrl drawIcon [br_mapIconPlayer, _color, getPosATL _teamMember, _iScale, _iScale, getDir _teamMember, _name, 1];
		};
	};
} foreach br_teamMembers;