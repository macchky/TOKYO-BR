/*---------------------------------------------------------------------------
STRM: Player has touched the mute invite checkbox
---------------------------------------------------------------------------*/
params ["_ctrl", "_ctrlCheckedInt"];
if (_ctrlCheckedInt == 0) then
{
	profileNamespace setVariable ["teamMuteInviteNotification", false];
}
else
{
	profileNamespace setVariable ["teamMuteInviteNotification", true];
};
true 