/*---------------------------------------------------------------------------
STRM: Kick
---------------------------------------------------------------------------*/
disableSerialization;

private ["_ctrl", "_from", "_teamID", "_teamListbox", "_display", "_selectedIndex", "_netIDPlayerToKick"];

if (!br_teamAllowTeamChanges) exitWith { systemChat "Team edits cannot be made after the game has started."};

_ctrl = _this select 0;
_display = ctrlParent _ctrl;
_teamListbox = _display displayCtrl 7203;
_selectedIndex = lbCurSel _teamListbox;

if (_selectedIndex < 0) exitWith {};

// Protect against spam
if ((diag_tickTime - br_teamGUIButtonInviteLastPressed) > 2) then
{
	private _netIDPlayerToKick = _teamListbox lbData _selectedIndex;
	
	diag_log format["[Team::TeamList_RemovePlayer] Display calculated as %1", _display];
	diag_log format["[Team::TeamList_RemovePlayer] _selectedIndex as %1", _selectedIndex];
	diag_log format["[Team::TeamList_RemovePlayer] _netIDPlayerToKick as %1", _netIDPlayerToKick];

	_teamListbox lbDelete _selectedIndex;

	br_teamGUIButtonInviteLastPressed = diag_tickTime;
	["Client_Teams_PlayerRequestRemovePlayerFromTeam", [_netIDPlayerToKick]] call BR_Client_Network_SendMessage;
	systemChat format[">>> Removing player from team..."];
};

true