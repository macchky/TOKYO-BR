/*---------------------------------------------------------------------------
STRM: Toggle zee blur
---------------------------------------------------------------------------*/
if (br_teamGUIMenuBGBlur isEqualTo objNull) then
{
	br_teamGUIMenuBGBlur = ppEffectCreate ["DynamicBlur", 415];
};
if (_this) then
{
	br_teamGUIMenuBGBlur ppEffectAdjust [1.5];
	br_teamGUIMenuBGBlur ppEffectCommit 1;
	br_teamGUIMenuBGBlur ppEffectEnable true;
}
else
{
	br_teamGUIMenuBGBlur ppEffectAdjust [0];
	br_teamGUIMenuBGBlur ppEffectCommit .5;
	br_teamGUIMenuBGBlur ppEffectEnable false;
};