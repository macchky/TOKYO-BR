/*---------------------------------------------------------------------------
STRM: Invite button was clicked.
---------------------------------------------------------------------------*/
disableSerialization;

private ["_ctrl", "_from", "_toNetID", "_playerListbox", "_display", "_selectedIndex"];

if (!br_teamAllowTeamChanges) exitWith { systemChat "Team edits cannot be made after the game has started."};

_ctrl = _this select 0;
_display = ctrlParent _ctrl;
_playerListbox = _display displayCtrl 7204;
_selectedIndex = lbCurSel _playerListbox;
_toNetID = _playerListbox lbData _selectedIndex;

diag_log format["[Team::PlayerList_InviteButtonClick] Display calculated as %1", _display];
diag_log format["[Team::PlayerList_InviteButtonClick] _selectedIndex as %1", _selectedIndex];
diag_log format["[Team::PlayerList_InviteButtonClick] _toNetID as %1", _toNetID];

if (_selectedIndex > -1) then 
{
	// Protect against spam
	if ((diag_tickTime - br_teamGUIButtonInviteLastPressed) > 1) then
	{
		br_teamGUIButtonInviteLastPressed = diag_tickTime;

		if ((objectFromNetId _toNetID) isEqualTo "") exitWith
		{
			systemChat "Invalid player.";
		};

		if (_toNetID in br_teamMembers) exitWith 
		{
			systemChat "That person is already on your team.";
		};

		["Client_Teams_InvitePlayerToTeamRequest", [_toNetID]] call BR_Client_Network_SendMessage;
		systemChat format[">>> Invite request sent.", name (objectFromNetId _toNetID)];
	};
};
true