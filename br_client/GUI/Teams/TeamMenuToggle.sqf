/*---------------------------------------------------------------------------
STRM: Team Menu system entry point
---------------------------------------------------------------------------*/
disableSerialization;
private _displayStatus = uiNamespace getVariable ['RscBRTeamManager', displayNull];
if (_displayStatus isEqualTo displayNull) then 
{
	private _success = createDialog "RscBRTeamManager";
	if (_success) then 
	{
		br_teamGUIMenuIsVisible = true;
		setMousePosition [0.5, 0.5];
	};
}
else
{
	closeDialog 7200;
	
	br_teamGUIMenuIsVisible = false;
};
true