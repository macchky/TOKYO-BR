/*---------------------------------------------------------------------------
STRM:  Cheesy key event handler.
---------------------------------------------------------------------------*/
params ["_control","_keycode","_isShift","_isCtrl","_isAlt"];
private _haltPropagation=false;

if (_keycode in ActionKeys "User2") exitWith { [] spawn BR_Client_GUI_Skins_ToggleSkinMenu; true };

if (_keycode in ActionKeys "User3") exitWith { [] spawn BR_Client_Game_Effects_MuteSound; false };

if (_keycode in ActionKeys "User4") exitWith { [] spawn BR_Client_Player_Boost_AttemptBoostAction; true };

if (_keycode in ActionKeys "User5") exitWith { [] spawn BR_Client_Player_MagRepack_RepackMagazines; true};

if (inputAction "getOver" > 0) exitWith { call BR_Client_Player_Actions_Jump; false};

if (_keycode in ActionKeys "TeamSwitch") exitWith 
{
	if (br_public_teamingEnabled) then 
	{
		[] spawn BR_Client_GUI_Teams_TeamMenuToggle;
	};
	true
};

if (_keycode isEqualTo 203 && player getVariable ["IsTeamSpectator", false]) then { [0,false] call BR_Client_Player_TeamSpectator_ViewNextTeamMember; false };
if (_keycode isEqualTo 205 && player getVariable ["IsTeamSpectator", false]) then { [1,false] call BR_Client_Player_TeamSpectator_ViewNextTeamMember; false };

_haltPropagation