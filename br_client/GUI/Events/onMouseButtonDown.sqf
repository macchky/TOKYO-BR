/*---------------------------------------------------------------------------
STRM: When mouse button depressed.
---------------------------------------------------------------------------*/
params ["_control","_keycode","_isShift","_isCtrl","_isAlt"];
_halt=false;
if (_keycode == 1) then {
	if (str(side player) isEqualTo "CIV") then {
		_halt=true;
	};
};
_halt