/*---------------------------------------------------------------------------
STRM: Don't draw on the map!
---------------------------------------------------------------------------*/
params ["_control","_keycode","_isShift","_isCtrl","_isAlt"];
private _haltPropagation=false;
switch (_keyCode) do 
{
	case 0x1D:
	{
		if (currentChannel in [0,1,2,5]) then {_haltPropagation=true;};
	};
};
_haltPropagation