/*---------------------------------------------------------------------------
Toggles the Damage HUD ONLY
---------------------------------------------------------------------------*/
if (_this) then
{
	// https://community.bistudio.com/wiki/cutRsc
	// "All 'cut' commands are in the same layer, the same as all 'title' commands are in another one." So to remove cutRsc resource execute cutText on the same layer:
	diag_log "Damage HUD Enabled";
	("RscBRDamageHudLayer" call BIS_fnc_rscLayer) cutRsc ["RscBRDamageHud", "PLAIN", 1, false];
	BR_Client_Damage_HudEventHandler = addMissionEventHandler ["Draw3D", {_this call BR_Client_GUI_HUDEventOnDraw3D;}];
	BR_Client_Damage_HudIsVisible = true;
}
else
{
	diag_log "Damage HUD Disabled";
	("RscBRDamageHudLayer" call BIS_fnc_rscLayer) cutText ["", "PLAIN"];
	BR_Client_Damage_HudIsVisible = false;
	removeMissionEventHandler ["Draw3D", BR_Client_Damage_HudEventHandler];
	BR_Client_Damage_HudEventHandler=objNull;
};
true