private ["_textures","_cfg","_name","_texture","_i"];
_textures = [];
_cfg = configFile >> "CfgSkins";
for "_i" from 0 to count(_cfg)-1 do {
	if(isClass (_cfg select _i)) then {
		_name = getText ((_cfg select _i) >> "name");
		_texture = getText((_cfg select _i) >> "texture");
		_textures pushBack [_name,_texture];
	};
};
_textures