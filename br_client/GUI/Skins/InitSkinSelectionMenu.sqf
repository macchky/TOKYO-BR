/*---------------------------------------------------------------------------
Old code to show skin menu
---------------------------------------------------------------------------*/
disableserialization;
_display = _this select 0;

_listbox = _display displayCtrl 1500;
_textures = call BR_Client_GUI_Skins_GetTextures;
{
	_name = _x select 0;
	_listbox lbAdd _name;
} forEach _textures;

_listbox ctrlAddEventHandler ["lbSelChanged",{
	disableserialization;
	_data = (_this select 0) lbText (_this select 1);
	[_data, false] call BR_Client_GUI_Skins_ApplySkin;
}];

_apply = _display displayCtrl 1600;
_apply buttonSetAction "[(((findDisplay 1339) displayCtrl 1500) lbText (lbCurSel ((findDisplay 1339) displayCtrl 1500))), true] call BR_Client_GUI_Skins_ApplySkin;closeDialog 0;";

_close = _display displayCtrl 1601;
_close buttonSetAction "closeDialog 0;";