if(toLower(uniform player) != getText(configFile >> "CfgSkins" >> "uniform")) exitWith {};
if(br_skinsCurrentSkin == "") then {
	player setObjectTexture [0,getText(((configFile >> "CfgSkins") select 1) >> "texture")];
} else {
	[br_skinsCurrentSkin, false] call BR_Client_GUI_Skins_ApplySkin;
};
true 