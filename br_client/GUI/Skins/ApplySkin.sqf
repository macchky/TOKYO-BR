/*---------------------------------------------------------------------------
Old skin selection code
---------------------------------------------------------------------------*/
params ["_name", "_global"];

// Rangemaster?
if(toLower(uniform player) != getText(configFile >> "CfgSkins" >> "uniform")) exitWith {};

private _texturePath = "a3\characters_f_kart\civil\data\c_poloshirtpants_2_co.paa";
private _playerObject = player;

if(_name isEqualTo "") exitWith {hint 'You must select a skin to apply!';};

br_skinsCurrentSkin = _name;

private _availableSkins = call BR_Client_GUI_Skins_GetTextures;

{
	if((_x select 0) == _name) exitWith { _texturePath = (_x select 1) };
} forEach _availableSkins;

if (_global) then
{
	_playerObject setObjectTextureGlobal [0,_texturePath];
} 
else
{
	_playerObject setObjectTexture [0,_texturePath];
};
true 