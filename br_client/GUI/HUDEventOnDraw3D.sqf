/*---------------------------------------------------------------------------
Paints Damage info on the HUD
---------------------------------------------------------------------------*/
if (BR_Client_Damage_HudIsVisible) then 
{
	private ["_hudDisplay", "_health","_boost", "_isHitPointDamage", "_hitPointDamageArray", "_hitPartControl", "_hitPartDisplayNumber"];
	disableSerialization;
	if (diag_tickTime - BR_Client_Damage_HudTicktimeRenderedAt > .7) then
	{
		_hudDisplay = uiNamespace getVariable ["RscBRDamageHud",objNull];
		if !(isNull _hudDisplay) then 
		{
			_health = BR_Client_Damage_PlayerAttributes select 0;
			_boost = BR_Client_Damage_PlayerAttributes select 1;
			_isHitPointDamage = BR_Client_Damage_PlayerAttributes select 2;
			_hitPointDamageArray = BR_Client_Damage_PlayerAttributes select 3;
			if ((count _hitPointDamageArray > 0) && (_isHitPointDamage == 1)) then
			{
				[BR_Client_Damage_DisplayHitParts_Head, _hitPointDamageArray select 2, _hudDisplay] call BR_Client_GUI_HUDCheckHitPartsAndDisplay;
				[BR_Client_Damage_DisplayHitParts_Chest, _hitPointDamageArray select 2, _hudDisplay] call BR_Client_GUI_HUDCheckHitPartsAndDisplay;
				[BR_Client_Damage_DisplayHitParts_Arms, _hitPointDamageArray select 2, _hudDisplay] call BR_Client_GUI_HUDCheckHitPartsAndDisplay;
				[BR_Client_Damage_DisplayHitParts_Legs, _hitPointDamageArray select 2, _hudDisplay] call BR_Client_GUI_HUDCheckHitPartsAndDisplay;
			}
			else
			{
				{
					_tempCtrl = _hudDisplay displayCtrl _x;
					_tempCtrl ctrlSetFade 1;
					_tempCtrl ctrlCommit .5;
				} forEach [2500,2503,2508,2510];
			};
		};
		// Last line
		BR_Client_Damage_HudTicktimeRenderedAt=diag_tickTime;
	};
};
