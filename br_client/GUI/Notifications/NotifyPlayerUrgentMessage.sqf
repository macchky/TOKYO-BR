/*---------------------------------------------------------------------------
STRM: Message that immediately displays in lower portion of screen.
---------------------------------------------------------------------------*/
_tempMessage = format["<t size='%1' font='%2'>%3</t>", br_fontSizeDefault, br_fontDefault, _this];
[_tempMessage,0,0.3,10,0,0,150] spawn bis_fnc_dynamictext;
true 