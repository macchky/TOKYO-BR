/*---------------------------------------------------------------------------
STRM: Manages the kill messages
---------------------------------------------------------------------------*/
[] spawn {
	while {true} do 
	{
		sleep .3;
		if !(br_hudKillMessageNotifications isEqualTo []) then
		{
			private _tempArray = + br_hudKillMessageNotifications;
			br_hudKillMessageNotifications = [];
			{
				_displayTime=5;
				if ((_forEachIndex+1) < (count _tempArray)) then {_displayTime=3.5};
				_tempMessage= format["<t size='%1' font='%2'>%3</t>", br_fontSizeDefault, br_fontDefault, _x];
				[_tempMessage,0,0.87,_displayTime,0,0,200] spawn BIS_fnc_dynamicText;
				sleep _displayTime;
			} foreach _tempArray;
 		};
	};
};