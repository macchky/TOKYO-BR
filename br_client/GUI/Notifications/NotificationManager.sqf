/*---------------------------------------------------------------------------
Cheesy manager
---------------------------------------------------------------------------*/
[] spawn {
	while {true} do 
	{
		sleep .3;
		if !(br_hudNotifications isEqualTo []) then
		{
			private _tempArray = + br_hudNotifications;
			br_hudNotifications = [];
			{
				_tempMessage= format["<t size='%1' font='%2'>%3</t>", br_fontSizeDefault, br_fontDefault, (_x select 0)];
				[_tempMessage,0,0.7,(_x select 1),0,0,100] spawn BIS_fnc_dynamicText;
				sleep ((_x select 1)-.5);
			} foreach _tempArray;
 		};
 		if !(br_hudNotificationsSystemChat isEqualTo []) then
		{
			private _tempArray = + br_hudNotificationsSystemChat;
			br_hudNotificationsSystemChat = [];
			{
				systemChat _x;
			} foreach _tempArray;
 		};
	};
};