/*---------------------------------------------------------------------------
STRM: Adds player notification.

	Expects array param:  ["Message", int_message_duration_seconds]

---------------------------------------------------------------------------*/
br_hudNotifications pushBack _this;
true