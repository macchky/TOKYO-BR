/*---------------------------------------------------------------------------
STRM -
This thread is responsible for monitoring for player deaths, and displaying
	messages with no delay.  It accounts for lag in the server sending
	down multiple deaths, trades, etc... and won't display the final
	single-player-left message
---------------------------------------------------------------------------*/
private _done = false;
private _displayTime = 5;
private _tempMessage = "";

//diag_log "[CLIENT MONITOR REMAINING PLAYERS] Starting up...";

waitUntil {br_public_playersRemaining != -1 && br_public_playersInitially != -1};

while {! _done} do
{
//	diag_log format["[CLIENT MONITOR REMAINING PLAYERS] Initial Count=%1 		Remain=%2", br_public_playersInitially, br_public_playersRemaining];
	private _currentPlayerCount = br_public_playersRemaining;
	waitUntil {_currentPlayerCount != br_public_playersRemaining};
	// Player count changed. 
	// During this pause, the server is sending out the details of the kill to be shown on-screen.  Since 2015, server always waited 2 seconds to show this message.
	//	We will wait to see if more people are killed, here.  This accounts for trades, and we still only show the message once.
	sleep 2;
	// Is it 1 player left?  If so exit
	if (br_public_playersRemaining <= 1) exitWith{};

	_tempMessage = format["<t size='%1' font='%2'>%3 DEAD, %4 TO GO!</t>", br_fontSizeDefault, br_fontDefault, br_public_playersInitially-br_public_playersRemaining, br_public_playersRemaining];
	[_tempMessage,0,.2,_displayTime,0,0,250] spawn BIS_fnc_dynamicText;
};
true