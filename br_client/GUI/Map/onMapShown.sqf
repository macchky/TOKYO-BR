/*---------------------------------------------------------------------------
STRM: Map shown
---------------------------------------------------------------------------*/
diag_log "Map shown";

//######################################
// TEAM Help
//######################################
if (br_inTeam && alive player && side player == west) then
{
	diag_log "in team";
	if (!br_teamMapHelpMessageShownAlready) then 
	{
	diag_log "in team2";
		["TeamMapHelp",["","You can draw on the map by holding Ctrl + Left Mouse Button"]] call bis_fnc_showNotification;
		br_teamMapHelpMessageShownAlready=true;
	};
	diag_log "setCurrentChannel";

	// Set to Group channel
	br_teamPlayerCurrentChannel = currentChannel;
	setCurrentChannel 3;
};