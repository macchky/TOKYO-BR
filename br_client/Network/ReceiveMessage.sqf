private ["_sessionIDIntended","_message", "_params", "_messagePayload", "_method", "_methodName"];
_messagePayload = _this;
//diag_log format["DEBUG: Received message! Message Payload=%1", _messagePayload];
_message = _messagePayload select 0;
_params = _messagePayload select 1;
_methodName = format["BR_Client_Network_%1", _message];
_method = missionNamespace getVariable [_methodName,""];
_params call _method;
true