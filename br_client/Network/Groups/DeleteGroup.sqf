/*---------------------------------------------------------------------------
STRM: Deletes a local group on behalf of the server.
---------------------------------------------------------------------------*/
private ["_group"];
_group = _this select 0;
diag_log format["[GROUPS] Client was asked to delete group %1", _group];
deleteGroup _group;
diag_log "[GROUPS] 		-Deleted.";
true