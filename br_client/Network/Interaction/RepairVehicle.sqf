/*---------------------------------------------------------------------------
STRM: Server has asked we fix a vehicle local to this client.
---------------------------------------------------------------------------*/
private ["_params", "_vehicleToRepair", "_hitboxNameArray"];

_vehicleToRepair = _this select 0;
_hitboxNameArray = _this select 1;

if (isnull _vehicleToRepair) exitWith {diag_log "ERROR:  Client as asked to repair a null vehicle!"};

for "_i" from 0 to (count _hitboxNameArray) do {
	if ((_hitboxNameArray select _i) in BR_VEHICLE_REPAIR_ALLOWED_PARTS) then 
	{
		_vehicleToRepair setHitPointDamage [(_hitboxNameArray select _i), 0];
	};
};

diag_log format["I just repaired a vehicle %1", _vehicleToRepair];

true