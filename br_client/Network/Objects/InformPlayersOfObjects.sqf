/*---------------------------------------------------------------------------
Stormridge - 
Server wants to inform client of objects so we can add handles / events.
This is a hack for 1 feature, for now.
---------------------------------------------------------------------------*/
_objectListRaw = (_this select 0);
_objectList = [];
{
	_temp = objectFromNetId _x;
	if !(isNull _temp) then {_objectList pushBack _temp};
} foreach _objectListRaw;
// Sort
{
	if (_x isKindOf "Air") then {br_heliVehicleObjects pushBack _x;};
} foreach _objectList;
true 