/*---------------------------------------------------------------------------
STRM: Server is telling us our team members.
---------------------------------------------------------------------------*/
br_teamMembers = [];
br_teamMembers = + (_this select 0);
systemChat ">>> Your team is updated.  Teammates:";
{
	systemChat format[">>>  %1", name (objectFromNetId _x)];
} foreach br_teamMembers;
call BR_Client_GUI_Teams_TeamList_UpdateIfVisible;
if (!br_game_started) then 
{
	["TeamUpdated",["","Press U to see your team lineup."]] call bis_fnc_showNotification;
};
true 