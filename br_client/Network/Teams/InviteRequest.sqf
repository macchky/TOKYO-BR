/*---------------------------------------------------------------------------
STRM: Pour Moi?
	
	[_teamID, netId _playerObjectInitiate]
---------------------------------------------------------------------------*/
diag_log format["[CLIENT TEAMS] Received invite message %1", _this];
params ["_teamID", "_senderNetID"];
[_teamID, _senderNetID] call BR_Client_GUI_Teams_AddTeamInvite;
true 