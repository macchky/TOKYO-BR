/*---------------------------------------------------------------------------
STRM: Server notifies client it has left a team.
---------------------------------------------------------------------------*/
private _teamID = _this select 0;
diag_log format["[CLIENT TEAMS] Server requested player leave team %1", _teamID];
if (_teamID isEqualTo br_teamID) then 
{
	br_inTeam = false;
	br_teamID = "0";
	br_teamMembers = [];
	diag_log format["[CLIENT TEAMS] Player departed team %1", _teamID];
	call BR_Client_GUI_Teams_TeamList_UpdateIfVisible;
	if (!isNil "br_teamMapDisplayEH") then 
	{
		((findDisplay 12) displayCtrl 51) ctrlRemoveEventHandler ["Draw", br_teamMapDisplayEH];
		br_teamMapDisplayEH = nil;
	};
	systemChat ">>> You have left your current team.";
}
else
{
	diag_log format["[CLIENT TEAMS] ERROR.  Client is not on that team.  They are on team %1", br_teamID];
};
true 