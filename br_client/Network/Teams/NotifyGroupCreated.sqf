/*---------------------------------------------------------------------------
STRM: Server calls this when a team's group has been created.
---------------------------------------------------------------------------*/
["GroupCreated",["","Group Voice and Map drawing enabled."]] call bis_fnc_showNotification;
true 