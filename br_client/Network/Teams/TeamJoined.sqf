/*---------------------------------------------------------------------------
STRM: Server will call this if you are allowed to join a team.
---------------------------------------------------------------------------*/
systemChat ">>> Team joined.";
br_inTeam = true;
br_teamID = _this select 0;
br_teamMapDisplayEH = ((findDisplay 12) displayCtrl 51) ctrlSetEventHandler ["Draw", "_this call BR_Client_GUI_Teams_Map_onMapDraw"];
diag_log format["[CLIENT TEAMS] Player joined team %1", br_teamID];
call BR_Client_GUI_Teams_TeamList_UpdateIfVisible;
true 