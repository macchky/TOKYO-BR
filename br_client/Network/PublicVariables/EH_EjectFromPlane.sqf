/*---------------------------------------------------------------------------
STRM - the things we do...
---------------------------------------------------------------------------*/
br_parachuteFix = {
	waitUntil {isTouchingGround player};
	sleep 2;
	diag_log "Removing LHM Para SEH...";
	["LHM_ParaFix", "OnEachFrame"] call BIS_fnc_removeStackedEventHandler;
};

diag_log "[Plane] Player has been asked by the server to leave the plane.";
if !(br_ejectionMessageReceived) then {
	br_ejectionMessageReceived=true;
	diag_log "[Plane] Spawning client thread to get out.  Ignoring further requests from server to leave the plane.";
	[] spawn {
		while {true} do {
			if ((typeOf vehicle player) != "C130J" &&
				(typeOf vehicle player) != "B_T_VTOL_01_infantry_F") 
				exitWith {diag_log "[Plane] Player has exited the plane or was never in the plane."; [] spawn br_parachuteFix; true };
			if ((typeOf vehicle player) == "C130J" ||
				(typeOf vehicle player) == "B_T_VTOL_01_infantry_F") then {
				diag_log "[Plane] Player is in the plane.  Commanding to leave...";
				player action ["getOut", vehicle player];
				sleep .2;
				player addBackpack "B_Parachute";
			};
			sleep 3;
		};
		if ((backpack player != "B_Parachute") && ((typeOf vehicle player) != "Steerable_Parachute_F")) then {
			diag_log "[Plane] Player still did not have parachute and was not parachuting.  Adding parachute...";
			//diag_log format["[Plane] 	backpack=%1, type vehicle=%2", backpack player, (typeOf vehicle player)];
			player addBackpack "B_Parachute";
		};
	};
};