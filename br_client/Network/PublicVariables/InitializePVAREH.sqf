/*---------------------------------------------------------------------------
STRM: Initialize public EHs
---------------------------------------------------------------------------*/

// Called to reset MP animations
"BR_Animation" addPublicVariableEventHandler
{
	(_this select 1) spawn BR_Client_Network_PublicVariables_EH_MPAnimation;
};

// Called when it's time to eject from the plane
"br_fnc_ejectPlayer" addPublicVariableEventHandler
{
	[] spawn BR_Client_Network_PublicVariables_EH_EjectFromPlane;
};

// Public event for player animation state. 	anim_cancel = [netId player,_stance];
"anim_cancel" addPublicVariableEventHandler {
	_player = objectFromNetId (anim_cancel select 0);
	_playerStance = anim_cancel select 1;
	(_player) switchMove _playerStance;
};
