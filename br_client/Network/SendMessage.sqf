private ["_messageTypeString", "_params", "_id", "_message"];

_messageTypeString = _this select 0;
_params = _this select 1;

_message = [br_client_sessionID,_messageTypeString,_params];
_message remoteExecCall ["BR_Server_Network_ReceiveMessage",2];
_message = nil;
true