/*---------------------------------------------------------------------------
Stormridge
	Removes particle effects on client side, given object.
---------------------------------------------------------------------------*/
_object = objectFromNetId (_this select 0);
if !(isNull _object) then {
	{
		if (typeOf _x == "#particlesource") then {deleteVehicle _x;};
	} forEach (_object nearObjects 5);  
	deleteVehicle _object;
};
true