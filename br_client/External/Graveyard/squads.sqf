/*
	Squad Battle League System - By Ryan - For @PUBattleRoyale
*/

// STRM: This file is not used.  

BRGH_MySquad = grpNull;
BRGH_SquadOwner = false;

"SBL_Notify" addPublicVariableEventHandler {
	hint (_this select 1);
};
"SBL_DeleteGroup" addPublicVariableEventHandler {
	_netId = (_this select 1);
	_group = groupFromNetID _netId;
	if(local _group) then {
		deleteGroup _group;
	};
};
"SBL_CreateSuccess"	addPublicVariableEventHandler {
	SBL_CreateSuccess = nil;
	createDialog 'SBL_Squad';
};

[] spawn {
	while{true} do {
		setGroupIconsVisible [false,false]; 
		if(!isNull BRGH_MySquad) then {		
			if((group player) != BRGH_MySquad) then {
				[player] joinSilent BRGH_MySquad; 
			};
		} else {
			_units = units (group player); 
			if(count _units > 1) then { 
				[player] joinSilent (creategroup WEST);
			};
		};

	};
};

SBL_fnc_OpenInterface = {
	createDialog "SBL_Main";
};
SBL_fnc_LeaveSquad = {
	SBL_LeaveSquad = [player];
	publicVariableServer "SBL_LeaveSquad";
};
SBL_fnc_CreateSquad = {
	SBL_CreateSquad = [player];
	publicVariableServer "SBL_CreateSquad";
};
SBL_fnc_InviteToSquad = {
	_targetUID = "";
	disableserialization;
	_display = uiNamespace getVariable ["SBL_Squad_Display",displayNull];
	if(isNull _display) exitWith {};
	_list = _display displayCtrl 1501;
	
	_index = lbCurSel _list;
	if(_index == -1) exitWith {hint "Please select a player to invite!";};
	
	_c = _list lbColor _index;
	if((_c select 0) == 0) exitWith {hint "That player cannot be invited!";};
	
	_targetUID = _list lbData _index;
	_list lbSetColor [_index,[0,1,1,1]];
	
	SBL_InviteSquad = [player,_targetUID];
	publicVariableServer "SBL_InviteSquad";
};
SBL_fnc_AcceptInvite = {	
	disableserialization;
	_display = uiNamespace getVariable ["SBL_Invites_Display",displayNull];
	if(isNull _display) exitWith {};
	_list = _display displayCtrl 1500;
	
	_index = lbCurSel _list;
	if(_index == -1) exitWith {hint "Please select a squad to join!";};
	
	_uid = _list lbData _index;
	_group = grpNull;
	{ if(getplayeruid _x == _uid) exitWith {_group = group _x;}; } forEach playableUnits;
	if(isNull _group) exitWith {hint "That is not a squad!";};
	
	SBL_AcceptInvite = [player,_group];
	publicVariableServer "SBL_AcceptInvite";
	closeDialog 0;
};	
(findDisplay 46) displayAddEventHandler ["KeyDown", {
	_success = false;
	if((_this select 1) in (ActionKeys "User1") || (_this select 1) == 22) then {
		if(!BR_Game_Started) then {
			if(isNull (findDisplay 1337)) then {
				[] spawn SBL_fnc_OpenInterface;
				_success = true;
			};
		};	
	};
	_success
}];
