//--- Classic camera script, enhanced by Karel Moricky, edited by lazyink and lystic for Battle Royale
// Update by Stormridge for 1.6x

systemChat ">> SPECTATOR: Starting Spectator Mode";
systemChat ">> SPECTATOR: Press ESCAPE to see keybinds";

//Configurations
br_CamEnableThermal = true; //allow the use of thermal mode
br_CamThermalModes = [0,1]; //thermal visions to toggle between (listed on wiki)
br_CamEnableNightVision = true; //allow the use of nightvision
br_mapPlayerIcon = gettext (configfile >> "cfgmarkers" >> "mil_start" >> "icon");

br_fnc_VisualizeBlueZone = {
    private ["_pos1", "_rds", "_steps", "_radStep", "_pos2", "_wall", "_ret","_blueSize"];
	
	waitUntil {!isNil "br_numberOfZones"};
	diag_log "Loaded Spectator Walls, Waiting for Zones";
	//waitUntil {!isNil "br_punishArea"};	// No one is using this any more
	diag_log "Zones active, Waiting for Zone Size";
	waitUntil {!isNil "br_zone_started"};
	diag_log "Zones Started";
	waitUntil {!isNil "zoneSize"};
	while {true} do {
	    _zones = br_numberOfZones;
		_blueSize = zoneSize;
		diag_log format["Blue Start Size: %1",_blueSize];
		diag_log format["No of Zones: %1",_zones];
		_pos1 = [_this,0,getMarkerPos "playArea",[[]]] call BIS_fnc_param;
		_rds = [_this,1,_blueSize,[0]] call BIS_fnc_param;
		_ret = [];
		_retb = [];
		_steps = floor ((2 * pi * _rds) / 15); // 5 meter steps
		_radStep = 360 / _steps;
		for [{_i = 0}, {_i < 360}, {_i = _i + _radStep}] do {
			_pos2 = [_pos1, _rds, _i] call BIS_fnc_relPos;
			_pos2 set [2, 0];
			_wall = "UserTexture10m_F" createVehicleLocal _pos2;
			_wallb = "UserTexture10m_F" createVehicleLocal _pos2;
			_wall setObjectTexture [0,'#(argb,8,8,3)color(1,1,1,0.4)'];
			_wallb setObjectTexture [0,'#(argb,8,8,3)color(1,1,1,0.4)'];
			_wall enableSimulation false;
			_wall setPosATL _pos2;
			_wall setDir _i;
			_wallb enableSimulation false;
			_wallb setPosATL _pos2;
			_wallb setDir (_i + 180);
			_ret = _ret + [_wall];
			_retb = _retb + [_wallb];
		};
		waitUntil{br_numberOfZones != _zones}; //wait until zones changes
		{
		    deleteVehicle _x;
		} forEach _ret;
		{
		    deleteVehicle _x;
		} forEach _retb;
	};	
};
//End Configuration

if (!isNil "BIS_DEBUG_CAM") exitwith {};

//--- Is FLIR available
if (isnil "BIS_DEBUG_CAM_ISFLIR") then {
	BIS_DEBUG_CAM_ISFLIR = isclass (configfile >> "cfgpatches" >> "A3_Data_F");
};

{player reveal [_x,4];} foreach allunits;

BIS_DeathBlur ppEffectAdjust [0.0];
BIS_DeathBlur ppEffectCommit 0.0;
[] spawn {	// And again, if BI's feedback FSM is slow.
	sleep 5;
	BIS_DeathBlur ppEffectAdjust [0.0];
	BIS_DeathBlur ppEffectCommit 0.0;	
};


BIS_DEBUG_CAM_MAP = false;
BIS_DEBUG_CAM_VISION = 0;
BIS_DEBUG_CAM_FOCUS = 0;
BIS_DEBUG_CAM_COLOR = ppEffectCreate ["colorCorrections", 1600];
if (isnil "BIS_DEBUG_CAM_PPEFFECTS") then {
	BIS_DEBUG_CAM_PPEFFECTS = [
		[1, 1, -0.01, [1.0, 0.6, 0.0, 0.005], [1.0, 0.96, 0.66, 0.55], [0.95, 0.95, 0.95, 0.0]],
		[1, 1.02, -0.005, [0.0, 0.0, 0.0, 0.0], [1, 0.8, 0.6, 0.65],  [0.199, 0.587, 0.114, 0.0]],
		[1, 1.15, 0, [0.0, 0.0, 0.0, 0.0], [0.5, 0.8, 1, 0.5],  [0.199, 0.587, 0.114, 0.0]],
		[1, 1.06, -0.01, [0.0, 0.0, 0.0, 0.0], [0.44, 0.26, 0.078, 0],  [0.199, 0.587, 0.114, 0.0]]
	];
};

br_fnc_spectator = {
	//By lystic & lazyink & infistar
	_show = _this select 0;
	_doWork = _this select 1;
	
	[] spawn {
		sleep 15;
		player additem "ItemGPS";
	};

	br_fnc_MapIcons = {

		disableSerialization;
		if (isnil "fnc_MapIcons_run") then
		{
			
			fnc_MapIcons_run = true;
			
			_map = (findDisplay 12) displayCtrl 51;
			_mapdraw = _map ctrlSetEventHandler ["Draw", "_this call br_fnc_mapIconsDraw;"];

		} 
		else 
		{
			fnc_MapIcons_run = nil;
			_map ctrlremoveeventhandler ["Draw", _mapdraw];
		};
		br_fnc_mapIconsDraw = {
			private["_ctrl"];
			_ctrl =  _this select 0;
			_iscale = (1 - ctrlMapScale _ctrl) max .2;
			_irad = 27;
			_color = [0, 0, 0, .8];
			{
				if (alive _x && (str(side _x) != "CIV") && (getPlayerUID _x != "")) then
				{
					_color = [0, 0, 0, .8];
					//if (_x == player) then {} else {_color = [0, 0, 0, 0.8];};
					if (_x getVariable ["BRHit",false]) then {_color = [1,0.35,0.15,0.8];}; //taking damage
					if (_x getVariable ["BRFired",false]) then {_color = [1,0,0,0.8];}; //shooting
					_ctrl drawIcon [br_mapPlayerIcon, _color, getPosASL _x, _iscale*30, _iscale*30, getDir _x, name _x, 1];
				};
			} forEach allPlayers;
		};
	};
	br_esp_toggle = _doWork;
	if(br_esp_toggle) then {
			call br_fnc_MapIcons;
			["SpectatorESP", "onEachFrame", {
					LayerID = 2732;
					{	
						LayerID = LayerID + 1;
						LayerID cutText ["","PLAIN"]; //clear the layer

						br_fireTrigger = _x getVariable ["BRFired",false];
						br_inCombat = _x getVariable ["BRHit",false];

						LayerID cutRsc ["rscDynamicText", "PLAIN"];
						_ctrl = ((uiNamespace getvariable "BIS_dynamicText") displayctrl 9999);
						_screenDiff = safezoneW / 2;

						_HP = (100 - floor((damage _x) * 100));
						_boost = _x getVariable ["br_BoostVar",0];
						// Map Marker for player
						_playername = name _x;
						_playerpos = getPos _x;
						_veh = vehicle _x;
						_cpos = (positionCameraToWorld [0,0,0]);
						_posU = getPos _x;
						_dist = (_cpos distance _posU);
						//_posU2 = [(getPosATL _veh) select 0, (getPosATL _veh) select 1, ((getPosATL _veh) select 2) + (((boundingBox _veh) select 1) select 2) + 1.8];
						_posU2 = _x modelToWorldVisual (_x selectionPosition "Head");
						// STRM: do a better job of lifting this up.
						_posU2 set [2, (_posU2 select 2) - 2 + sqrt _dist * 1.5];

						_pos2D = worldToScreen _posU2;

						_pos = getposatl _x; //get player pos
						_eyepos = ASLtoATL eyepos _x; //get eye pos
						if((getTerrainHeightASL [_pos select 0,_pos select 1]) < 0) then { //correct position when above water
							_eyepos = eyepos _x;
							_pos = getposasl _x;
						};
						if (count _pos2D > 0 && _dist < 1200 && !visibleMap) then {
							
							if (_dist <= 100) then {_ctrl ctrlSetFade 0;};
							if (_dist > 250) then {_ctrl ctrlSetFade 0.1;};
							if (_dist > 500) then {_ctrl ctrlSetFade 0.4;};
							
							//_Tsize = 0.35;
							_Tsize=.6;

							// STRM: Rewrote to do weapon stuff in alternate thread.
							_pName = _x getVariable["SPECTATOR_pName", ""];
							_primaryWeaponCurrentAmmo = _x getVariable["SPECTATOR_primaryWeaponCurrentAmmo", 0];
							_primaryWeaponMagCount = _x getVariable["SPECTATOR_primaryWeaponMagCount", 0];
							_sName = _x getVariable["SPECTATOR_sName", ""];
							_handgunWeaponCurrentAmmo = _x getVariable["SPECTATOR_handgunWeaponCurrentAmmo", 0];
							_handgunWeaponMagCount = _x getVariable["SPECTATOR_handgunWeaponMagCount", 0];

							_text = format ["<t size='%3'font='PuristaMedium'color='#fafafa'>%1 %2M</t><br /><t size='.45'font='PuristaMedium'color='#dddddd'>%4HP (%5)</t><br />",name _x,floor _dist,_Tsize,_HP,_boost];

							//draw bounding box
							if (!br_fireTrigger && !br_inCombat) then {
								if(br_SpectatorMode == 1) then {
									_text = format["%8<t size='.45'font='PuristaMedium'color='#d5d5d5'>%2 [%3-%4]</t>",_Tsize,_pName,_primaryWeaponCurrentAmmo,_primaryWeaponMagCount,_sName,_handgunWeaponCurrentAmmo,_handgunWeaponMagCount,_text];
								} else {
									_text = format ["<t size='%3'font='PuristaMedium'color='#fafafa'>%1 %2M</t><br /><t size='.45'font='PuristaMedium'color='#dddddd'>%4HP (%5)</t><br />",name _x,floor _dist,_Tsize,_HP,_boost];
								};
							} else {

								if (br_fireTrigger) then {
									if(br_SpectatorMode == 1) then {
										_text = format["%8<t size='.45'font='PuristaMedium'color='#ff0000'>%2 [%3-%4]</t>",_Tsize,_pName,_primaryWeaponCurrentAmmo,_primaryWeaponMagCount,_sName,_handgunWeaponCurrentAmmo,_handgunWeaponMagCount,_text];
										//hintSilent parseText _text;
									} else {
										_text = format ["<t size='%3'font='PuristaMedium'color='#ff0000'>%1 %2M</t><br /><t size='.45'font='PuristaMedium'color='#ff0000'>%4HP (%5)</t><br />",name _x,floor _dist,_Tsize,_HP,_boost];
									};
								} else {
									if (br_inCombat) then {
										_text = format ["<t size='%3'font='PuristaMedium'color='#fafafa'>%1 %2M</t><br /><t size='.45'font='PuristaMedium'color='#d77f27'>%4HP (%5)</t><br />",name _x,floor _dist,_Tsize,_HP,_boost];
										if(br_SpectatorMode == 1) then {
											_text = format["%8<t size='.45'font='PuristaMedium'color='#d77f27'>%2 [%3-%4]</t>",_Tsize,_pName,_primaryWeaponCurrentAmmo,_primaryWeaponMagCount,_sName,_handgunWeaponCurrentAmmo,_handgunWeaponMagCount,_text];
										} else {
											_text = format ["<t size='%3'font='PuristaMedium'color='#d77f27'>%1 %2M</t><br /><t size='.45'font='PuristaMedium'color='#d77f27'>%4HP (%5)</t><br />",name _x,floor _dist,_Tsize,_HP,_boost];
										};
									};
								};
							};
							_ctrl ctrlShow true;_ctrl ctrlEnable true;
							_ctrl ctrlSetStructuredText parseText _text;
							_ctrl ctrlSetPosition [(_pos2D select 0) - _screenDiff, (_pos2D select 1), safezoneW, safezoneH];
							_ctrl ctrlCommit 0;
						}
						else
						{	
							disableSerialization;
							_ctrl ctrlShow false;_ctrl ctrlEnable false;
						};

					} foreach (missionNamespace getVariable["SPECTATOR_ALLPLAYERS", []]);

					//remove extra layers 
					for '_i' from 1 to 50 do {
						LayerID = LayerID + 1;
						LayerID cutText["","PLAIN"]; //clear the layer
					};
					
			}] call BIS_fnc_addStackedEventHandler;	
	} else {
		LayerID = 2732;
		for '_i' from 1 to 50 do {
			LayerID = LayerID + 1;
			LayerID cutText["","PLAIN"]; //clear the layer
		};
		["SpectatorESP", "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
	};
};
	
//--- Undefined
private _playerObj = _this select 0;
private _camStartPos = _this select 1;
//if (typename _this != typename objnull) then {_this = cameraon};

private _ppos = _camStartPos;

private ["_local"];


[] spawn {
	// Stormridge - Monitor	thread which moves crap out of the Draw3D EH
	private ["_playerList", "_weaponInfoString", "_curWeaponName", "_curWeaponMagCount","_curWeaponAmmo"];
	while {true} do {
		_playerList=[];
		{

			if ( 	(alive _x) && 
					(str(side _x) != "CIV") && 
					!(_x getVariable ["IsSpectator",false])) then 
			{
				_playerList pushBack _x;

				_pweapon = primaryWeapon _x;
				_sweapon = handgunweapon _x;
				_cfgPWeapon = configfile >> "CfgWeapons" >> _pweapon;
				_cfgSWeapon = configfile >> "CfgWeapons" >> _sweapon;
				_pName = gettext (_cfgPWeapon >> "displayname");							
				_sName = gettext (_cfgSWeapon >> "displayname");
				if(format["%1 ",_pName] == " ") then {_pName = "-"};
				if(format["%1 ",_sName] == " ") then {_sName = "-"};
				
				_info = magazinesAmmoFull _x;
				_primaryWeaponMagCount = 0;
				_primaryWeaponCurrentAmmo = 0;
				_handgunWeaponMagCount = 0;
				_handgunWeaponCurrentAmmo = 0;

				_hgunMags = getArray(_cfgSWeapon >> "Magazines");
				_pgunMags = getArray(_cfgPWeapon >> "Magazines");
				{
					_mag = _x select 0;
					_ammoCount = _x select 1;
					_isLoaded = _x select 2;
					_holder = _x select 4;
					if(_mag in _hgunMags) then {
						_handgunWeaponMagCount = _handgunWeaponMagCount + 1;
					};
					if(_mag in _pgunMags) then {
						_primaryWeaponMagCount = _primaryWeaponMagCount + 1;
						
					};
					if(_isLoaded) then {
						if(_holder == _pweapon) then {
							_primaryWeaponCurrentAmmo = _ammoCount;
						};
						if(_holder == _sweapon) then {
							_handgunWeaponCurrentAmmo = _ammoCount;
						};	
					};
				} forEach _info;
				_handgunWeaponMagCount = (_handgunWeaponMagCount - 1) max 0;
				_primaryWeaponMagCount = (_primaryWeaponMagCount - 1) max 0;
				// Store
				_x setVariable["SPECTATOR_pName", _pName];
				_x setVariable["SPECTATOR_primaryWeaponCurrentAmmo", _primaryWeaponCurrentAmmo];
				_x setVariable["SPECTATOR_primaryWeaponMagCount", _primaryWeaponMagCount];
				_x setVariable["SPECTATOR_sName", _sName];
				_x setVariable["SPECTATOR_handgunWeaponCurrentAmmo", _handgunWeaponCurrentAmmo];
				_x setVariable["SPECTATOR_handgunWeaponMagCount", _handgunWeaponMagCount];
			};
		} foreach allPlayers;
		missionNamespace setVariable["SPECTATOR_ALLPLAYERS", _playerList];
		uiSleep 1;
	};
};

_local = "camcurator" camCreate (_ppos vectorAdd [0,0,2]);
BIS_DEBUG_CAM = _local;
_local camCommand "MANUAL ON";
_local camCommand "INERTIA OFF";
_local cameraEffect ["INTERNAL", "BACK"];				//Switch chamera to BIS_DEBUG_CAM
_local camCommand format ["speedDefault %1", .2];
_local camCommand format ["speedMax %1", 3];
_local camCommand "maxPitch 89.0";
_local camCommand "minPitch -89.0";

_local spawn {sleep 1; _this camCommand "surfaceSpeed off";};

showCinemaBorder false;
BIS_DEBUG_CAM setDir direction (vehicle _playerObj);

//--- For auto follow fire.

br_toggleFollowAction = false;
br_moveCam = false;

//--- Marker
BIS_DEBUG_CAM_MARKER = createmarkerlocal ["BIS_DEBUG_CAM_MARKER",_ppos];
BIS_DEBUG_CAM_MARKER setmarkertypelocal "mil_start";
BIS_DEBUG_CAM_MARKER setmarkercolorlocal "colorblue";
BIS_DEBUG_CAM_MARKER setmarkersizelocal [.5,.5];
BIS_DEBUG_CAM_MARKER setmarkertextlocal "CAMERA";


//--- Key Down

_keyDown = (finddisplay 46) displayaddeventhandler ["keydown",
{
	//diag_log format["[SPECTATOR KEYDOWN] This = %1",_this];

	_key = _this select 1;
	_ctrl = _this select 3;

	if (_key in (actionkeys 'showmap')) then {
		if (BIS_DEBUG_CAM_MAP) then {
			openmap [false,false];
			BIS_DEBUG_CAM_MAP = false;
		} else {
			openmap [true,true];
			BIS_DEBUG_CAM_MAP = true;
			BIS_DEBUG_CAM_MARKER setmarkerposlocal position BIS_DEBUG_CAM;
			BIS_DEBUG_CAM_MARKER setmarkerdirlocal direction BIS_DEBUG_CAM;
			mapanimadd [0,0.1,position BIS_DEBUG_CAM];
			mapanimcommit;
		};
	};
	//  '*'  on numeric keypad
	if (_key == 55) then {
		_worldpos = screentoworld [.5,.5];
		if (_ctrl) then {
			vehicle player setpos _worldpos;
		} else {
			copytoclipboard str _worldpos;
		};
	};
	//  '.  on numeric keypad
	if (_key == 83 && !isnil 'BIS_DEBUG_CAM_LASTPOS') then {
		BIS_DEBUG_CAM setpos BIS_DEBUG_CAM_LASTPOS;
	};
	// accent grave
	if (_key == 41) then {
		BIS_DEBUG_CAM_COLOR ppeffectenable false;
	};
	// 1 through 0
	if (_key >= 2 && _key <= 11) then {
		// _id = _key - 2;
		// if (_id < count BIS_DEBUG_CAM_PPEFFECTS) then {
		// 	BIS_DEBUG_CAM_COLOR ppEffectAdjust (BIS_DEBUG_CAM_PPEFFECTS select _id);
		// 	BIS_DEBUG_CAM_COLOR ppEffectCommit 0;
		// 	BIS_DEBUG_CAM_COLOR ppeffectenable true;
		// };
	};
}];

if(isNil "br_cam_runonce") then {
	br_CamMode = 0;
	br_SpectatorMode = 2;
	br_CamTarget = objnull;
	br_ThermalMode = -1;
	br_CamNightVision = false;
	[] spawn br_fnc_VisualizeBlueZone; //Add Blue Zone Borders. Fixed by lystic :D	
	["Player Lock For Spectator (Stops Player Movement)"] spawn {
		waituntil{alive player};
		_pos = getpos player;
		_dir = getdir player;
		while{true} do {
			waitUntil{!isNull br_CamTarget};
			player setDir _dir;
			player setPos _pos;
		};
	};
	br_cam_runonce = (findDisplay 46) displayaddeventhandler ["keydown",
	{
		_key = _this select 1;
		switch(_key) do {

			// C
			case 46: {
				if(br_CamEnableThermal) then {
					if(br_ThermalMode == (count(br_CamThermalModes)-1)) then {
						br_ThermalMode = -1;
					} else {
						br_ThermalMode = br_ThermalMode + 1;
					};
					if(br_ThermalMode != -1) then {
						true setCamUseTi (br_CamThermalModes select br_ThermalMode);
					} else {
						false setCamUseTi 0;
					};
				};
				true;
			};

			// N
			case 49: {
				if(br_CamEnableNightVision) then {
					br_CamNightVision = !br_CamNightVision;
					camUseNVG br_CamNightVision;
				};
			};

			// B
			case 48: {
				if(br_SpectatorMode == 2) then {
					br_SpectatorMode = 0;
					[br_SpectatorMode,true] call br_fnc_spectator;
				} else {
					if(br_SpectatorMode == 0) then {
						br_SpectatorMode = 1;
						[br_SpectatorMode,true] call br_fnc_spectator;
					} else {
						br_SpectatorMode = 2;
						[br_SpectatorMode,false] call br_fnc_spectator;
					};
				};
					
				true;
			};

			// LeftArrow on arrow keypad
			case 203: {
				_units = [];
				{
					if(alive _x && side _x != civilian) then {_units = _units + [_x];};
				} forEach playableUnits;

				if(!isNull br_CamTarget) then {
					_index = _units find br_CamTarget;
					if(_index != -1) then {
						if(_index <= 0) then {_index == (count(_units)-1);} else {_index = _index - 1;};
						_unit = _units select _index;
						if(!isNull _unit) then {
							br_CamTarget = _unit;
							if(br_CamMode == 1) then {
								br_CamTarget switchCamera 'EXTERNAL';
							} else {
								br_CamTarget switchCamera 'INTERNAL';
							};
						};
					};
				};
			};

			// RightArrow on arrow keypad
			case 205: {
				_units = [];
				{
					if(alive _x && side _x != civilian) then {_units = _units + [_x];};
				} forEach playableUnits;

				if(!isNull br_CamTarget) then {
					_index = playableUnits find br_CamTarget;
					if(_index != -1) then {
						if(_index >= (count(_units)-1)) then {_index == 0;} else {_index = _index + 1;};
						_unit = _units select _index;
						if(!isNull _unit) then {
							br_CamTarget = _unit;
							if(br_CamMode == 1) then {
								br_CamTarget switchCamera 'EXTERNAL';
							} else {
								br_CamTarget switchCamera 'INTERNAL';
							};
						};
					};
				};
			};

			// V
			case 47: {
				if(isNull br_CamTarget) then {
					_cursorTarget = screenToWorld[0.5,0.5];

					_object = objnull;
					{
						if(alive _x && side _x != civilian) exitWith {_object = _x;};
					} forEach (nearestObjects [_cursorTarget,['Man'],100]);

					if(!isNull _object) then {
						BIS_DEBUG_CAM = objnull;
						br_CamTarget = _object;
						br_CamTarget switchCamera 'INTERNAL';
						systemChat format['>> SPECTATOR: Following %1',name br_CamTarget];
					};
				} else {
					if(br_CamMode == 0) then {
						br_CamMode = 1;
						br_CamTarget switchCamera 'EXTERNAL';
					} else {
						br_CamMode = 0;
						[br_CamTarget, getPosATL br_CamTarget] call BR_Client_Spectator_SpectatorCam;
						br_CamTarget = objnull;
					};
				};
				true;
			};
			default {false};
		};
	}];
};

//--- Mouse wheel moving
_mousezchanged = (finddisplay 46) displayaddeventhandler ["mousezchanged",{
	_n = _this select 1;
	BIS_DEBUG_CAM_FOCUS = BIS_DEBUG_CAM_FOCUS + _n/10;
	if (_n > 0 && BIS_DEBUG_CAM_FOCUS < 0) then {BIS_DEBUG_CAM_FOCUS = 0};
	if (BIS_DEBUG_CAM_FOCUS < 0) then {BIS_DEBUG_CAM_FOCUS = -1};
	BIS_DEBUG_CAM camcommand 'manual off';
	BIS_DEBUG_CAM campreparefocus [BIS_DEBUG_CAM_FOCUS,1];
	BIS_DEBUG_CAM camcommitprepared 0;
	BIS_DEBUG_CAM camcommand 'manual on';
}];

_map_mousebuttonclick = ((finddisplay 12) displayctrl 51) ctrladdeventhandler ["mousebuttonclick",
{
	_haltPropagation=false;
	_button = _this select 1;
	_ctrl = _this select 5;
	if (_button == 0) then {
		_x = _this select 2;
		_y = _this select 3;
		_worldpos = (_this select 0) posscreentoworld [_x,_y];
		if (!_ctrl) then {
			BIS_DEBUG_CAM setpos [_worldpos select 0,_worldpos select 1,position BIS_DEBUG_CAM select 2];
			BIS_DEBUG_CAM_MARKER setmarkerposlocal _worldpos;
		};
		_haltPropagation=true;
	};
	if (_button == 1) then {
		//diag_log "DETECTED MOUSE 2";
		_haltPropagation=true;
	};
	_haltPropagation
}];


//Wait until destroy is forced or camera auto-destroyed.
[_local,_keyDown,_mousezchanged,_map_mousebuttonclick] spawn {
	private ["_local","_keyDown","_mousezchanged","_map_mousebuttonclick","_lastpos"];

	_local = _this select 0;
	_keyDown = _this select 1;
	_mousezchanged = _this select 2;
	_map_mousebuttonclick = _this select 3;
	_lastpos = [0,0,0];

	waituntil {
		if (!isnull BIS_DEBUG_CAM) then {_lastpos = position BIS_DEBUG_CAM};
		isNull BIS_DEBUG_CAM
	};

	player cameraEffect ["TERMINATE", "BACK"];
	deletemarkerlocal BIS_DEBUG_CAM_MARKER;
	BIS_DEBUG_CAM = nil;
	BIS_DEBUG_CAM_MAP = nil;
	BIS_DEBUG_CAM_MARKER = nil;
	BIS_DEBUG_CAM_VISION = nil;
	camDestroy _local;
	
	BIS_DEBUG_CAM_LASTPOS = _lastpos;

	ppeffectdestroy BIS_DEBUG_CAM_COLOR;
	(finddisplay 46) displayremoveeventhandler ["keydown",_keyDown];
	(finddisplay 46) displayremoveeventhandler ["mousezchanged",_mousezchanged];
	((finddisplay 12) displayctrl 51) ctrlremoveeventhandler ["mousebuttonclick",_map_mousebuttonclick];

};