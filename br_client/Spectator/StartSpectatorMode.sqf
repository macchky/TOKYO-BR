/*---------------------------------------------------------------------------
STRM: Starts up  spectator mode
---------------------------------------------------------------------------*/
private _camStartPos = _this;

player setVariable ["IsSpectator", true];

if (side player != civilian) then {[player] joinSilent (createGroup CIVILIAN);diag_log "[SPECTATOR] Switched Side";};

diag_log "[SPECTATOR] Moving body";
private _specfinallocation = [getMarkerPos "spectator",random 50 max 5,random 360,0] call SHK_pos;
player setPos _specfinallocation;
player allowDamage false;

diag_log "[SPECTATOR] Asking server to disable sim.";
["Client_Spectator_Initialize", [netId player, "spectator"]] call BR_Client_Network_SendMessage;

if (!isNil "br_teamMapDisplayEH") then 
{
	((findDisplay 12) displayCtrl 51) ctrlRemoveEventHandler ["Draw", br_teamMapDisplayEH];
	br_teamMapDisplayEH = nil;
};

br_isThisPlayerSpectator=true;

[player, _camStartPos] spawn BR_Client_Spectator_SpectatorCam;