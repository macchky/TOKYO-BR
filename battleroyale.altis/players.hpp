class Header
{
	gameType = LastMan;				//DM, Team, Coop, ...
	minPlayers = 1;				//Min # of players the mission supports
	maxPlayers = 64;			//Max # of players the mission supports
};