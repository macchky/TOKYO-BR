/*---------------------------------------------------------------------------
This file is sent to clients on Mission start, and only executed by clients
AFTER they have a "body" and have zoned in.

This file is still kept on the server as a way to control and update files,
live, from server-side.

This replaces the old Init.SQF, such that the client FSM has time to prepare
and then only runs this script after the player has zoned into the lobby.
---------------------------------------------------------------------------*/

call compileFinal preprocessFileLineNumbers "Solo.sqf";
call compileFinal preprocessFileLineNumbers "Ranked.sqf";

if (!getRemoteSensorsDisabled) then {disableRemoteSensors true;};

br_newsFeedURL="BRNewsFeed.html";
br_rulesMessage = parseText "<t size='2' color='#EAA32D'>Arma 3 Battle Royale Rules</t><br/>
<br/>
<t color='#EAA32D'>No hate speech allowed of any kind</t>, either in voice or text chat. A first offense results in a 14 day ban, a second offense is a permanent ban.<br/>
<br/>
There are no longer any appeals to reduce HS bans and longer bans may be given at the discretion of the admin staff if a user is particularly offensive or spams hate speech repeatedly.
Hate speech includes but is not limited to:<br/>
<t  color='#FF0000'> - Racist slurs (e.g. any variation of the n-word)</t><br/>
<t  color='#FF0000'> - Homophobic slurs (e.g. any variation of the f-word)</t><br/>
 - Playing Hitler's speeches, etc.<br/>
 - Playing racist or homophobic songs, etc.<br/>
<br/>
Use common good manners and please don't try to test our limits and patience; using slight misspellings and spoonerisms to avoid detection but still get your meaning across will be treated just as harshly as if you used the offensive terms outright.<br/>
<br/>
<t color='#EAA32D'>Teaming is forbidden in servers with [SOLO] tags and [TEAM] servers are limited to 4 player teams!</t> If you get caught teaming in a SOLO server or participating on a TEAM server with a team larger than 4 players, you will receive a 7-day ban for your first offense, a 30-day ban for your second offense and a permanent ban for your third offense.<br/>
<br/>
Teaming includes but is not limited to:<br/>
<t color='#FF0000'> - Playing together (e.g. running around the map in tandem, looting, sharing loot, not killing another player when you have the chance, etc.)</t><br/>
<t color='#FF0000'> - Sharing information over VOIP services</t><br/>
If a teaming investigation leads to a conclusion that the teaming has continued for a longer duration without being detected earlier, a more severe punishment (up to and including a permanent ban) may be given without any prior warnings. <br/>
<br/>
<t color='#EAA32D'>Abusing bugs</t> such as glitching inside rocks or viewing through walls will result in a 30 day ban for a first offense and a permanent ban for a second offense.<br/>
<br/>
<t color='#EAA32D'>Streamsniping</t> can result in a ban, up to and including a permanent ban for a first offense.<br/>
<br/>
<t color='#EAA32D'>Cheating and hacks</t> will result in a permanent ban for a first offense.<br/>
<br/>
Any questions and clarifications about the rules will be answered in Discord, so it's better to ask before rather than after getting banned.<br/>
<br/>
If you feel you have been banned by mistake, you can appeal in the <t color='#EAA32D'>#br-banappeals</t> channel on the Battle Royale Discord. Discord invite is available at <a underline='true' href='http://battleroyalegames.com/community'>http://battleroyalegames.com/community</a><br/>
<br/>";

br_gameRulesMessageText = "Teaming on this SOLO server is forbidden. We have automatic tools to detect teaming, as well as manual investigations and other players reporting you. We have given over 17800 teaming bans already, please read the rules and keep it fair for everyone! You can team on servers which don't have the SOLO tag in the server name.","This is a SOLO play server","I understand";

//#################################################################################
// Code
//#################################################################################
waituntil {time > 0};	// STRM: Added to ensure local game time has started
waitUntil {!isNull player && isPlayer player && alive player};

pvar_playerUID = [player,getPlayerUID player];
publicVariableServer "pvar_playerUID";

// Start location
br_startPosition = getMarkerPos "start";

[] spawn BR_Client_Game_Monitor_BlueCirclePunishment;	
[] spawn BR_Client_Game_Monitor_BlackCirclePlayAreaPunishment;
[] spawn BR_Client_Game_Monitor_LobbyNamesVON;
//[] spawn BR_Client_Game_Monitor_LobbyDistanceLock;

//[] spawn BR_Client_Game_Monitor_LobbyDamageProtection;   	// Disabled until Client Fix
//[] spawn BR_Client_Game_WaitParachuteCinemaExperience;	// Disabled until Client Fix

[] spawn BR_Client_Player_Boost_BoostSystemWaitStart;
[] spawn BR_Client_GUI_Map_MapSwitchTextures;
[] spawn BR_Client_GUI_Help_HPBarHelper;
[] spawn BR_Client_GUI_Rules_DisplayRules;

diag_log "[CLIENT] Monitors Loaded";

// STRM: When transitioning a player into a new body, sometimes animations can be messed up.  This will reset it.
player switchMove "amovpercmstpsnonwnondnon";

// ########################################
// Lobby AI
// ########################################
// STRM: See if we have special abilities // Keep this server side.
private _playerUID = getPlayerUID player;
if (_playerUID in br_adminList) then {
	brLobbyGameManager = 0 call BR_Client_Environment_LobbyGameManager_CreateLobbyGameManager;
} else {
	if (_playerUID in br_streamerAdminList && !(call br_trackServer)) then {
		brLobbyGameManager = 2 call BR_Client_Environment_LobbyGameManager_CreateLobbyGameManager;
	};
};
if !(isnil "brLobbyGameManager") then {
	[] spawn {
		disableSerialization;
		waitUntil {br_deploy_players};	
		deleteVehicle brLobbyGameManager;
	};
};

//####################################################
// Stormridge's custom damage model for Arma3 BR
//####################################################
br_oneShotProtectionTimespan = 1.0;
br_lastOneShotTime = 0;
br_lastExplosionTime = 0;
br_lastMassiveHit = 0;
br_lastBulletTime = 0;
br_thisBulletOverallDamage = 0;
br_thisBulletStartingHealth = 0;
br_maxOverallDamagePerBullet = 0.25;
player addEventHandler ["HandleDamage", 
{ 
	private ["_isFuelExplosion", "_dmgDelta", "_isSniper" ,"_resultValue","_priorDamageValue","_knownProjectileModifier","_dmgMultiplierHead","_dmgMultiplierNeck","_dmgMultiplierBody","_dmgMultiplierHands","_dmgMultiplierLegs","_dmgMultiplierOverall","_isExplosiveRound"];
	
	params [
	    "_unit",
	    "_selection",
	    "_engineProposedDamageValue",
	    "_source", 
	    "_projectile",
	    "_hitPartIndex",
	    "_instigator"
	];

	_priorDamageValue = 0; 
	_resultValue = 0;
	_knownProjectileModifier= 1.0;

	_dmgMultiplierHead = .7; 
	_dmgMultiplierNeck = .5;
	_dmgMultiplierBody = 1.1;
	_dmgMultiplierHands = .5;
	_dmgMultiplierLegs = .225;
	_dmgMultiplierOverall = 0.75;

	_isExplosiveRound = false;
	_isSniper = false;
	_isFuelExplosion = false;

	switch (_projectile) do
	{
		case "FuelExplosion":
		{
			_dmgMultiplierOverall = 100;
			_isFuelExplosion = true;
		};
		case "G_40mm_HEDP":
		{
			_knownProjectileModifier = .35;
			_isExplosiveRound = true;
		};
		case "G_40mm_HE":
		{
			_knownProjectileModifier = .5;
			_isExplosiveRound = true;
		};
		case "mini_Grenade":
		{
			_isExplosiveRound = true;
		};
		case "APERSMine_Range_Ammo":
		{
			_isExplosiveRound = true;
		};
		case "APERSBoundingMine_Range_Ammo":
		{
			_isExplosiveRound = true;
		};
		case "APERSTripMine_Wire_Ammo":
		{
			_isExplosiveRound = true;
		};
		case "CUP_PipeBomb_Ammo":
		{
			_isExplosiveRound = true;
		};
		case "ClaymoreDirectionalMine_Remote_Ammo":
		{
			_isExplosiveRound = true;
		};
		case "SLAMDirectionalMine_Wire_Ammo":
		{
			_isExplosiveRound = true;
		};
		case "CUP_R_PG7VR_AT":
		{
			_isExplosiveRound = true;
		};
		// Non-Explosive Round Exceptions Go Here.
		case "CUP_B_12Gauge_Pellets":
		{
			_knownProjectileModifier = .5; _dmgMultiplierHead = .1; _dmgMultiplierNeck=.1; _dmgMultiplierLegs=.1; _dmgMultiplierHands=.1;
		};
		// Remove in next client patch when config is boosted.
		case "B_762x51_Ball":
		{
			_isSniper = true;
			_knownProjectileModifier = 1.5;
		};
		// Remove in next client patch when config is boosted.
		case "CUP_B_762x39_Ball_Tracer_Green":
		{
			_isSniper = true;
			_knownProjectileModifier = 1;
		};
		// Remove in next client patch when config is boosted.
		case "CUP_B_762x39_Ball":
		{
			_isSniper = true;
			_knownProjectileModifier = 1;
		};		
		case "CUP_B_86x70_Ball_noTracer":
		{
			_isSniper = true;
		};
		case "CUP_B_762x54_Ball_White_Tracer":
		{
			_isSniper = true;
		};
		case "CUP_B_127x108_Ball_Green_Tracer":
		{
			_isSniper = true;
		};
	};
	if (_isExplosiveRound) then 
	{
		if (diag_tickTime - br_lastExplosionTime > 5) then
		{
			if !(isNull _instigator) then
			{
				br_lastExplosionTime = diag_tickTime;
				player setVariable ["br_recentExplosiveHitAmmo", _projectile, true];
				player setVariable ["br_recentExplosiveHitOwnerNetId", netid _instigator, true];
				[] spawn
				{
					sleep 4;
					player setVariable ["br_recentExplosiveHitAmmo", "", true];
				};
			};
		};
	};
	switch (_hitPartIndex) do
	{ 
		case 0:
		{
			_priorDamageValue = _unit getHitIndex 0; // Face
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierHead * _knownProjectileModifier);
		};
		case 1:
		{
			_priorDamageValue = _unit getHitIndex 1; // Neck
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierNeck * _knownProjectileModifier);
		}; 			
		case 2:
		{
			_priorDamageValue = _unit getHitIndex 2; // Head
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierHead * _knownProjectileModifier); 
		};
		case 3:
		{
			_priorDamageValue = _unit getHitIndex 3; // Pelvis
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierBody * _knownProjectileModifier); 
		}; 
		case 4:
		{
			_priorDamageValue = _unit getHitIndex 4; //spine1
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierBody * _knownProjectileModifier); 
		};  
		case 5:
		{
			_priorDamageValue = _unit getHitIndex 5; //spine2
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierBody * _knownProjectileModifier); 
		};  
		case 6:
		{
			_priorDamageValue = _unit getHitIndex 6;	//spine3
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierBody * _knownProjectileModifier); 
		};			
		case 7:
		{
			_priorDamageValue = _unit getHitIndex 7; // Body
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierBody * _knownProjectileModifier); 
		}; 
		case 8:
		{
			_priorDamageValue = _unit getHitIndex 8; // Arms
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierHands * _knownProjectileModifier); 
		};  
		case 9:
		{
			_priorDamageValue = _unit getHitIndex 9; // Hands
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierHands * _knownProjectileModifier); 
		}; 
		case 10:
		{
			_priorDamageValue = _unit getHitIndex 10; // Legs
			_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierLegs * _knownProjectileModifier); 
		}; 
		case -1:
		{
			_priorDamageValue = damage _unit;		

			// If sniper ammo, you dead.
			if (_isSniper) then
			{
				_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _dmgMultiplierOverall);
			} 
			else
			{

				_overallMultiplierForThisHit = _dmgMultiplierOverall;

				if (diag_tickTime - br_lastBulletTime < 0.1) then
				{

					_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _overallMultiplierForThisHit * _knownProjectileModifier);
					
					_dmgDelta = abs (_resultValue - _priorDamageValue);

					if (br_thisBulletOverallDamage + _dmgDelta >= br_maxOverallDamagePerBullet && !_isFuelExplosion) then
					{
						br_thisBulletOverallDamage = br_maxOverallDamagePerBullet;
						_resultValue = br_thisBulletStartingHealth + br_thisBulletOverallDamage;
					}
					else
					{
						br_thisBulletOverallDamage = br_thisBulletOverallDamage + _resultValue;
						_resultValue = br_thisBulletStartingHealth + br_thisBulletOverallDamage;
					};
				}
				else
				{
					br_lastBulletTime = diag_tickTime;
					br_thisBulletOverallDamage = 0;
					br_thisBulletStartingHealth = _priorDamageValue;

					_resultValue = _priorDamageValue + ((_engineProposedDamageValue - _priorDamageValue) * _overallMultiplierForThisHit * _knownProjectileModifier);

					_dmgDelta = abs (_priorDamageValue - _resultValue);

					if (_dmgDelta > br_maxOverallDamagePerBullet) then
					{
						_resultValue = _priorDamageValue + br_maxOverallDamagePerBullet;
					} 
					else
					{
					};
					br_thisBulletOverallDamage = 0 + _resultValue;
				};
			};
		};
		default{ };
	};
	_resultValue
}];

// Apply default skin
["Default Skin", true] call BR_Client_GUI_Skins_ApplySkin;

////////////////////////////////////////////////////////////////////////////////////////////////////////
// IN PROGRESS - MOVE THESE TO CLIENT ON NEXT PATCH
////////////////////////////////////////////////////////////////////////////////////////////////////////
// STRM: Stormridge's quick fix for Tactical View hacks.   Move to client on next update.
br_tacViewMonitorMouse = objNull;
[] spawn
{
	waituntil {!(IsNull findDisplay 46)};
	(findDisplay 46) displayAddEventHandler ["KeyDown",
		{
			private _rValue = false;
			if ((_this select 1) in ActionKeys "TacticalView") then
			{
				_rValue = true;
			};
			_rValue
		}
	];
	(findDisplay 46) displayAddEventHandler ["MouseButtonDown",
		{
			private _rValue = false;
			if (inputAction "TacticalView" > 0) then 
			{
				if (isNull br_tacViewMonitorMouse) then 
				{
					br_tacViewMonitorMouse = [] spawn
					{
						while {true} do
						{
							if (cameraView isEqualTo "GROUP") then
							{
								player switchCamera "INTERNAL";
							};
							sleep .03;
						};
					};
				};
				_rValue = true;
			};
			_rValue
		}
	];
};

BR_Client_Network_Teams_NotifyLineupChangeSilent = 
{
	br_teamMembers = [];
	br_teamMembers = + (_this select 0);
	call BR_Client_GUI_Teams_TeamList_UpdateIfVisible;
	true
};

BR_Client_Game_WaitParachuteCinemaExperience_TempServer = 
{
	waitUntil{!isNil "br_date"};

	player allowDamage false;

	waitUntil {br_game_started};

	showGPS false;
	showMap false;
	showHUD false;

	uiSleep 8;

	[0,1.5,false] call BIS_fnc_cinemaBorder;
	private ["_ppGrain"];
	_ppGrain = ppEffectCreate ["filmGrain", 2012];
	_ppGrain ppEffectEnable true;
	_ppGrain ppEffectAdjust [0.1, 1, 1, 0, 1];
	_ppGrain ppEffectCommit 0;		

	if (worldName == "wake") then {
		//player addBackpack "B_Parachute";	
		_spawnPos = [getMarkerPos "center",random 2000 max 750,random 360,0] call SHK_pos;
		player setPosASL [ _spawnPos select 0, _spawnPos select 1,(_spawnPos select 2) + 2300];
	};

	if (br_isGameStreetFight) then {
		_spawnZone = br_gameAreaRadius - 400;
		player addBackpack "B_Parachute";
		_spawnPos = [getMarkerPos "center",_spawnZone,random 360,0] call SHK_pos;
		player setPosASL [ _spawnPos select 0, _spawnPos select 1,(_spawnPos select 2) + 2400];
	};

	waitUntil {br_alert};

	0 call BR_Client_Game_Effects_ShakeScreen;
	uiSleep 8;
	0 call BR_Client_Game_Effects_ShakeScreen;

	waitUntil {br_deploy_players};

	showGPS true;
	showHUD true;
	showMap true;

	uiSleep 3;
	[1,1.5,false] call BIS_fnc_cinemaBorder;
	if (worldName == "wake") then 
	{
		player addBackpack "B_Parachute";
		systemChat ">> Backpack supplied!";
	};
	uiSleep 1.5;
	ppEffectDestroy _ppGrain;

	waitUntil { (((getPosATL player) select 2) < 400) };

	player allowDamage true;

	true 	
};
[] spawn BR_Client_Game_WaitParachuteCinemaExperience_TempServer;

BR_Client_Game_Monitor_LobbyDistanceLock_TempServer = {
	private ["_currentPos","_initialPos"];

	_initialPos = br_startPosition;

	if (worldName isEqualTo "tanoa") then
	{
		while{!br_game_started} do
		{
			if (br_deploy_players) exitWith {};
		    _currentPos = position player;
		    if (_initialPos distance2D _currentPos > 85) then {	player setpos ([_initialPos,(random 30 max 10),random 360,0] call SHK_pos);};
		    uiSleep 2;
		};
	} 
	else
	{
		while{!br_game_started} do
		{
			
			if (br_deploy_players) exitWith {};
		    
		    _currentPos = position player;
		    
		    if (_initialPos distance2D _currentPos > 55 && 
		    	!(player getVariable["IsSpectator",false]) && 
		    	!(player getVariable["IsTeamSpectator",false])) then 
		    {

				if (worldName == "bozcaada") then 
				{
					private _spawnPosition = _initialPos;

					for '_i' from 0 to 30 do 
					{
						_bozSpawnPos = [_spawnPosition,(random 40 max 10),random 360,0] call SHK_pos;
						//diag_log format["[BOZ_SPAWN] Trying %1", _bozSpawnPos];
						if (count (lineIntersectsObjs [_bozSpawnPos vectorAdd [0,0,50], _bozSpawnPos vectorAdd [0,0,-50], objNull, objNull, false, 32]) == 0) exitWith {_spawnPosition = _bozSpawnPos;};
					};

					player setPosATL[_spawnPosition select 0, _spawnPosition select 1, 0];
				}
				else
				{
		        	player setpos ([_initialPos,(random 30 max 10),random 360,0] call SHK_pos);
		        };
		    };

		    uiSleep 2;
		};
	};
	true 	
};
[] spawn BR_Client_Game_Monitor_LobbyDistanceLock_TempServer;


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

// // STRM: Notify the client that this intialization is complete, so the FSM can remove the curtain
br_ClientSidePlayerInitComplete=true;
diag_log "[CLIENT] Done with ClientSidePlayerInit.";
diag_log "[CLIENT] Signaling FSM to remove the curtain.";