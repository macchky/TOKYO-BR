/*---------------------------------------------------------------------------
Initialize server variables and set the game type.

WARNING:  This file is shared by BattleRoyale and Gang War!
---------------------------------------------------------------------------*/
call compileFinal preprocessFileLineNumbers "Solo.sqf";
call compileFinal preprocessFileLineNumbers "Ranked.sqf";

diag_log format["[RULES] SOLO=%1 	RANKED=%2", br_solo, br_ranked];

if (!getRemoteSensorsDisabled) then {disableRemoteSensors true;};

if (br_gameType isEqualTo "BR") then {
	call compile preprocessFileLineNumbers "battleroyale_setup.sqf";
	diag_log format["[MARKERS] Blue Zone Start Time: %1", br_blueStartTime];
	diag_log format["[MARKERS] Number of zones: %1", br_numberOfZones];
	diag_log format["[MARKERS] Round Time: %1", br_roundTime / 60];
	diag_log format["[MARKERS] Red Zone Start: %1", br_startRedZones];
	call compileFinal preprocessFileLineNumbers "\br_server\server_init.sqf";
} else {
	if (br_gameType isEqualTo "GW") then {
		call compile preprocessFileLineNumbers "gangwar_setup.sqf";
	};
};