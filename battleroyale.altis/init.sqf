/*

	Concept by PLAYERUNKNOWN (Brendan Greene). Based on the book/film Battle Royale.

	Development by PLAYERUNKNOWN & Stormridge.
	
	Additional code by Sa-Matra, Tzer, Anatanakomi & nutyyy.
	
	Leaderboard Development by ohhlie & Rexeh.
	
	Legacy code by [VB]AWOL & pwn0zor & Kegan 'Ryan' Holern.

	Legacy BR Tools by eXchangeDev.
	
	MV-22 port by Hotshotmike.
	
	C130 port by ebu.
	
	Extra weapon packs by Hotshotmike, FHQ, Trixie, blazenchamber & The Community Upgrade Project.
	
	Joint-rails system by ASDG.
	
	Community management & support by SniperGhost.

	Ideas, promotion and mod artwork by PLAYERUNKNOWN with help from the Twitter community.
	
	Special thanks to ShroomzTV, CohhCarnage, Smak and The Blacklist.
	
	My thanks also to all the players, donators, streamers and content creators, without whom we would be nothing. <3
	
	A special thanks to @Dabbo07 for being a great guy and re-naming his mission to avoid confusion with our mod. 
	You can check out his work here http://steamcommunity.com/sharedfiles/filedetails/?id=185349097
	
	Using this code without PLAYERUNKNOWN's direct permission is forbidden.
	Please do not attempt to reuse this code, the server files will be eventually available for hosting.	
	
	version 0.8.0.0
	
	PLAYERUNKNOWN's Battle Royale & associated code is copyright © 2013-2015 Brendan Greene.
	
	All other code copyright respective authors.
	
	battleroyalegames.com	

	
*/