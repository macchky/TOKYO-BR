#include <GameMode.hpp>

br_restartServer = compileFinal 'true';  // Dev-mode = false.
br_trackServer = compileFinal 'true';  	// Leaderboard Tracking?
br_is64Player = getNumber(missionConfigFile >> "Header" >> "maxPlayers") == 64;
br_triggerDivisor = 100;				// Divisor for trigger size.

br_use_V2_circle_system = true;	// STRM: Should the game use the V2 circle algorithm for maps that support it?
if (br_isGameStreetFight) then {br_use_V2_circle_system=false;};

br_allowAllPlayersSpectatorAccess = false;	// Allow non-spectators to spectate?

br_startLock = 30;					// Radius, in meters, from the start position that players can move within.

br_vehicleLimit = 90;

br_crateNumberBozcaada = round (random 6 max 3);						// Total number of care-packages to spawn.

br_startrandom = random 30 max 10;			// Random radius, in meters, from the start position that players will spawn. (random 30 meters, minimum of 5 meters)

br_maxFogDensity = 0.2;					// Max density of the fog.
br_maxFogStrength = 0.12;					// Max strength of the fog.

br_lootPercent = 55;						// Percentage of positions in a building that will spawn loot.	
br_lootAllowZamak = true;					// Allow Zamak as an airdrop?
br_lootZamakPercentChance = .10;			// 10% of all drops are Zamak (~ 1 per game.)

br_vehicleFuel_Minimum = 5;					// The percent (max 50) that the fuel tank can have at its least amount.
br_vehicleAmmo_Minimum = 5;					// The percent (max 50) that the vehicle can have in its guns at a minimum.
br_minVehicleDistance = 400;					// Min distance, in meters, between vehicles.  Stratis will only find 45 spots, Altis:90 Boz:90

br_killDamage =  0.5;					// Damage taken when outside the Game Area. 0.9 = Walking Dead
br_killDelay =  20;						// Time between damage.

br_punishDamage =  0.2;					// Damage taken when outside the Blue Zone. 0.9 = Walking Dead
br_punishDelay =  20;					// Time between damage.

br_playersToStart = 40;					// Default # players required to start the game.

if (worldName == "altis") then {
	if (br_isGameStreetFight) then {
		br_playersToStart = 20;					// Players needed to start the game if Altis 64.		
		br_freeRoamTime = 1;   				// Total Free Roam time before the blue zone in announced, in minutes.
		br_gameAreaRadius = 1000;				// Radius, in meters, of the game area.			
		br_blueZoneSize = 500;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
		br_blueZoneDelay = (2*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
		br_redZoneRadius = 100;	
		br_vehicleLimit = 15;
		br_triggerDivisor = 20;				// Divisor for trigger size.

		br_minVehicleDistance = 100;			// Min distance, in meters, between vehicles.

		br_ospreyMaxSpawns = 2;
	} else {

		if (br_is64Player) then {
			br_playersToStart = 40;					// Players needed to start the game if Altis 64.
			br_smallZoneMin = 20;					// Upper player threshold for the smaller blue zone system.
		} else {
			br_playersToStart = 40;					// Players needed to start the game.
			br_smallZoneMin = 20;					// Upper player threshold for the smaller blue zone system.
		};
		
		br_freeRoamTime = 6;   				// Total Free Roam time before the blue zone in announced, in minutes.
		br_gameAreaRadius = 4000;				// Radius, in meters, of the game area.			
		br_blueZoneSize = 2000;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
		br_blueZoneDelay = (5*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
		br_redZoneRadius = 300;					// Radius, in meters, of the red zone.
	
		br_ospreyMaxSpawns = 4;

		br_vehicleLimit = 75;
	};
};

if (worldName == "Wake") then {
	br_playersToStart = 40;					// Players needed to start the game.
	
	br_freeRoamTime = 6;   					// Total Free Roam time before the blue zone in announced, in minutes.
	br_smallZoneMin = 15;					// Upper player threshold for the smaller blue zone system.
	br_gameAreaRadius = 6000;				// Radius, in meters, of the game area.	
	
	br_blueZoneSize = 1500;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
	br_blueZoneDelay = (4*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
	br_redZoneRadius = 200;					// Radius, in meters, of the red zone.	

	br_ospreyMaxSpawns = 3;

	br_vehicleLimit = 75;
};

if (worldName == "gorgona") then {
	br_playersToStart = 32;					// Players needed to start the game.
	br_ospreyMaxSpawns = 3;

	br_freeRoamTime = 5;   					// Total Free Roam time before the blue zone in announced, in minutes.
	br_smallZoneMin = 15;					// Upper player threshold for the smaller blue zone system.
	br_gameAreaRadius = 3000;				// Radius, in meters, of the game area.	
		
	br_blueZoneSize = 1000;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
	br_blueZoneDelay = (4*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
	br_redZoneRadius = 300;					// Radius, in meters, of the red zone.	
};
if (worldName == "stratis") then {
	br_playersToStart = 40;					// Players needed to start the game.
	br_ospreyMaxSpawns = 3;
	
	br_freeRoamTime = 6;   					// Total Free Roam time before the blue zone in announced, in minutes.
	br_smallZoneMin = 15;					// Upper player threshold for the smaller blue zone system.
	br_gameAreaRadius = 5000;				// Radius, in meters, of the game area.	
		
	br_blueZoneSize = 1500;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
	br_blueZoneDelay = (4*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
	br_redZoneRadius = 200;					// Radius, in meters, of the red zone.	

	br_vehicleLimit = 40;
};
if (worldName == "pianosa_aut") then {
	br_playersToStart = 32;					// Players needed to start the game.
	br_ospreyMaxSpawns = 3;
	
	br_freeRoamTime = 5;   					// Total Free Roam time before the blue zone in announced, in minutes.
	br_smallZoneMin = 15;					// Upper player threshold for the smaller blue zone system.
	br_gameAreaRadius = 5000;				// Radius, in meters, of the game area.			
	br_blueZoneSize = 1500;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
	br_blueZoneDelay = (4*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
	br_redZoneRadius = 300;					// Radius, in meters, of the red zone.	
};

if (worldName == "bozcaada") then {
	br_playersToStart = 40;					// Players needed to start the game.
	
	br_freeRoamTime = 5;   					// Total Free Roam time before the blue zone in announced, in minutes.
	br_smallZoneMin = 15;					// Upper player threshold for the smaller blue zone system.
	br_gameAreaRadius = 6000;				// Radius, in meters, of the game area.	
	br_blueZoneSize = 2000;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
	br_blueZoneDelay = (4*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
	br_redZoneRadius = 250;					// Radius, in meters, of the red zone.	

	br_vehicleLimit = 90;
};

if (worldName == "tanoa") then {
	br_vehicleLimit = 120;
	br_minVehicleDistance = 150;			// Min distance, in meters, between vehicles.

	br_freeRoamTime = 6;   				// Total Free Roam time before the blue zone in announced, in minutes.
	br_gameAreaRadius = 4000;				// Radius, in meters, of the game area.			
	br_blueZoneSize = 2000;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
	br_blueZoneDelay = (5*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.	
	br_redZoneRadius = 200;					// Radius, in meters, of the red zone.
	br_ospreyMaxSpawns = 4;

	br_playersToStart = 32;					// Players needed to start the game.

	br_smallZoneMin = 15;					// Upper player threshold for the smaller blue zone system.
};


// 	Define allowed vehicles. 		// ["Vehicle_Class_Name", Number to spawn]

br_allowedVehicles = [ 		
		// STRM:  Removed this line in order to remove specific skins on trucks.
		//"C_Offroad_01_repair_F","C_Offroad_luxe_F","C_Offroad_stripped_F","C_Offroad_01_bluecustom_F","C_Hatchback_01_sport_white_F","C_Hatchback_01_sport_orange_F","C_Hatchback_01_sport_blue_F","C_Hatchback_01_F","C_Hatchback_01_bluecustom_F","C_Quadbike_01_red_F","C_Quadbike_01_white_F","C_Van_01_transport_F"
		
		// STRM: Added this line so that cars have random skins
		// For now, hacking up the list so each car is randomly chosen. 
		//		Since SIG has 6 variants, then give each car 6 chances to spawn.
		
		"C_Offroad_01_F", "C_Offroad_01_F","C_Offroad_01_F","C_Offroad_01_F","C_Offroad_01_F","C_Offroad_01_F",

		"C_Hatchback_01_sport_F", "C_Hatchback_01_sport_F", "C_Hatchback_01_sport_F", "C_Hatchback_01_sport_F",

		"C_Hatchback_01_F","C_Hatchback_01_F","C_Hatchback_01_F",	// these just piss people off, but have to have bad with the good

		"C_Quadbike_01_F","C_Quadbike_01_F","C_Quadbike_01_F","C_Quadbike_01_F","C_Quadbike_01_F",

		"C_Van_01_transport_F","C_Van_01_transport_F","C_Van_01_transport_F","C_Van_01_transport_F",

		"C_SUV_01_F","C_SUV_01_F","C_SUV_01_F","C_SUV_01_F","C_SUV_01_F","C_SUV_01_F",

		//"SIG_Roadrunner","SIG_Hcode","SIG_Hubcaps","SIG_SuperBeeB","SIG_SuperBeeM","SIG_SuperBeeG",	// Removed until we fix troll horn

		"C_Van_01_box_F", // Nerfed in config.  Most rare until we prove they still aren't OP.

		"C_Offroad_01_repair_F", 	// Yellow repair truck
		"B_G_Offroad_01_repair_F","B_G_Offroad_01_repair_F","B_G_Offroad_01_repair_F", 	// Camo truck
		"O_Truck_02_transport_F", 	// Camo flatbed zamak
		"C_Truck_02_transport_F",	// Orange flatbed zamak

		"beetle_bleufonce", "beetle_red",	// Keep these rare.   "beetle_violet"

		"BAF_Offroad_D", "BAF_Offroad_W", "LandRover_CZ_EP1", "LandRover_ACR", "LandRover_TK_CIV_EP1"	// LandRovers, excluding medical and those with guns
		
];

br_allowedBoats = [ 
		"B_Boat_Transport_01_F","C_Boat_Civil_01_police_F","C_Boat_Civil_01_F","C_Boat_Civil_01_rescue_F","O_Lifeboat"
];


//	Define Admins.					// ["PlayerID"]
//		These players have special abilities in the lobby.  USE WITH CAUTION!
br_adminList = [

	"76561198043195607", "76561197968599375",  // STRM accounts 1 & 2
	"76561198025631899", // PU
	"76561198014678596", // Ana
	"76561197960295173", // Tzer
	"76561198129589009", // filmgoerjuan
	"76561198021684398", // Ollie
	"76561198045077926", // Thompson
	"76561198000452188",  // MrTag
	"76561198014580913"	 // Rexeh
];

// These players can join late, for testing and admin.  (Requires RCON unlock / re-lock)
br_allowLateJoinList = [

	"76561198043195607", "76561197968599375",  // STRM accounts 1 & 2
	"76561198025631899", // PU
	"76561198014678596", // Ana
	"76561197960295173", // Tzer
	"76561198129589009", // filmgoerjuan
	"76561198021684398", // Ollie
	"76561198045077926", // Thompson
	"76561198000452188", // MrTag
	"76561198014580913"	 // Rexeh
];

// These players can see an AI in the lobby that allows them to start the game. USE WITH CAUTION!
//		By default this list should be _EMPTY_.
//		This WILL NOT WORK UNLESS  >>> br_trackServer = FALSE <<<
br_streamerAdminList = [
];

//	Define Spectators. 				// ["PlayerID"]

br_spectatorList = [
"76561198025631899",
"76561197966985049",
"76561198081096413",
"76561198050599388",
"76561197997190608",
"76561197961561215",
"76561198027262493",
"76561197965472766",
"76561197984642533",
"76561198018355980",
"76561198040810073",
"76561198024059019",
"76561197960270723",
"76561198025563965",
"76561198034093047",
"76561198116073625",
"76561197963017948",
"76561197979949128",
"76561197986928787",
"76561197964443605",
"76561198051995777",
"76561198028049906",
"76561197970460349",
"76561197993239761",
"76561198093925833",
"76561198048982520",
"76561197995079340",
"76561198081801792",
"76561198048740999",
"76561198076752617",
"76561198000881027",
"76561197986034235",
"76561198011769010",
"76561198054081049",
"76561198051801204",
"76561198027922296",
"76561197969151605",
"76561197983916561",
"76561198084104645",
"76561198072886882",
"76561197970265018",
"76561198013037436",
"76561197969650468",
"76561197995346332",
"76561197961444340",
"76561198043407114",
"76561197960698927",
"76561198060866590",
"76561197970835542",
"76561198078478596",
"76561197968619211",
"76561198041073291",
"76561197997556029",
"76561198068747708",
"76561198040255818",
"76561198133332653",
"76561197960951853",
"76561198045918474",
"76561198055438202",
"76561197976190680",
"76561198035597031",
"76561198050048063",
"76561198009978959",
"76561197960799782",
"76561198044913897",
"76561197973973196",
"76561198094823661",
"76561198075160207",
"76561198014832068",
"76561198005531463",
"76561198007116581",
"76561197972692780",
"76561198045607538",
"76561197968612670",
"76561198086197694",
"76561197980157117",
"76561198123170029",
"76561198044716753",
"76561198103356529",
"76561198100785873",
"76561198040378403",
"76561198078547846",
"76561197976046192",
"76561198058330497",
"76561197995236202",
"76561198029395723",
"76561198062566000",
"76561198032695381",
"76561197969086503",
"76561198068090138",
"76561198019341495",
"76561198135851550",
"76561198054909702",
"76561197971951082",
"76561197992530313",
"76561197999886431",
"76561197980392059",
"76561198047729399",
"76561198077511634",
"76561197998588679",
"76561198052590805",
"76561198067137924",
"76561197996444757",
"76561198155024518",
"76561198050334115",
"76561197961957254", // Moondye 7
"76561198161382768", // Knueppelpaste (MD7)
"76561197978730167", // Maihopawango (MD7)
"76561198005720354",
"76561198041177124",
"76561198040531523",
"76561198042132589",
"76561198068435955",
"76561197983886598",
"76561198003976743",
"76561197967743909",
"76561198004409445",
"76561198062562255",
"76561198018947086",
"76561198001576275",
"76561198040992758",
"76561197960330333",
"76561197971192137",
"76561198006378653",
"76561198129470176",
"76561197962285004",
"76561198075212667",
"76561198014678596",
"76561197960295173",
"76561197981235509",
"76561198141361954",
"76561198047711634",
"76561198024478413",
"76561198017988359",
"76561198067868940",
"76561197981707409",
"76561198004831291",
"76561198194732335",
"76561197964925885",
"76561198041066298",
"76561197982698803",
"76561198043994878",
"76561198013659044",
"76561198071201186",
"76561197979960418",
"76561198021684398",
"76561198069676573",
"76561198050287050",
"76561197998360529",
"76561198129589009",
"76561198043195607", 
"76561198026881026", // Ace12
"76561197968599375",
"76561197997596176", // Valient Thor
"76561197992494835", // Fuffenz
"76561198014580913", // Rexeh
"76561197982095955", // devilwalker
"76561198045077926", // 80PGaming
"76561197960363160", // Hans Meiser
"76561197977052017",  // G Thang
"76561198135532864", // Tzer 2nd account
"76561197973243840", // PJSaltana
"76561197982079120", // MrGrimmmz
"76561197987494747", // Klittdoris
"76561197983395022", // Priestarrr
"76561198014315679", // Krakensmash
"76561198071076035", // NovaProject
"76561197986866881", // Misanthrope
"76561198005200485",  // Luddigus
"76561198027982536", //M1nder
"76561198072232115", //ZANKI0H
"76561198074384365",  //rxnexus
"76561198000452188",	// MrTag
"76561198066807235",	// Ellisonfire
"76561198029708770",		// ABCamper
"76561198036926766"		// Ewanng
];


//	Define Donators. 				// ["PlayerID"]

br_donators = [

	"76561198025631899","76561198081096413","76561198050599388",
	"76561197961561215","76561198027262493","76561197965472766","76561197984642533","76561198018355980","76561198040810073","76561198024059019",
	"76561197960270723","76561198025563965","76561198034093047","76561197963017948","76561197979949128","76561197986928787","76561197997190608",
	"76561197964443605","76561198051995777","76561198028049906","76561197970460349","76561197993239761","76561198093925833","76561198048982520",
	"76561197995079340","76561198081801792","76561198048740999","76561198076752617","76561198000881027","76561197986034235","76561198011769010",
	"76561198054081049","76561198051801204","76561198027922296","76561197969151605","76561197983916561","76561198084104645","76561198072886882",
	"76561197970265018","765611980379901414","76561198013037436","76561197969650468","76561197995346332","76561197961444340","76561198043407114",
	"76561197960698927","76561198060866590","76561197970835542","76561198078478596","76561197968619211","76561198041073291","76561197997556029",
	"76561198068747708","76561198040255818","76561198133332653","76561197960951853","76561198045918474","76561198055438202","76561197976190680",
	"76561198035597031","76561198050048063","76561198009978959","76561197960799782","76561198044913897","76561197973973196","76561198094823661",
	"76561198075160207","76561198014832068","76561198005531463","76561198007116581","76561197972692780","76561198045607538","76561197968612670",
	"76561198086197694","76561197980157117","76561198123170029","76561198044716753","76561198103356529","76561198100785873","76561198040378403",
	"76561198078547846","76561197976046192","76561198058330497","76561197995236202","76561198029395723","76561198062566000","76561198032695381",
	"7656119816736797","76561197969086503","76561198068090138","76561198019341495","76561198135851550","76561197971951082","76561197992530313",
	"76561197999886431","76561198111755168","76561197992880410","76561198009783351","76561198067597885","76561198039950396",
	"76561197970523405","76561198007776778","76561198021684398","76561197989891262","76561198004409445"

];

// Some equations to calculate the blue zone timings.

br_numberOfZones = ((br_blueZoneSize / 500) + 4);
br_numberOfZones_atStart = br_numberOfZones;

br_blueStartTime = (br_numberOfZones * (br_blueZoneDelay / 60)) - 1;
br_roundTime = (br_blueStartTime + br_freeRoamTime + 8) * 60;
br_startRedZones = (br_roundTime / 60) - 4;
br_lootRadius = br_gameAreaRadius;
br_vehicleArea = br_gameAreaRadius;

if (worldName isEqualTo "tanoa") then {
	br_allowedVehicles = + [
		"C_Offroad_02_unarmed_F", 
		"C_Offroad_02_unarmed_F", 
		"C_Offroad_02_unarmed_F", 
		"C_Offroad_02_unarmed_F", 
		"C_Offroad_02_unarmed_F", 
		"C_Offroad_02_unarmed_F", 
		"C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"I_C_Offroad_02_unarmed_F", 
		"B_T_LSV_01_unarmed_F", 
		"B_T_LSV_01_unarmed_F", 
		"B_T_LSV_01_unarmed_F", 
		"O_T_LSV_02_unarmed_F",
		"O_T_LSV_02_unarmed_F",
		"O_T_LSV_02_unarmed_F"
	];
	br_allowedBoats	= [
		"C_Scooter_Transport_01_F", 
		"I_C_Boat_Transport_02_F", 
		"C_Boat_Transport_02_F",
		"B_Boat_Transport_01_F","C_Boat_Civil_01_police_F","C_Boat_Civil_01_F","C_Boat_Civil_01_rescue_F","O_Lifeboat"
	];
};