skipLobby = 1;

respawn = "BASE";
respawnDelay = 7;
respawnDialog = 0;
respawnOnStart = 0;
respawnButton = 1;

disableChannels[] = {0,1,2,6};	// 0 = Global 1 = Side 2 = Command 3 = Group 4 = Vehicle 5 = Direct 6 = System

onLoadMission = "Extreme Survival Championship";
OnLoadIntro = "One Dead. Many To Go.";
OnLoadIntroTime = 0;
OnLoadMissionTime = 0;
overviewText = "Extreme Survival Championship";

scriptedPlayer = 1;
disabledAI = 1;

forceRotorLibSimulation = 2;

enableItemsDropping = 0;
enableDebugConsole = 1;

onPauseScript = "";
author = "PLAYERUNKNOWN";
briefing = 0;
debriefing = 0;
joinUnassigned = 0;

corpseManagerMode = 2;
corpseLimit = 100;
corpseRemovalMinTime = 6000;
corpseRemovalMaxTime = 9000;

wreckManagerMode = 2;
wreckLimit = 100;
wreckRemovalMinTime = 6000;
wreckRemovalMaxTime = 9000;

overviewPicture = "\br_client\images\br_loadscreen.paa";
loadScreen = "\br_client\images\br_loadscreen.paa";

allowFunctionsLog = 1;

aiKills = 0;

disableRandomization[] = {"All"};

showSquadRadar = 0;
showUAVFeed = 0;

showHUD[] = {
	true,	// Scripted HUD (same as showHUD command)
	true,	// Vehicle + soldier info
	false,	// Vehicle radar [HIDDEN]
	true,	// Vehicle compass [HIDDEN]
	true,	// Tank direction indicator
	false,	// Commanding menu
	false,	// Group Bar
	true,	// HUD Weapon Cursors
	false	// HUD Squad Radar
};
